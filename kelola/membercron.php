<?php
session_start();
header("Cache-control: private");

include ("urasi.php");
include ("fungsi.php");
include ("../modul/webmember/lang/$lang/definisi.php");
include ("../modul/webmember/urasi.php");

if (!$_SESSION['s_userid']) die();

// $sql = "SELECT konstanta, terjemahan FROM translation";
// $result = $mysql->query($sql);
// while (list($konstanta, $terjemahan) = $mysql->fetch_row($result)) {
	// define($konstanta,$terjemahan);
// }

$sql = "SELECT user_id, user_email, fullname, kategori_hipnoterapi, kode_digit, expired, alert FROM webmember WHERE activity=2 ORDER BY user_id";
$result = $mysql->query($sql);
if ($mysql->num_rows($result) > 0) {
	
	while($row = $mysql->fetch_assoc($result)) {
		
		$user_email = $row['user_email'];
		$user_name = $row['fullname'];
		$kode_digit = $row['kode_digit'];
		$kategori_hipnoterapi = $row['kategori_hipnoterapi'];
		
		$nominal = ($kategori_hipnoterapi == 1) ? substr($amountcats[$kategori_hipnoterapi], 0, 3) . $kode_digit : substr($amountcats[$kategori_hipnoterapi], 0, 4) . $kode_digit;
		$terbilang = bilangan($nominal);
		
		$tanggal1 = new DateTime($row['expired']);
		$tanggal2 = new DateTime();
		$dayscompare = $tanggal2->diff($tanggal1)->format("%a");
		
		if ($dayscompare <= $alert3 && $dayscompare >= 0) {
			if ($dayscompare <= $alert3 && $dayscompare > $alert2) {
				
				if ($row['alert'] < 1) {
					
					$sql = "UPDATE webmember SET alert=1 WHERE user_id='{$row['user_id']}'";
					$mysql->query($sql);
					
					$html = array(
						'/#expired/' => tglformat($row['expired']),
						'/#namamember/' => "<strong>$user_name</strong>",
						'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>",
						'/#nominalkategori/' => "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>",
					);
					$message = preg_replace(array_keys($html), array_values($html), $email_template1);

					fiestophpmailer($user_email,EMAIL_TEMPLATE_1,$message,$smtpuser,$sendername,$smtpuser, $message);
				}
			} else if ($dayscompare <= $alert2 && $dayscompare > $alert1) {
				
				if ($row['alert'] < 2) {
					$sql = "UPDATE webmember SET alert=2 WHERE user_id='{$row['user_id']}'";
					$mysql->query($sql);
					
					$html = array(
						'/#expired/' => tglformat($row['expired']),
						'/#namamember/' => "<strong>$user_name</strong>",
						'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>",
						'/#nominalkategori/' => "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>",
					);
					$message = preg_replace(array_keys($html), array_values($html), $email_template2);
					
					fiestophpmailer($user_email,EMAIL_TEMPLATE_2,$message,$smtpuser,$sendername,$smtpuser, $message);
				}
			} else if ($dayscompare <= $alert1 && $dayscompare >= 0) {
				
				if ($row['alert'] < 3) {
					$sql = "UPDATE webmember SET alert=3 WHERE user_id='{$row['user_id']}'";
					$mysql->query($sql);
					
					$html = array(
						'/#expired/' => tglformat($row['expired']),
						'/#namamember/' => "<strong>$user_name</strong>",
						'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>",
						'/#nominalkategori/' => "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>",
					);
					$message = preg_replace(array_keys($html), array_values($html), $email_template3);
					
					fiestophpmailer($user_email,EMAIL_TEMPLATE_3,$message,$smtpuser,$sendername,$smtpuser, $message);
				}
			}
		}
	}
}

?>