<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $config_site_metadescription;?>">
    <meta name="keyword" content="<?php echo $config_site_metakeyword;?>">
    <meta name="author" content="fiesto.com">
	<link href="<?php echo $favicon ?>" rel="SHORTCUT ICON" />

    <title><?php echo $config_site_titletag;?></title>
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <link href="<<<TEMPLATE_URL>>>/css/bootstrap.min.css" rel="stylesheet">
	<?php echo $style_css; ?>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- Custom CSS -->
    <link href="<<<TEMPLATE_URL>>>/css/basic.css?ver=6.2" rel="stylesheet">
    <link href="<<<TEMPLATE_URL>>>/css/animate.css" rel="stylesheet">
	<link href="<<<TEMPLATE_URL>>>/css/owl.carousel.css" rel="stylesheet"/>
    <link href="<<<TEMPLATE_URL>>>/css/owl.theme.css" rel="stylesheet"/>
    <link href="<<<TEMPLATE_URL>>>/css/owl.transitions.css" rel="stylesheet"/>
	<link rel="stylesheet" href="<<<TEMPLATE_URL>>>/css/jquery.fileupload.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
<?php if(!$_GET['p']) {?>
.content-wrapper:nth-child(odd) {
    background: #fff;
}
.textlogo img {
    margin-top: 25px;
}
<?php } else {?>
.navbar-header {
    width: 100%;
}
#header-site a.navbar-brand{justify-content:center}
.contact-header li{color:#fff}
.content-wrapper {
    padding: 70px 15px 60px;
}
#menu-block .block-menu-line {
    box-shadow: none;
    border: 1px solid #ddd;
}
#header-site {
    border: 0;
    border-radius: 0;
    margin: 0;
    background: #1f155e;
    border-bottom: 2px solid #eee;
	position:relative;
	padding-bottom:37px;
}
.contact-header img {
    filter: brightness(0) invert(1);
}
.contact-header a {
    color: #fff;
}
.textlogo img {
	margin-top:13px;
}
.navbar-inverse .navbar-toggle .icon-bar {
    background-color: #fff;
    height: 4px;
}
@media(min-width:768px){
	.content-wrapper .container > .row {
		margin: 0;
	}
}
@media(max-width:767px){
.content-wrapper {
    padding: 60px 15px 70px;
}
#header-site {
    border: 0;
    border-radius: 0;
    margin: 0;
    border-bottom: 2px solid #eee;
    position: relative;
    padding-bottom: 0px;
}
}
<?php } ?>
</style>
</head>

<body>
<div id="fixed">
<?php
$template = new TemplateForm();
include "header.php";
echo $display_main_content_block;
include "footer.php";
?>	
</div>
    <script src="<<<TEMPLATE_URL>>>/js/jquery-1.9.1.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/bootstrap.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/owl.carousel.min.js"></script>
    <script src="<<<TEMPLATE_URL>>>/js/wow.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/jquery.ui.widget.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/jquery.iframe-transport.js"></script>
	<script src="<<<TEMPLATE_URL>>>/js/jquery.fileupload.js"></script>
	<?php echo $script_js; ?>
	<script>
		$(function() {

			$('#close-menu').click( function() {
				$(".navbar-collapse.in").removeClass("in");
				$("html").removeClass("menu-active");
			} );
			$('.navbar-toggle').click( function() {
				$("html").addClass("menu-active");
			} );
			$('.bg-black').click( function() {
				$(".navbar-collapse.in").removeClass("in");
				$("html").removeClass("menu-active");
			} );
			$('.navbar-inverse .navbar-toggle').click( function() {
				$("html").addClass("menu-active");
			} );
			$('#content-blocker').click( function() {
				$("html").removeClass("menu-active");
			} );
			// $("#slide-home").owlCarousel({
				// navigation : false, // Show next and prev buttons
				// slideSpeed : 300,
				// autoPlay : true,
				// paginationSpeed : 400,
				// pagination : false,
				// singleItem:true
			// });
			$("#slide-home").owlCarousel({
				autoplay :true,
				loop:true,
				autoplayTimeout: 10000,
				items:1,
				nav:false,
				dots :false
			});	
			// $(".download_conten").owlCarousel({
				// autoplay :true,
				// loop:true,
				// autoplayTimeout: 10000,
				// items:4,
				// nav:true,
				
				// navText: ['<svg width="100%" height="100%" viewBox="0 0 11 20"><path style="fill:none;stroke-width: 2px;stroke: #182e77;" d="M9.554,1.001l-8.607,8.607l8.607,8.606"/></svg>','<svg width="100%" height="100%" viewBox="0 0 11 20" version="1.1"><path style="fill:none;stroke-width: 2px;stroke: #182e77;" d="M1.054,18.214l8.606,-8.606l-8.606,-8.607"/></svg>'],
				// dots :false

			// });	
		});
		wow = new WOW(
		  {
			animateClass: 'animated',
			offset:       100,
			callback:     function(box) {
			  console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
			}
		  }
		);
		wow.init();
		
		
		$(function () {
			'use strict';
			// Change this to the location of your server-side upload handler:
			var url = '<?php echo $cfg_img_url?>/webmember/';
					
			$('#pasfoto').fileupload({
				url: url + '?state=pasfoto',
				dataType: 'json',
				paramName: 'pasfoto',
				done: function (e, data) {
					console.log(data.result.pasfoto);
					$.each(data.result.pasfoto, function (index, file) {
						// $('<p/>').text(file.name).appendTo('#files');
					});
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .progress-bar').css(
						'width',
						progress + '%'
					);
				}
			}).prop('disabled', !$.support.fileInput)
				.parent().addClass($.support.fileInput ? undefined : 'disabled');
				
			$('#fotoktp').fileupload({
				url: url + '?state=fotoktp',
				dataType: 'json',
				done: function (e, data) {
					console.log(data.result.files);
					$.each(data.result.files, function (index, file) {
						// $('<p/>').text(file.name).appendTo('#files');
					});
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progressfotoktp .progress-bar').css(
						'width',
						progress + '%'
					);
				}
			}).prop('disabled', !$.support.fileInput)
				.parent().addClass($.support.fileInput ? undefined : 'disabled');
				
			$('#fotosertifikat').fileupload({
				url: url + '?state=fotosertifikat',
				dataType: 'json',
				done: function (e, data) {
					console.log(data.result.files);
					$.each(data.result.files, function (index, file) {
						// $('<p/>').text(file.name).appendTo('#files');
					});
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progressfotosertifikat .progress-bar').css(
						'width',
						progress + '%'
					);
				}
			}).prop('disabled', !$.support.fileInput)
				.parent().addClass($.support.fileInput ? undefined : 'disabled');
				
			$('#fotolain1').fileupload({
				url: url + '?state=fotolain1',
				dataType: 'json',
				done: function (e, data) {
					console.log(data.result.files);
					$.each(data.result.files, function (index, file) {
						// $('<p/>').text(file.name).appendTo('#files');
					});
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progressfotolain1 .progress-bar').css(
						'width',
						progress + '%'
					);
				}
			}).prop('disabled', !$.support.fileInput)
				.parent().addClass($.support.fileInput ? undefined : 'disabled');
				
			$('#fotolain2').fileupload({
				url: url + '?state=fotolain2',
				dataType: 'json',
				done: function (e, data) {
					console.log(data.result.files);
					$.each(data.result.files, function (index, file) {
						// $('<p/>').text(file.name).appendTo('#files');
					});
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progressfotolain2 .progress-bar').css(
						'width',
						progress + '%'
					);
				}
			}).prop('disabled', !$.support.fileInput)
				.parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
	</script>
	
</body>

</html>
