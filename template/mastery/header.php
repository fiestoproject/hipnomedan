    <!-- Navigation -->
	<div id="content-blocker"></div>
    <nav id="header-site" class="navbar navbar-inverse" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo $cfg_app_url ?>">
					<img src="<?php echo $template->image_url('logo');?>" alt="Logo">
					<?php if(!$_GET['p']) {?>
						<span class="textlogo" ><img src="<?php echo $template->image_url('textlogo');?>"></span>
					<?php } else {?>
						<span class="textlogo" ><img src="<?php echo $template->image_url('textlogoother');?>"></span>
					<?php } ?>
				</a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
			<!--<div class="contact-header">
				<ul class="list-inline">
					<li>
						<img src="<<<TEMPLATE_URL>>>/images/iconHP.svg">
						<span>
							Telepon<br/>
							<a href="tel:<?php echo $template->content('no_telp_header');?>">
							<strong><?php echo $template->content('no_telp_header');?></strong>
							</a>
						</span>
					</li>
					<li>
						<img src="<<<TEMPLATE_URL>>>/images/iconMail.svg">
						<span>
							Email<br/>
							<a href="mailto:<?php echo $template->content('email_header');?>">
							<strong><?php echo $template->content('email_header');?></strong>
							</a>
						</span>
					</li>
				</ul>
			</div>-->
            <!-- Collect the nav links, forms, and other content for toggling -->
			<div class="bg-black"></div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	<?php if(!$_GET['p']) {?>
	<div id="slide-home" class="owl-carousel owl-theme">	
		<div class="item">
			<img src="<?php echo $template->image_url('slide1_img');?>" alt="Slide Image">
			<img class="slide-mobile" src="<?php echo $template->image_url('slidemobile1_img');?>" alt="Slide Image">

		</div>	
		<div class="item">
			<img src="<?php echo $template->image_url('slide2_img');?>" alt="Slide Image">
			<img class="slide-mobile" src="<?php echo $template->image_url('slidemobile2_img');?>" alt="Slide Image">

		</div>	
		<div class="item">
			<img src="<?php echo $template->image_url('slide3_img');?>" alt="Slide Image">
			<img class="slide-mobile" src="<?php echo $template->image_url('slidemobile3_img');?>" alt="Slide Image">
			
		</div>
	</div>
	<?php } ?>
	<div id="menu-block">
		<div class="container">
			<div class="block-menu-line">
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<div id="close-menu">✕</div>
					<?php echo $display_menu; ?>
				</div>
				<!--<div id="social">
					<ul class="list-inline">
						<li><a href="<?php echo $template->content('link_fb');?>"><img src="<<<TEMPLATE_URL>>>/images/iconMail.svg"></a></li>
						<li><a href="<?php echo $template->content('link_ig');?>"><img src="<<<TEMPLATE_URL>>>/images/iconMail.svg"></a></li>
						<li><a href="<?php echo $template->content('link_youtube');?>"><img src="<<<TEMPLATE_URL>>>/images/iconMail.svg"></a></li>
						<li><a href="<?php echo $template->content('link_google');?>"><img src="<<<TEMPLATE_URL>>>/images/iconMail.svg"></a></li>
					</ul>
				</div>-->
			</div>
		</div>
	</div>
