<?php

// $data["Top Header"]=array(
		// "no_telp_header"=>array("label"=>"No Telp","type"=>"inputText"),
		// "email_header"=>array("label"=>"Email","type"=>"inputText"),
		// "link_fb"=>array("label"=>"Facebook","type"=>"inputText"),
		// "link_ig"=>array("label"=>"Instagram","type"=>"inputText"),
		// "link_youtube"=>array("label"=>"Youtube","type"=>"inputText"),
		// "link_google"=>array("label"=>"Google+","type"=>"inputText"),
// );


$data["header"]=array(
		"logo"=>array("label"=>"Logo","type"=>"inputImage"),
		"textlogo"=>array("label"=>"Logo Teks Home","type"=>"inputImage"),
		"textlogoother"=>array("label"=>"Logo Teks Other","type"=>"inputImage"),
);

$data["slide1"]=array(
		"slide1_img"=>array("label"=>"Gambar Slide 1","type"=>"inputImage"),
		"slidemobile1_img"=>array("label"=>"Gambar Slide Mobile 1","type"=>"inputImage"),
		// "slide1_title"=>array("label"=>"Judul","type"=>"inputText"),
		// "slide1_content"=>array("label"=>"Content","type"=>"inputText"),
);

$data["slide2"]=array(
		"slide2_img"=>array("label"=>"Gambar Slide 2","type"=>"inputImage"),
		"slidemobile2_img"=>array("label"=>"Gambar Slide Mobile 2","type"=>"inputImage"),
		// "slide2_title"=>array("label"=>"Judul","type"=>"inputText"),
		// "slide2_content"=>array("label"=>"Content","type"=>"inputText"),
);

$data["slide3"]=array(
		"slide3_img"=>array("label"=>"Gambar Slide 3","type"=>"inputImage"),
		"slidemobile3_img"=>array("label"=>"Gambar Slide Mobile 3","type"=>"inputImage"),
		// "slide3_title"=>array("label"=>"Judul","type"=>"inputText"),
		// "slide3_content"=>array("label"=>"Content","type"=>"inputText"),
);


// $data["Call to Action"]=array(
		// "title_call_block"=>array("label"=>"Judul","type"=>"inputText"),
		// "link_call_block"=>array("label"=>"Link Url","type"=>"inputText"),
		// "teks_call_block"=>array("label"=>"Teks Button","type"=>"inputText"),
// );

// $data["Strength Point"]=array(
		// "strength_img_1"=>array("label"=>"Image Strength 1","type"=>"inputImage"),
		// "strength_title_1"=>array("label"=>"Title Strength 1","type"=>"inputText"),
		// "strength_content_1"=>array("label"=>"Content Strength 1","type"=>"inputText"),
		// "strength_img_2"=>array("label"=>"Image Strength 2","type"=>"inputImage"),
		// "strength_title_2"=>array("label"=>"Title Strength 2","type"=>"inputText"),
		// "strength_content_2"=>array("label"=>"Content Strength 2","type"=>"inputText"),
		// "strength_img_3"=>array("label"=>"Image Strength 3","type"=>"inputImage"),
		// "strength_title_3"=>array("label"=>"Title Strength 3","type"=>"inputText"),
		// "strength_content_3"=>array("label"=>"Content Strength 3","type"=>"inputText"),
// );

$data["Who We Are"]=array(
		"image_who_block"=>array("label"=>"Image","type"=>"inputImage"),
		"title_who_block"=>array("label"=>"Title","type"=>"inputText"),
		"content_who_block"=>array("label"=>"Content","type"=>"inputTiny"),
		"button_who_block"=>array("label"=>"Teks Button","type"=>"inputText"),
		"link_who_block"=>array("label"=>"Link Url","type"=>"inputText"),
);

/*Cara buat template option untuk category gallery*/
// $cat_news=$mysql->query_data("SELECT id,nama FROM gallerycat");
// $data["blog_gallery"]=array(
		// "gallery_judul"=>array("label"=>"Judul","type"=>"inputText"),
		// "gallery_category"=>array("label"=>"Section Gallery","type"=>"inputSelect","data"=>$cat_news),
		// "gallery_limit"=>array("label"=>"Max Thumbnail","type"=>"inputText"),
// );
/*End cara buat template option untuk category gallery*/

/*Cara buat template option untuk category news*/
// $cat_news=$mysql->query_data("SELECT id,nama FROM newscat");
// $data["blog_news"]=array(
		// "bn_judul"=>array("label"=>"Judul","type"=>"inputText"),
		// "bn_category"=>array("label"=>"Section News","type"=>"inputSelect","data"=>$cat_news),
		// "bn_limit"=>array("label"=>"Max Thumbnail","type"=>"inputText"),
// );

// $data["agenda"]=array(
		// "ag_judul"=>array("label"=>"Judul","type"=>"inputText"),
		// "ag_category"=>array("label"=>"Section News","type"=>"inputSelect","data"=>$cat_news),
		// "ag_limit"=>array("label"=>"Max Thumbnail","type"=>"inputText"),
// );
/*End cara buat template option untuk category news*/


$data["Footer"]=array(
		"bg_footerlink"=>array("label"=>"Background","type"=>"inputImage"),
		"footerlink_1"=>array("label"=>"Footer 1","type"=>"inputTiny"),
		"footerlink_2"=>array("label"=>"Footer 2","type"=>"inputTiny"),
		"footerlink_3"=>array("label"=>"Footer 3","type"=>"inputTiny"),
		"footerlink_4"=>array("label"=>"Footer 4","type"=>"inputTiny"),
		"copyright"=>array("label"=>"Copyright ","type"=>"inputTiny"),
);

$data["Mobile Link"]=array(
		"phone_number"=>array("label"=>"Telpon","type"=>"inputText"),
		"wa_number"=>array("label"=>"WA","type"=>"inputText"),
		"email_link"=>array("label"=>"Email","type"=>"inputText"),
		"map_link"=>array("label"=>"Map link","type"=>"inputText"),
		"line_link"=>array("label"=>"Line","type"=>"inputText"),
		"bbm_link"=>array("label"=>"BBM","type"=>"inputText"),
);



?>
