<?php
ob_start();
$template = new TemplateForm();

?>

    <!-- Page Content -->
	<!-- Slider Item -->
	
	<div id="who-we-are" class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 chat-bar-block wow fadeInUp ">
					<h1 class="page-header">Keefektifitasan Hipnoterapi Dibandingkan Metode Lainnya</h1>
					<div id="chart-bar">
						<div class="line-bar">
							<div class="title-bar">Hipnoterapi (6 Sesi)</div>
							<div class="bar-wrapper line1">
								<div class="persen-bar">93%</div>
							</div>
						</div>
						<div class="line-bar">
							<div class="title-bar">Behavioural Terapi (22 Sesi)</div>
							<div class="bar-wrapper line2">
								<div class="persen-bar">72%</div>
							</div>
						</div>
						<div class="line-bar">
							<div class="title-bar">Psikoanalisa (600 Sesi)</div>
							<div class="bar-wrapper line3">
								<div class="persen-bar">38%</div>
							</div>
						</div>
					</div>
					<p>Sumber: American Health Magazine "A Corporation Study"</p>
				</div>
				<div class="col-sm-6 content-desc wow fadeInUp">
					<div class="line-content">
						<h1 class="page-header"><?php echo $template->content('title_who_block');?></h1>
						<?php echo $template->content('content_who_block');?>
						<a href="<?php echo $template->content('link_who_block');?>" class="btn btn-default more"><?php echo $template->content('button_who_block');?></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<!--
	<div id="strengt" class="content-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-4 wow fadeInUp">
					<div class="strengt-item">
						<img src="<?php echo $template->image_url('strength_img_1');?>">
						<div class="title-strengt">
							<h4><?php echo $template->content('strength_title_1');?></h4>
							<p><?php echo $template->content('strength_content_1');?></p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 wow fadeInUp">
					<div class="strengt-item">
						<img src="<?php echo $template->image_url('strength_img_2');?>">
						<div class="title-strengt">
							<h4><?php echo $template->content('strength_title_2');?></h4>
							<p><?php echo $template->content('strength_content_2');?></p>
						</div>
					</div>
				</div>
				<div class="col-sm-6 col-md-4 wow fadeInUp">
					<div class="strengt-item">
						<img src="<?php echo $template->image_url('strength_img_3');?>">
						<div class="title-strengt">
							<h4><?php echo $template->content('strength_title_3');?></h4>
							<p><?php echo $template->content('strength_content_3');?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	-->
	


<?php
$display_main_content_block .= ob_get_clean();
?>
