<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

error_reporting(E_ALL | E_STRICT);
require('../../kelola/fungsi.php');
require('UploadHandler.php');

$state = fiestolaundry($_GET['state'], 15);

if ($state == 'pasfoto') {
	$request = 'pasfoto';
} else if ($state == 'fotoktp') {
	$request = 'fotoktp';
} else if ($state == 'fotosertifikat') {
	$request = 'fotosertifikat';
} else if ($state == 'fotolain1') {
	$request = 'fotolain1';
} else if ($state == 'fotolain2') {
	$request = 'fotolain2';
}

$options = array('param_name' => $request);
$upload_handler = new UploadHandler($options);