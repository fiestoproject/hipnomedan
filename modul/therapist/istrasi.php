<?php

if (!$isloadfromindex) {
    include ("../../kelola/urasi.php");
    include ("../../kelola/fungsi.php");
    include ("../../kelola/lang/$lang/definisi.php");
    pesan(_ERROR, _NORIGHT);
}

$keyword = fiestolaundry($_GET['keyword'], 100);
$screen = fiestolaundry($_GET['screen'], 11);
$pid = fiestolaundry($_REQUEST['pid'], 11);

$action = fiestolaundry($_REQUEST['action'], 10);
$nama = fiestolaundry($_POST['nama'], 200);
$alamat = fiestolaundry($_POST['alamat'], 200);
$telepon = fiestolaundry($_POST['telepon'], 15);
$email = fiestolaundry($_POST['email'], 255);
$uraian = fiestolaundry($_POST['uraian'], 0, true);
$izin_praktek = fiestolaundry($_POST['izin_praktek'], 0, true);
$sertifikasi = fiestolaundry($_POST['sertifikasi'], 0, true);
$publish = fiestolaundry($_POST['publish'], 11);
$url_ig = fiestolaundry($_POST['url_ig'], 255);
$url_fb = fiestolaundry($_POST['url_fb'], 255);
$nama_ig = fiestolaundry($_POST['nama_ig'], 200);
$nama_fb = fiestolaundry($_POST['nama_fb'], 200);

//$notification = fiestolaundry(($_GET['notification']));

$modulename = $_GET['p'];
if (isset($_POST['back'])) {
    back($modulename);
}

if ($action == "save") {
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($nama, _NAME, false);
        $notificationbuilder .= validation($email, _EMAIL, false);
        // $notificationbuilder .= validation($product_id, _TUNINGGUIDEPRODUCT, false);
		if ($email != '') {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			  $notificationbuilder .= $email . _NOTVALIDEMAIL;
			}
		}
		
		if ($_FILES['filename']['error'] != UPLOAD_ERR_NO_FILE) {
			//jika kolom file diisi
			//upload dulu sebelum insert record
			$hasilupload = fiestoupload('filename', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if ($hasilupload != _SUCCESS) {
				$notificationbuilder = $hasilupload;
			} else {
				//ambil informasi basename dan extension
				$temp = explode(".", $_FILES['filename']['name']);
				$extension = $temp[count($temp) - 1];
				$basename = '';
				for ($i = 0; $i < count($temp) - 1; $i++) {
					$basename .= $temp[$i];
				}
				
			}
		}
			
        if ($notificationbuilder != "") {
            $action = createmessage($notificationbuilder, _ERROR, "error", "add");
        } else {
			
			$sql = "INSERT INTO therapistdata (nama, alamat, telepon, email, izin_praktek, sertifikasi, uraian, publish, url_ig, nama_ig, url_fb, nama_fb) VALUES ('$nama', '$alamat', '$telepon', '$email', '$izin_praktek', '$sertifikasi', '$uraian', '$publish', '$url_ig', '$nama_ig', '$url_fb', '$nama_fb')";
			if ($mysql->query($sql)) {
				$newid = $mysql->insert_id();
				$modifiedfilename = "$basename-$newid.$extension";
				
				list($filewidth, $fileheight, $filetype, $fileattr) = getimagesize("$cfg_fullsizepics_path/" . $_FILES['filename']['name']);
				//create thumbnail
				$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
				if ($hasilresize != _SUCCESS)
				$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
							
				if ($filewidth > $cfg_max_width) { //rename sambil resize gambar asli sesuai yang diizinkan
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename", 'w', $cfg_max_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
					//del gambar asli
					unlink("$cfg_fullsizepics_path/" . $_FILES['filename']['name']);
				} else { //create thumbnail
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
					rename("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename");
				}
				
				$sql = "UPDATE therapistdata SET filename='$modifiedfilename' WHERE id='$newid'";
				$mysql->query($sql);
									
				$action = createmessage(_DBSUCCESS, _SUCCESS, "success", "");
			} else {
				$action = createmessage(_DBERROR, _ERROR, "error", "add");
			}
        }
    } else {
        /* default value untuk form */
        $action = "";
    }
}

if ($action == "add") {
    $admintitle = _ADDLITERATUR;

    $admincontent .= '
	<form class="form-horizontal" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
            <input type="hidden" name="action" value="save">
			<div class="control-group">
			<label class="control-label">' . _FILENAME . ':</label>
			<div class="controls">
				<div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden"  name="filename" size="35" />
					<div class="input-append">
						<div class="uneditable-input span2">
							<i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span>
						</div>
						<span class="btn btn-file"><span class="fileupload-new">Cari File</span><span class="fileupload-exists">'._CHANGE.'</span>
						<input type="file" name="filename" size="35">
						</span><a data-dismiss="fileupload" class=\btn fileupload-exists" href="#">'._REMOVE.'</a>
					</div>
				</div>
			</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _NAME . '*</label>
				<div class="controls">
					<input type="text" id="nama" name="nama" placeholder="' . _NAME . '" class="span12">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _ADDRESS . '</label>
				<div class="controls">
					<textarea id="alamat" name="alamat" rows="5" cols="10" class="span12"></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _PHONE . '</label>
				<div class="controls">
					<input type="text" id="telepon" name="telepon" placeholder="' . _PHONE . '" class="span12">
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">' . _EMAIL . '*</label>
				<div class="controls">
					<input type="text" id="email" name="email" placeholder="' . _EMAIL . '" class="span12">
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">' . _IZINPRAKTEK . '</label>
				<div class="controls">
					<textarea id="izin_praktek" name="izin_praktek" rows="5" cols="10" class="span12 usetiny"></textarea>
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">' . _SERTIFICATE . '</label>
				<div class="controls">
					<textarea id="sertifikasi" name="sertifikasi" rows="5" cols="10" class="span12 usetiny"></textarea>
				</div>
			</div>		
			<div class="control-group">
				<label class="control-label">' . _DESCRIPTION . '</label>
				<div class="controls">
					<textarea id="uraian" name="uraian" rows="5" cols="10" class="span12 usetiny"></textarea>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _URLIG . '</label>
				<div class="controls">
					<input type="text" id="url_ig" name="url_ig" placeholder="' . _URLIG . '" class="span12">
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">' . _NAMAIG . '</label>
				<div class="controls">
					<input type="text" id="nama_ig" name="nama_ig" placeholder="' . _NAMAIG . '" class="span12">
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">' . _URLFB . '</label>
				<div class="controls">
					<input type="text" id="url_fb" name="url_fb" placeholder="' . _URLFB . '" class="span12">
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">' . _NAMAFB . '</label>
				<div class="controls">
					<input type="text" id="nama_fb" name="nama_fb" placeholder="' . _NAMAFB . '" class="span12">
				</div>
			</div>	
			<div class="control-group">
				<label class="control-label">' . _PUBLISH . '</label>
				<div class="controls">
					<input type="checkbox" id="publish" name="publish" value="1" checked>
				</div>
			</div>			
            <div class="control-group">
				<div class="controls">
					<input type="submit" name="submit" class="buton" value="' . _ADD . '">
					<input type="submit" name="back" class="buton" value="' . _BACK . '">
				</div>
            </div>
	</form>
    ';
}

if ($action == "update") {
    
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($nama, _NAME, false);
        $notificationbuilder .= validation($email, _EMAIL, false);
		
		if ($email != '') {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			  $notificationbuilder .= $email . _NOTVALIDEMAIL;
			}
		}
		
		if ($_FILES['filename']['error'] != UPLOAD_ERR_NO_FILE) {
			//jika kolom file diisi
			//upload dulu sebelum insert record
			$hasilupload = fiestoupload('filename', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if ($hasilupload != _SUCCESS) {
				$notificationbuilder = $hasilupload;
			} else {
				//ambil informasi basename dan extension
				$temp = explode(".", $_FILES['filename']['name']);
				$extension = $temp[count($temp) - 1];
				$basename = '';
				for ($i = 0; $i < count($temp) - 1; $i++) {
					$basename .= $temp[$i];
				}
				
				$modifiedfilename = "$basename-$pid.$extension";
				
				//create thumbnail
				$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
				if ($hasilresize != _SUCCESS)
				$notificationbuilder = createmessage($hasilresize, _ERROR, "modify");

				if ($filewidth > $cfg_max_width) { //rename sambil resize gambar asli sesuai yang diizinkan
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename", 'w', $cfg_max_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "modify");
					//del gambar asli
					unlink("$cfg_fullsizepics_path/" . $_FILES['filename']['name']);
				} else { //create thumbnail
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "modify");
					rename("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename");
				}
				
				//del gambar yang dioverwrite (hanya jika filename beda)
				$sql = "SELECT filename FROM therapistdata WHERE id='$pid'";
				$result = $mysql->query($sql);
				list($oldfilename) = $mysql->fetch_row($result);
				if ($modifiedfilename != $oldfilename) {
					if ($oldfilename != '' && file_exists("$cfg_fullsizepics_path/$oldfilename")) unlink("$cfg_fullsizepics_path/$oldfilename");
					if ($oldfilename != '' && file_exists("$cfg_thumb_path/$oldfilename")) unlink("$cfg_thumb_path/$oldfilename");
				}
			}
			
			$sql = "UPDATE therapistdata SET filename='$modifiedfilename' WHERE id='$pid'";
			$result = $mysql->query($sql);
		}
		
        if ($notificationbuilder != "") {
            // $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
			$action = createmessage($notificationbuilder, _ERROR, "error", "modify");	

        } else {
			
			$sql = "UPDATE therapistdata SET nama='$nama', alamat='$alamat', telepon='$telepon', email='$email', izin_praktek='$izin_praktek', sertifikasi='$sertifikasi', uraian='$uraian', publish='$publish', url_ig='$url_ig', nama_ig='$nama_ig', url_fb='$url_fb', url_ig='$url_fb'  WHERE id='$pid'";
			$result = $mysql->query($sql);
			
			if ($result) {
				$action = createmessage(_DBSUCCESS, _SUCCESS, "success", "");	
			} else {
				$action = createmessage(_DBERROR, _ERROR, "error", "modify");	
			}
        }
    } else {
        /* default value untuk form */
        $action = "modify";
    }
}

if ($action == "modify") {
	$sql  = "SELECT *
			FROM therapistdata
			WHERE id='$pid'";
    $result = $mysql->query($sql);
    $admintitle = _EDIT;
    if ($mysql->num_rows($result) > 0) {
        // list($id, $filename, $penulis, $penerbit, $jumlah_halaman, $status, $judul_buku) = $mysql->fetch_row($result);
		$row = $mysql->fetch_assoc($result);
		extract($row);
		
		if ($filename != '' && file_exists("$cfg_thumb_path/$filename")) {
			$admincontent .= "<div class=\"img-filename\">
			<img src=\"$cfg_thumb_url/$filename\">
			</div>";
		}
		
		$admincontent .= '
		<form class="form-horizontal" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
				<input type="hidden" name="action" value="update">
				<div class="control-group">
				<label class="control-label">' . _FILENAME . ':</label>
				<div class="controls">
					<div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden"  name="filename" size="35" />
						<div class="input-append">
							<div class="uneditable-input span2">
								<i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span>
							</div>
							<span class="btn btn-file"><span class="fileupload-new">Cari File</span><span class="fileupload-exists">'._CHANGE.'</span>
							<input type="file" name="filename" size="35">
							</span><a data-dismiss="fileupload" class=\btn fileupload-exists" href="#">'._REMOVE.'</a>
						</div>
					</div>
				</div>
				</div>
				<div class="control-group">
					<label class="control-label">' . _NAME . '*</label>
					<div class="controls">
						<input type="text" id="nama" name="nama" placeholder="' . _NAME . '" class="span12" value="'.$nama.'">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">' . _ADDRESS . '</label>
					<div class="controls">
						<textarea id="alamat" name="alamat" rows="5" cols="10" class="span12">'.$alamat.'</textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">' . _PHONE . '</label>
					<div class="controls">
						<input type="text" id="telepon" name="telepon" placeholder="' . _PHONE . '" class="span12" value="'.$telepon.'">
					</div>
				</div>	
				<div class="control-group">
					<label class="control-label">' . _EMAIL . '*</label>
					<div class="controls">
						<input type="text" id="email" name="email" placeholder="' . _EMAIL . '" class="span12" value="'.$email.'">
					</div>
				</div>	
				<div class="control-group">
					<label class="control-label">' . _IZINPRAKTEK . '</label>
					<div class="controls">
						<textarea id="izin_praktek" name="izin_praktek" rows="5" cols="10" class="span12 usetiny">'.$izin_praktek.'</textarea>
					</div>
				</div>	
				<div class="control-group">
					<label class="control-label">' . _SERTIFICATE . '</label>
					<div class="controls">
						<textarea id="sertifikasi" name="sertifikasi" rows="5" cols="10" class="span12 usetiny">'.$sertifikasi.'</textarea>
					</div>
				</div>		
				<div class="control-group">
					<label class="control-label">' . _DESCRIPTION . '</label>
					<div class="controls">
						<textarea id="uraian" name="uraian" rows="5" cols="10" class="span12 usetiny">'.$uraian.'</textarea>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">' . _URLIG . '</label>
					<div class="controls">
						<input type="text" id="url_ig" name="url_ig" placeholder="' . _URLIG . '" class="span12" value="'.$url_ig.'">
					</div>
				</div>	
				<div class="control-group">
					<label class="control-label">' . _NAMAIG . '</label>
					<div class="controls">
						<input type="text" id="nama_ig" name="nama_ig" placeholder="' . _NAMAIG . '" class="span12" value="'.$nama_ig.'">
					</div>
				</div>	
				<div class="control-group">
					<label class="control-label">' . _URLFB . '</label>
					<div class="controls">
						<input type="text" id="url_fb" name="url_fb" placeholder="' . _URLFB . '" class="span12" value="'.$url_fb.'">
					</div>
				</div>	
				<div class="control-group">
					<label class="control-label">' . _NAMAFB . '</label>
					<div class="controls">
						<input type="text" id="nama_fb" name="nama_fb" placeholder="' . _NAMAFB . '" class="span12" value="'.$nama_fb.'">
					</div>
				</div>	
				<div class="control-group">
					<label class="control-label">' . _PUBLISH . '</label>
					<div class="controls">
						<input type="checkbox" id="publish" name="publish" value="1"
						';
						if ($publish == 1) {
							$admincontent .= 'checked="checked"';
						}
						$admincontent .= '/>
					</div>
				</div>
				<div class="control-group">
					<div class="controls">
						<input type="submit" name="submit" class="buton" value="' . _SAVE . '">
						<input type="submit" name="back" class="buton" value="' . _BACK . '">
					</div>
				</div>
		</form>
		';
    } else {
        $admincontent .= createstatus(_NOBRAND, "error");
    }
}

// DELETE BRAND
if ($action == "remove" || $action == "delete") {
    $admintitle = _DELBRAND;
    $sql = "SELECT id, filename FROM therapistdata WHERE id='$pid'";
    $result = $mysql->query($sql);
    if ($mysql->num_rows($result) == "0") {
        $action = createmessage(_NOBRAND, _INFO, "info", "");
    } else {
        list($id, $filename) = $mysql->fetch_row($result);
        if ($action == "remove") {
            $admincontent .= "<h5>" . _PROMPTDEL . "</h5>";
            $admincontent .= '<div class="control-group">
                                <div class="controls">
                                        <a class="buton" href="?p=' . $modulename . '&action=delete&pid=' . $id . '">' . _YES . '</a>
                                        <a class="buton" href="javascript:history.go(-1)">' . _NO . '</a></p>
                                </div>
                        </div>';
        } else {
			
			if ($filename != '' && file_exists("$cfg_fullsizepics_path/$filename")) unlink("$cfg_fullsizepics_path/$filename");
            if ($filename != '' && file_exists("$cfg_thumb_path/$filename")) unlink("$cfg_thumb_path/$filename");
					
            $sql = "DELETE FROM therapistdata WHERE id='$pid'";
            $result = $mysql->query($sql);
            if ($result) {
                $action = createmessage(_DELETESUCCESS, _SUCCESS, "success", "");
            } else {
                $action = createmessage(_DBERROR, _ERROR, "error", "modify");
            }
        }
    }
}

if ($action == "" || $action == "search") {
    
	$admintitle = "";
	if ($action == "search") {
		$admintitle = _SEARCHRESULTS;
	}
	
	$sql  = "SELECT * FROM therapistdata";
	if ($action == "search") {
		$sql .= " WHERE nama LIKE '%$keyword%' OR email LIKE '%$keyword%' OR telepon LIKE '%$keyword%'";
	}
    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($total_records > 0) {
        $start = $screen * $max_page_list;

		$sql  = "SELECT * FROM therapistdata ";
		if ($action == "search") {
			$sql .= " WHERE nama LIKE '%$keyword%' OR email LIKE '%$keyword%' OR telepon LIKE '%$keyword%'";
		}
		
		$sql .= " ORDER BY nama ";
		$sql .= " LIMIT $start, $max_page_list ";
        $result = $mysql->query($sql);

        if ($pages > 1) {
            $adminpagination = pagination($namamodul, $screen);
        }
        $admincontent .= "<table class=\"stat-table table table-stats table-striped table-sortable table-bordered\">\n";
        $admincontent .= "<tr><th>" . _NAME . "</th><th>" . _EMAIL . "</th><th>" . _PHONE . "</th>";
        $admincontent .= "<th align=\"center\">" . _ACTION . "</th></tr>\n";
        while ($row = $mysql->fetch_assoc($result)) {
			extract($row);
			$titleurl = array();
			$titleurl['cat_id'] = $car_brand;
			$url = $urlfunc->makePretty("?p=literatur&action=list&cat_id=$car_id", $titleurl);
            $admincontent .= "<tr class=\"merek\">\r\n";
            $admincontent .= "<td>$nama</td>";
            $admincontent .= "<td>$email</td>\r\n";
            $admincontent .= "<td>$telepon</td>\r\n";
            // $admincontent .= "<td>$jumlah_halaman</td>\r\n";
            $admincontent .= "<td align=\"center\" class=\"action-ud\">";
            $admincontent .= "<a href=\"?p=$modulename&action=modify&pid=$id\"><img alt=\"" . _EDIT . "\" border=\"0\" src=\"../images/modify.gif\"></a>\r\n";
            $admincontent .= "<a href=\"?p=$modulename&action=remove&pid=$id\"><img alt=\"" . _DEL . "\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            $admincontent .= "</tr>\n";
        }
        $admincontent .= "</table>";
    } else {
        createnotification(_NOLITERATUR, _INFO, "info");
    }
    $admincontent .= "<a href=\"?p=$modulename&action=add\" class=\"buton\">" . _ADDLITERATUR . "</a>";
}

// if ($action == 'search') {
    // $admintitle = _SEARCHRESULTS;

	// $sql  = "SELECT id, penulis, jumlah_halaman, penerbit, judul_buku
			 // FROM literatur";
    // $result = $mysql->query($sql);
    // $total_records = $mysql->num_rows($result);
    // $pages = ceil($total_records / $max_page_list);

    // if ($total_records > 0) {
        // $start = $screen * $max_page_list;
        // // $sql = "SELECT id, c.nama FROM literatur c INNER JOIN car_brand b ON b.id=c.brand_id WHERE (nama LIKE '%$keyword%') ORDER BY nama LIMIT $start, $max_page_list ";
		// $sql  = "SELECT id, penulis, jumlah_halaman, penerbit, judul_buku
					// FROM literatur
					// WHERE penulis LIKE '%$keyword%' OR judul_buku LIKE '%$keyword%' OR penerbit LIKE '%$keyword%'
					// ORDER BY id
					// LIMIT $start, $max_page_list";
        // $result = $mysql->query($sql);
        // if ($pages > 1) {
            // $adminpagination = pagination($namamodul, $screen);
        // }
        // $admincontent .= "<table class=\"stat-table table table-stats table-striped table-sortable table-bordered\">\n";
        // $admincontent .= "<tr><th>" . _PENULIS . "</th><th>" . _JUDULBUKU . "</th><th>" . _PENERBIT . "</th><th>" . JUMLAHHALAMAN . "</th>";
        // $admincontent .= "<th align=\"center\">" . _ACTION . "</th></tr>\n";
        // while (list($id, $penulis, $jumlah_halaman, $penerbit, $judul_buku) = $mysql->fetch_row($result)) {
			// $titleurl = array();
			// $titleurl['cat_id'] = $car_brand;
			// $url = $urlfunc->makePretty("?p=literatur&action=list&cat_id=$car_id", $titleurl);
            // $admincontent .= "<tr class=\"merek\">\r\n";
            // $admincontent .= "<td>$penulis</td>";
            // $admincontent .= "<td>$judul_buku</td>\r\n";
            // $admincontent .= "<td>$penerbit</td>\r\n";
            // $admincontent .= "<td>$jumlah_halaman</td>\r\n";
            // $admincontent .= "<td align=\"center\" class=\"action-ud\">";
            // $admincontent .= "<a href=\"?p=$modulename&action=modify&pid=$id\"><img alt=\"" . _EDIT . "\" border=\"0\" src=\"../images/modify.gif\"></a>\r\n";
            // $admincontent .= "<a href=\"?p=$modulename&action=remove&pid=$id\"><img alt=\"" . _DEL . "\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            // $admincontent .= "</tr>\n";
        // }
        // $admincontent .= "</table>";
    // } else {//menampilkan record di halaman yang sesuai
        // createnotification(_NOSEARCHRESULTS, _INFO, "info");
    // }
    // $admincontent .= "<a href=\"?p=$modulename\" class=\"buton\">" . _BACK . "</a>";
// }
?>
