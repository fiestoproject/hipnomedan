<?php 

$action = fiestolaundry($_GET['action'], 10);
$pid = fiestolaundry($_GET['pid'], 11);
$screen = fiestolaundry($_GET['screen'],11);
$now = date('Y-m-d');

if ($screen == '') $screen = 0;

$sql = "SELECT judulfrontend FROM module WHERE nama='therapistdata'";
$result = $mysql->query($sql);
list($title) = $mysql->fetch_row($result);

if ($action == '' || $action == 'list') {
	
	$sql = "SELECT * FROM therapistdata WHERE publish=1 ";
	$result = $mysql->query($sql);
	$total_pages = $mysql->num_rows($result);
	$pages = ceil($total_pages/$max_per_page);
	
	if ($total_pages > 0) {
		
		$start = $screen * $max_per_page;
		$sql .= " ORDER BY nama ASC ";
		$sql .= " LIMIT $start, $max_per_page";
		
		$result = $mysql->query($sql);
		
		if ($pages>1) $catpage = aksipagination($namamodul, $screen, "action=list");	
		$content .= ( $catpage != '') ? "<div class=\"catpage\">$catpage</div>\r\n" : "";
		
		$content .= '<div class="row">';
		while($row = $mysql->fetch_assoc($result)) {
			extract($row);
			
			$titleurl = array();
			$titleurl['pid'] = $nama;
			
			$thumbnail = ($filename != '' && file_exists("$cfg_thumb_path/$filename")) ? "$cfg_thumb_url/$filename" : "$cfg_app_url/images/noel.jpg";
			$url = $urlfunc->makePretty("?p=therapist&action=view&pid=$id", $titleurl);
			$url_contact = $urlfunc->makePretty("?p=therapist&action=contact&pid=$id", $titleurl);
			
			$url_ig = $url_ig != '' && $nama_ig != '' ? '<li><a href="'.$url_ig.'"><img src="'.$cfg_app_url.'/images/ig.png" />'.$nama_ig.'</a></li>' : '';
			$url_fb = $url_fb != '' && $nama_fb != '' ? '<li><a href="'.$nama_fb.'"><img src="'.$cfg_app_url.'/images/fb.png" />'.$nama_fb.'</a></li>' : '';
			
			$content .= '
			<div class="col-md-4">
				<div id="list-terapis">
					<div class="thumbnail-terapis">
						<div class="images-terapis">
							<img src="'.$thumbnail.'" src="'.$nama.'"/>
						</div>
						<h2 class="nama-terapis">'.$nama.'</h2>
						<ul class="sosmed-terapis">
							'.$url_ig.$url_fb.'
						</ul>
						<a href="'.$url_contact.'" class="btn btn-default more">'._CONTACTUS.'</a>
						<a class="link-default" href="'.$url.'">'._READMORE.' &gt;</a>
					</div>
				</div>
			</div>';

			// $content .= "					
				// <div class=\"col-sm-6\">
					// <div class=\"book-list\">";
						// $content .= "
								// <div class=\"img-book\">
									// <a class=\"image-popup\" href=\"$cfg_fullsizepics_url/$filename\" title=\"$penulis\">
										// <img class=\"img-responsive\" alt=\"$penulis\" src=\"$thumbnail\">
									// </a>
								// </div>	<!-- /.img-news -->\r\n";
						// $content .= "<div class=\"book-content\">";
						// $content .= "<div class=\"booktitle\">$nama</a></div>";
						// $content .= "<div class=\"bookpenulis\">$penulis</div>";
						// if ($izin_praktek != '') $content .= "<div class=\"bookpenerbit\">$izin_praktek</div>";
						// if ($sertifikasi != '') $content .= "<div class=\"bookpage\">$sertifikasi</div>";
						// $content .= "<a href=\"$url\">"._READMORE."</a>";
						// $content .= "
						// </div>
					// </div>	<!-- /.panel-body -->\r\n
				// </div>	<!-- /.col-sm-6 col-md-6 -->";	
		}
		
		$content .= '</div>';
	}
	
}

if ($action == "view") {
	$sql = "SELECT * FROM therapistdata WHERE id='$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) > 0) {
		$row = $mysql->fetch_assoc($result);
		extract($row);
		
		// check_before_contact();
		
		$titleurl = array();
		$titleurl['pid'] = $nama;
		
		$thumbnail = ($filename != '' && file_exists("$cfg_thumb_path/$filename")) ? "$cfg_thumb_url/$filename" : "$cfg_app_url/images/noel.jpg";
		$url = $urlfunc->makePretty("?p=therapist&action=view&pid=$id", $titleurl);
		$url_contact = $urlfunc->makePretty("?p=therapist&action=contact&pid=$id", $titleurl);
		
		$url_ig = $url_ig != '' && $nama_ig != '' ? '<li><a href="'.$url_ig.'"><img src="'.$cfg_app_url.'/images/ig.png" />'.$nama_ig.'</a></li>' : '';
		$url_fb = $url_fb != '' && $nama_fb != '' ? '<li><a href="'.$nama_fb.'"><img src="'.$cfg_app_url.'/images/fb.png" />'.$nama_fb.'</a></li>' : '';
		
		// Left
		$content .= '
		<div class="col-md-3">
			<div class="thumbnail-terapis">
				<div class="images-terapis">
					<img src="'.$thumbnail.'" alt="'.$nama.'"/>
				</div>
				<ul class="sosmed-terapis">
					'.$url_ig.$url_fb.'
				</ul>
				<a href="'.$url_contact.'" class="btn btn-default more">'._CONTACTUS.'</a>
			</div>
		</div>
		';
		
		// Right
		$izin_praktek = $izin_praktek != '' ? $izin_praktek : '';
		$sertifikasi = $sertifikasi != '' ? $sertifikasi : '';
		$content .= '
		<div class="col-md-9 content-detail-terapist">
			<div class="certificate-terapis row">
				<div class="col-sm-6">
					'.$izin_praktek.'
				</div>
				<div class="col-sm-6">
					'.$sertifikasi.'
				</div>
			</div>
			<div class="description-terapis">
				'.$uraian.'
			</div>';
			
		$get_article_by_id = get_article_by_id($pid);
		$content .= $get_article_by_id;
			
		$content .= '
		</div>

		';
	} else {
		
		$content .= NOTFOUND;
	}
}

if ($action == 'contact') {
	$sql = "SELECT * FROM therapistdata WHERE id='$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) > 0) {
		$row = $mysql->fetch_assoc($result);
		extract($row);
		
		$title = $nama;
		$thumbnail = ($filename != '' && file_exists("$cfg_thumb_path/$filename")) ? "$cfg_thumb_url/$filename" : "$cfg_app_url/images/noel.jpg";
		$url = $urlfunc->makePretty("?p=therapist&action=view&pid=$id", $titleurl);
		
		$url_ig = $url_ig != '' && $nama_ig != '' ? '<li><a href="'.$url_ig.'"><img src="'.$cfg_app_url.'/images/ig.png" />'.$nama_ig.'</a></li>' : '';
		$url_fb = $url_fb != '' && $nama_fb != '' ? '<li><a href="'.$nama_fb.'"><img src="'.$cfg_app_url.'/images/fb.png" />'.$nama_fb.'</a></li>' : '';
		
		// Left
		$content .= '
		<div class="col-md-3">
			<div class="thumbnail-terapis">
				<a href="'.$url.'" class="button-back-profile"> < Kembali ke profile</a>
				<div class="images-terapis">
					<img src="'.$thumbnail.'" alt="'.$nama.'"/>
				</div>
				<ul class="sosmed-terapis">
					'.$url_ig.$url_fb.'
				</ul>
				<a href="" class="btn btn-default more">'._CONTACTUS.'</a>
			</div>
		</div>
		';
		
		// Right
		// $izin_praktek = $izin_praktek != '' ? $izin_praktek : '';
		// $sertifikasi = $sertifikasi != '' ? $sertifikasi : '';
		$url = $urlfunc->makePretty("?p=therapist&action=dosend");
		$content .= '
		<div class="col-md-9 content-detail-terapist">
			<div class="form-terapis">
				<form method="POST" action="'.$url.'">
					<input type="hidden" name="therapist_id" value="'.(int)$pid.'">
					<input type="hidden" name="therapist_name" value="'.$nama.'">
					<div class="form-group">
						<label>Nama</label>
						<input class="form-control" type="text" name="nama">
					</div>
					<div class="form-group">
						<label>Alamat</label>
						<textarea class="form-control" rows="5" name="alamat"></textarea>
					</div>
					<div class="form-group">
						<label>Kota</label>
						<input class="form-control" type="text" name="kota">
					</div>
					<div class="form-group">
						<label>Negara</label>
						<input class="form-control" type="text" name="negara">
					</div>
					<div class="form-group">
						<label>HP/WA</label>
						<input class="form-control" type="text" name="telepon">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input class="form-control" type="text" name="email">
					</div>
					<div class="form-group">
						<label>Berita</label>
						<textarea class="form-control" rows="7" name="berita"></textarea>
					</div>
					<div class="form-group">
						<input class="btn btn-default more" type="submit" name="submit" value="Kirim">
					</div>
					
				</form>
			</div>';
			
		$content .= '
		</div>

		';
	} else {
		
		$content .= NOTFOUND;
	}
}

if ($action == 'dosend') {
	
	$sql = "SELECT name, value FROM contact WHERE grup='1' ORDER BY indeks";
	$result = $mysql->query($sql);
	while (list($name,$value) = $mysql->fetch_row($result)) {
		$$name=$value;
	}
	
	// $nama = fiestolaundry($_POST['nama'], 100);
	// $alamat = fiestolaundry($_POST['alamat'], 100);
	// $kota = fiestolaundry($_POST['kota'], 100);
	// $negara = fiestolaundry($_POST['negara'], 100);
	// $telepon = fiestolaundry($_POST['telepon'], 100);
	// $berita = fiestolaundry($_POST['berita'], 500);
	
	$email = fiestolaundry($_POST['email'], 100);
	$therapist_name = get_therapist_name($email);	//fiestolaundry($_POST['therapist_name'], 100);
	$therapist_id = fiestolaundry($_POST['therapist_id'], 11);
	
	$errors = array();
	
	$required = array('nama:100', 'alamat:100', 'kota:100', 'negara:100', 'telepon:100', 'email:255', 'berita:500', 'therapist_id:11');
	for($i = 0; $i < count($required); $i++) {
		$req_arr = explode(':', $required[$i]);
		$field = fiestolaundry($_POST[$req_arr[0]], $req_arr[1]);
		if ($field == '') {
			$errors[] = ucfirst($req_arr[0]) . " wajib diisi!";
		}
		
		if ($req_arr[0] == 'email' && $field != '') {
			if (!filter_var($field, FILTER_VALIDATE_EMAIL)) {
			  $errors[] = "$field email tidak valid";
			}
		}
		
	}

	if (count($errors) > 0) {
		$content .= '<ul>';
		foreach($errors as $error) {
			$content .= '<li>'.$error.'</li>';
		}
		$content .= '</ul>';
		$content .= '<a href="javascript:history.go(-1)">'._BACK.'</a>';
		
	} else {
		
		$isiemail = '';
		for($i = 0; $i < count($required); $i++) {
			$req_arr = explode(':', $required[$i]);
			$field = fiestolaundry($_POST[$req_arr[0]], $req_arr[1]);
			$name = ucfirst($req_arr[0]);
			
			if ($req_arr[0] != 'therapist_id' && $req_arr[0] != 'email' && $req_arr[0] != 'berita') {
				$isiemail .= "$name: $field\r\n";
			} 
			if ($req_arr[0] == 'email') {
				$isiemail .= "$name: $field\r\n\r\n";
			} 
			if ($req_arr[0] == 'berita') {
				$isiemail .= "$field\r\n";
			}
		}
		
		// var_dump(check_before_contact($email, $therapist_id));
		// die();
		
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		
		if (check_before_contact($email, /* $therapist_id, */ $user_agent)) {
			$umbalemail = 'rumahsaudagarkaya@gmail.com';
			if (fiestophpmailer($umbalemail, _SUBJECT, $isiemail, $smtpuser, $sendername, $email)) {
				
				$sql = "INSERT INTO therapistemail (therapist_id, email, tgl, user_agent) VALUES ('$therapist_id', '$email', '$now', '$user_agent')";
				$mysql->query($sql);
				$content .= "<div id=\"formstatus\">\n";
				$content .= "<p>$msgok</p>";
				$content .= "<p><a href=\"javascript:history.go(-1)\">$msgback</a></p>\n";
				$content .= "</div>\n";	
			}
		} else {
			
			$therapist_alert = "Anda telah menghubungi terapis <strong>$therapist_name</strong>, mohon tunggu 1 bulan untuk bisa menghubungi terapis.";
			$content .= '<p>'.$therapist_alert.'</p>';
			$content .= '<a href="javascript:history.go(-1)">'._BACK.'<a>';
		}
		
	}
	
}