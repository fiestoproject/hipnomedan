<?php 
include "../../kelola/urasi.php";
include "../../kelola/fungsi.php";

$action = $_GET['action'];

if ($action == 'get_store') {
	$query = fiestolaundry($_GET['query'], 50);
	$sql = "SELECT s.id, bengkel, k.name AS kota
			FROM cellostore s 
			INNER JOIN kabupaten k ON k.id=s.kota_id
			WHERE bengkel LIKE '%$query%' OR k.name LIKE '%$query%'";
	$result = $mysqli->query($sql);
	$data = array();
	while($row = $result->fetch_assoc()) {
		$data[] = array(
			'value' => $row['bengkel'] . ' - ' . $row['kota'],
			'data' => $row['id']
		);
	}
	$json = json_encode(array('suggestions' => $data));
	echo $json;
}


if ($action == 'get_car') {
	$query = fiestolaundry($_GET['query'], 50);
	$sql = "SELECT c.id, b.nama, c.type, c.year
			FROM car c 
			INNER JOIN car_brand b ON b.id=c.brand_id 
			WHERE b.nama LIKE '%$query%' OR c.type LIKE '%$query%'";
	$result = $mysqli->query($sql);
	$data = array();
	while($row = $result->fetch_assoc()) {
		$data[] = array(
			'value' => $row['nama'] . ' ' . $row['type'] . ' ' . $row['year'],
			'data' => $row['id']
		);
	}
	$json = json_encode(array('suggestions' => $data));
	echo $json;
}

/* if ($action == 'get_product') {
	$query = fiestolaundry($_GET['query'], 50);
	$sql = "SELECT id, title
			FROM catalogdata
			WHERE title LIKE '%$query%'";
	$result = $mysqli->query($sql);
	$data = array();
	while($row = $result->fetch_assoc()) {
		$data[] = array(
			'value' => $row['title'],
			'data' => $row['id']
		);
	}
	$json = json_encode(array('suggestions' => $data));
	echo $json;
} */

if ($action == 'get_product') {
	$q = fiestolaundry($_GET['q'], 11);
	$sql = "SELECT id, title FROM catalogdata WHERE title LIKE '%$q%' AND publish=1";
	$result = $mysqli->query($sql);
	$data = array();

	while($row = $result->fetch_assoc()) {
		$temp = array();
		$temp['id'] = $row['id'];
		$temp['name'] = $row['title'];
		$data[] = $temp;
	}

	$json_response = json_encode($data);

	echo $json_response;
}

if ($action == 'hapusthumb') {
	$berhasil=1;
	$img=$_GET['img'];
	$pid=$_GET['pid'];
	if($img!='') {
		if(file_exists("$cfg_fullsizepics_path/$img")) unlink("$cfg_fullsizepics_path/$img");
		if(file_exists("$cfg_thumb_path/$img")) unlink("$cfg_thumb_path/$img");
	}
	
	if($berhasil) {
		//ambil data thumb awal
		$thumb_awalr=array();
		$s = $mysqli->query("SELECT filename from tuning_guide WHERE id='$pid' AND filename<>''");
		if($s and $s->num_rows>0) {
			list($thumb_awal)=$s->fetch_row();
			$thumb_awalr=explode(":",$thumb_awal);
		}
		
		$filter=array();
		foreach($thumb_awalr as $i => $v) {
			if($v!=$img) {
				$filter[]=$v;
			}
		}
		$join_filter=join(":",$filter);
		$sql = "UPDATE tuning_guide SET filename='$join_filter' WHERE id='$pid'";
		if ($mysqli->query($sql)) {
			echo "berhasil";
		}
	
		//end ambil data thumb awal
	}
}