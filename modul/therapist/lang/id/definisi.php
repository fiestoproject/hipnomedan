<?php
define(_NOBRAND,"Tidak ada data literatur");
define(_BRAND,"Mobil");
define(_EDITBRAND,"Edit literatur");
define(_DELBRAND,"Hapus literatur");
define(_ADDBRAND,"Tambah literatur");
define(_ACTION,"Aksi");
define('_CHANGE', 'Change');
define('_REMOVE', 'Remove');
define('_NOLITERATUR', 'Tidak ada literatur');
define('_ADDLITERATUR', 'Tambah');
define('_ADDRESS', 'Alamat');
define('_PHONE', 'Telepon');
define('_EMAIL', 'Email');
define('_IZINPRAKTEK', 'Izin Praktek');
define('_SERTIFICATE', 'Sertifikat');
define('_DESCRIPTION', 'Uraian');
define('_PUBLISH', 'Tampilkan');
define('_URLIG', 'Url IG');
define('_NAMAIG', 'Nama IG');
define('_URLFB', 'Url FB');
define('_NAMAFB', 'Nama FB');
define('_NOTVALIDEMAIL', ' tidak valid');
define('_CONTACTUS', 'Hubungi');
?>