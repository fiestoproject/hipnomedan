<?php 

function get_product_title($product_id) {
	global $mysqli, $urlfunc;
	
	$sql = "SELECT id, cat_id, title FROM catalogdata WHERE id='$product_id'";
	$result = $mysqli->query($sql);
	list($id, $cat_id, $title) = $result->fetch_row();
	
	$titleurl = array();
	$titleurl['pid'] = $title;
	$titleurl['cat_id'] = get_cat_name($cat_id);
	
	$url = $urlfunc->makePretty("?p=catalog&action=detail&pid=$id&cat_id=$cat_id", $titleurl);
	$html = "<a href=\"$url\">$title</a>";
	return $html;
}

function get_article_by_id($therapist_id) {
	global $mysql, $cfg_img_url, $cfg_img_path, $urlfunc;
	
	$sql = "SELECT t.nama, n.* 
			FROM newsdata n
			INNER JOIN therapistdata t ON t.id=n.user_id
			WHERE user_id='$therapist_id' AND n.publish=1";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) > 0) {
		
		$content = '<div class="article-terapis">';
		$content .= '<h2 class="title-detail-terapist">Artikel</h2>';
		$content .= '<div class="row">';
		while($row = $mysql->fetch_assoc($result)) {
			extract($row);
			
			$thumbnail = explode(':', $thumb);
			$cfg_thumb_path = "$cfg_img_path/news/small/$thumbnail[0]";
			$cfg_thumb_url = "$cfg_img_url/news/small/$thumbnail[0]";
			
			$titleurl= array();
			$titleurl['pid'] = $judulberita;
			
			$url = $urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl);
			$tglberita = tglformat($tglberita);
			
			$filename = (file_exists($cfg_thumb_path)) ? $cfg_thumb_url : '';
			$content .= '<div class="col-md-6 dept news-content">';
			$content .= '<div class="dept_image"><img src="'.$cfg_thumb_url.'" /></div>';
			$content .= '<div class="newstitle" itemprop="name"><a itemprop="url" href="'.$url.'">'.$judulberita.'</a></div>';
			$content .= '<span class="newsdate"><meta itemprop="datePublished" content="2020-07-06" />'.$tglberita.'</span>';
			$content .= '<div class="newsshortdesc" temprop="description">';
			$content .= '<p style="text-align: justify;"><span style="font-size: 12pt;">Oleh: '.$nama.'</span></p>';
			$content .= $summary;
			$content .= '<a itemprop="url" href="'.$url.'" class="btn btn-default more">'._READMORE.'</a>';
			
			$content .= '</div>';
			$content .= '</div>';
				
		}
		
		$content .= '</div>';
		$content .= '</div>';
	} else {
		
		$content .= _NOARTICLEFOUND;
	}
	
	return $content;
} 

function check_before_contact($email, /* $therapist_id,  */$user_agent = NULL) {
	global $mysql, $now;
	
	// DATE_ADD(tgl, INTERVAL 1 MONTH) < '$now' AND 
	// AND therapist_id='$therapist_id' 
	if ($user_agent != '') $kondisi_ua = " AND user_agent='$user_agent'";
	$sql = "SELECT id, therapist_id, email, tgl
			FROM therapistemail 
			WHERE email='$email' $kondisi_ua
			ORDER BY id DESC 
			LIMIT 1";
	$valid = false;
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) > 0) {
		$row = $mysql->fetch_assoc($result);
		extract($row);
		
		$time = strtotime($tgl);
		$final = date("Y-m-d", strtotime("+1 month", $time));
		
		if (strtotime($final) <= strtotime($now)) {
			$valid = true;
		}
	} else {
		$valid = true;
	}
	
	return $valid;
}

function get_therapist_name($email) {
	global $mysql;
	
	$sql = "SELECT td.nama
			FROM therapistemail te
			INNER JOIN therapistdata td ON td.id=te.therapist_id
			WHERE te.email='$email'
			ORDER BY te.id DESC 
			LIMIT 1";
	$valid = false;
	$result = $mysql->query($sql);
	list($nama) = $mysql->fetch_row($result);
	return $nama;
}