<?php

$cfg_po_tax = 1.07;
$cfg_insurance = 0.2;
$cfg_insurance_plus = 5000;

/* --- NOTIFICATIONS --- */
$minimum_city_length_notification = "Pencarian kota paling sedikit harus memuat";
/* --- NOTIFICATIONS --- */

$cfg_thumb_path = "$cfg_img_path/catalog/small";
$cfg_thumb_url = "$cfg_img_url/catalog/small";
$cfg_payment_path = "$cfg_img_path/sc_payment";
$cfg_payment_logo = "$cfg_img_url/sc_payment";

$sql = "SELECT name, value FROM config WHERE modul='cart' ";
$result = $mysql->query($sql);
while (list($name, $value) = $mysql->fetch_row($result)) {
	$$name=$value;
}
ob_start();

echo <<<END
<!--================================awal Style Cart=============================================-->
<link type="text/css" href="$cfg_app_url/modul/cart/css/basic.css" rel="stylesheet">
<style>
.button-link {
    text-decoration:  none;
    padding: 5px 10px 5px 10px;
    background: #4479BA;
    color: #FFF;
}

.autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-no-suggestion { padding: 2px 5px;}
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: bold; color: #000; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { font-weight: bold; font-size: 16px; color: #000; display: block; border-bottom: 1px solid #000; }
</style>
<!--================================akhir Style Cart=============================================-->
END;

$style_css['cart']= ob_get_clean();

ob_start();
// $is_insurance = $_SESSION['use_insurance'];
// $is_packing = $_SESSION['use_packing'];
echo <<<SCRIPT
<script src="$cfg_app_url/js/accounting.min.js"></script>
<script src="$cfg_app_url/js/jquery.autocomplete.min.js"></script>
<script>

	$(function() {
		
		var is_insurance = sessionStorage.getItem('use_insurance');
		var is_packing = sessionStorage.getItem('use_packing');
		var is_dropship = sessionStorage.getItem('use_dropship');
		// if ('$is_insurance' == 1) {
			// $('input[name="use_insurance"]').attr('checked', true);
			// recalculate();
		// }
		// if ('$is_packing' == 1) {
			// $('input[name="use_packing"]').attr('checked', true);
			// recalculate();
		// }
		
		use_checked_elem("use_insurance");
		use_checked_elem("use_packing");
		
		$('input[name="use_packing"]').attr('checked', false);
		// $('#packing_price').val('');
		
		// Dropship
		var elem_attr_req 		= $('.elem_attr_req');
		remove_attr_req(elem_attr_req);
		
		$('#receivercheck').click(function() {
			var checked = $(this);
			if (!checked.is(':checked')) {
				remove_attr_req(elem_attr_req);
				$('#data-pemesan').show(200);
				$('#data-penerima').hide(200);
			} else {
				add_attr_req(elem_attr_req);
				$('#data-pemesan').hide(200);
				$('#data-penerima').show(200);
				elem_autocomplete($('input[name="kecamatan"]'));
				
			}
		})
		
		
		if ($('#result_choosecity').val() == 1) {
			var postagecity = $('input[name="postagecity"]').val();
			var postageprice = $('input[name="postageprice"]').val();
			var postageday = $('input[name="postageday"]').val();
			
			$('#postagecity').text(postagecity);
			$('#postageprice').text(postageprice);
			$('#postageday').text(postageday);
			
			// var is_insurance = sessionStorage.getItem('use_insurance');
			var insurance_value = $('input[name="insurance_value"]').val();
			var packing_value = $('input[name="packing_value"]').val();
			if (is_insurance == 1) {
				$('#insurance_price').html(format_rupiah(insurance_value));
			}
			if (is_packing == 1) {
				$('input[name="use_packing"]').attr('checked', true);
				$('input[name="use_packing"]').removeAttr('disabled');
				$('#packing_price').text(format_rupiah(packing_value));
				
				// // $('input[name="use_packing"]').attr({'checked': true, 'disabled': false});
				// if ($('#tempCity').val() != '') {
					
					// // $('input[name="use_packing"]').attr({'checked': true, 'disabled': false});
					// // $('#packing_price').text(format_rupiah(packing_value));
					// // alert($('#tempCity').val());
					// $('input[name="use_packing"]').removeAttr('disabled');
				// } 
				
			} else {
				$('input[name="use_packing"]').removeAttr('disabled');
			}
		} else {
			// $('input[name="use_packing"]').attr('checked', false);
		}
		
		
		// //Autocomplate
		// $('#kecamatan').autocomplete({
			// serviceUrl: '$cfg_app_url/modul/cart/ajax.php?action=cari_kecamatan',
			// dataType: 'json',
			// onSelect: function (suggestion) {
				// // console.log('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ' + suggestion.kecamatan);
				// var loader 			= $('#loader');
				// var form_ongkir 	= $('#form-ongkir');
				// var postagehidden 	= $('#postagehidden');
				// var cityhidden 		= $('#cityhidden');
				// var postagecity 	= $('#postagecity');
				// var postageday 		= $('#postageday');
				// var json_courier 	= $('#json_courier');
				
				// loader.show();
				// // form_ongkir.hide();
				// $.ajax({
					// data: {action: 'list', query : suggestion.kode},
					// url: '$cfg_app_url/modul/cart/ajax.php',
					// success: function(msg) {
						// form_ongkir.html(msg).show();
						// loader.hide();
						
						// var couriers = new Object();
						
						// $('input:radio[name="postageprice"]').click(function() {
							// var biaya = $(this).val();
							// var courier_id = $(this).next().val();
							// var courier_tipe = $(this).next().next().val();
							// var estimasi = $(this).next().next().next().val();
							// couriers.id = courier_id;
							// couriers.tipe = courier_tipe;
							// couriers.kota = suggestion.value;
							// var json = JSON.stringify(couriers);
							// json_courier.val(json);
							// cityhidden.val(courier_tipe + ' ' + suggestion.value);
							// postagehidden.val(biaya);
							// postagecity.val(cityhidden.val());
							// postageday.val(estimasi);
						// })
					// }
				// })
			// }
		// });
		
		elem_autocomplete($('#kecamatan'));
		
		// $('input:radio[name="postageprice"]').each(function(i, val) {
			// var id = $(this);
			// if (id.is(':checked')) {
				
				// console.log(i + ' ' + val);
			// } else {
				// console.log(i + ' ' + val + ':::');
			// }
		// }) 
		
		
		
		// $('input[name=\"kecamatan\"]').keypress(function(e) {
			// if (e.which == 13) {
				// $('#form-ongkir').show();
				// // $('#form-ongkir').removeClass('hide_div');
				// var query = $(this).val();
				
				// $.ajax({
					// data: {action: 'list', query : query},
					// url: '$cfg_app_url/modul/cart/ajax.php',
					// success: function(msg) {
						// alert(msg);
					// }
				// })
				// return false;
			// }
		// })
	})
	
	function elem_autocomplete(elem) {
		//Autocomplate
		elem.autocomplete({
			serviceUrl: '$cfg_app_url/modul/cart/ajax.php?action=cari_kecamatan',
			dataType: 'json',
			onSelect: function (suggestion) {
				// console.log('You selected: ' + suggestion.value + ', ' + suggestion.data + ', ' + suggestion.kecamatan);
				var loader 			= $('#loader');
				var form_ongkir 	= $('#form-ongkir');
				var postagehidden 	= $('#postagehidden');
				var cityhidden 		= $('#cityhidden');
				var postagecity 	= $('#postagecity');
				var postageday 		= $('#postageday');
				var json_courier 	= $('#json_courier');
				
				loader.show();
				form_ongkir.hide();
				$.ajax({
					data: {action: 'list', query : suggestion.kode},
					url: '$cfg_app_url/modul/cart/ajax.php',
					success: function(msg) {
						form_ongkir.html(msg).show();
						loader.hide();
						
						var couriers = new Object();
						
						$('input:radio[name="postageprice"]').click(function() {
							var biaya = $(this).val();
							var courier_id = $(this).next().val();
							var courier_tipe = $(this).next().next().val();
							var estimasi = $(this).next().next().next().val();
							couriers.id = courier_id;
							couriers.tipe = courier_tipe;
							couriers.kota = suggestion.value;
							var json = JSON.stringify(couriers);
							json_courier.val(json);
							cityhidden.val(courier_tipe + ' ' + suggestion.value);
							postagehidden.val(biaya);
							postagecity.val(cityhidden.val());
							postageday.val(estimasi);
						})
					}
				})
			}
		});
	}
	
	function use_checked_elem(elem) {

		$('input[name="'+elem+'"]').bind('click', function() {
			
			var insurance_value = $('input[name="insurance_value"]').val();
			var packing_value = $('input[name="packing_value"]').val();
			var insurance_price = $('input[name="insurance_price"]');
			var checked = $(this);
			
			var use_elem = elem == 'use_insurance' ? 'use_insurance' : 'use_packing';
			/* 
			
			if (elem == 'use_insurance') {
				if (checked.is(':checked')) {
					$('#insurance_price').html(format_rupiah(insurance_value));
					sessionStorage.setItem(use_elem, 1);
				} else {
					$('#insurance_price').html(0);
					sessionStorage.removeItem(use_elem);
				}
				
			} else {
				if (checked.is(':checked')) {
					$('#packing_price').html(format_rupiah(packing_value));
					sessionStorage.setItem(use_elem, 1);
				} else {
					$('#packing_price').html(0);
					sessionStorage.removeItem(use_elem);
				}
				
			} */
			
			
			var urldata = cfg_app_url + '/modul/cart/ajax.php';
			
			var temp_elem = $(this);
			if (elem == 'use_insurance') {
				var insurance_value = $('#insurance_value').val();
				if (temp_elem.is(':checked')) {
					$.get(urldata, {action : 'get_insurance', status : 1, price : insurance_value}, function(msg) {});
					$('#insurance_price').html(format_rupiah(insurance_value));
					sessionStorage.setItem(use_elem, 1);
					// recalculate();
				} else {
					$.get(urldata, {action : 'get_insurance', status : 0}, function(msg) {});
					$('#insurance_price').html(0);
					sessionStorage.removeItem(use_elem);
					// recalculate();
				}
			}
			
			// special packing kayu
			if (elem == 'use_packing') {
				var packing_value = $('#packing_value').val();
				if (temp_elem.is(':checked')) {
					$.get(urldata, {action : 'get_packing', status : 1, price : packing_value}, function(msg) {});
					$('#packing_price').html(format_rupiah(packing_value));
					sessionStorage.setItem(use_elem, 1);
					// recalculate();
					
					/* $('#total_price').text('Loading...');
					$.get(urldata, {action : 'get_packing', status : 1, price : packing_value}, function(msg) {
						if (msg != '') {
							response = JSON.parse(msg);
							if (response.results.error == 1) {
								temp_elem.attr('checked', false);
								// alert(response.results.status);
								result = confirm(response.results.status);
								if (result == true) {
									window.location = response.results.link;
								} else {
									recalculate();
								}
							}
						}
						recalculate();
					});
					if ($('#total_price').text() != 'Loading...') {
						
						recalculate();
					} */
				} else {
					$.get(urldata, {action : 'get_packing', status : 0}, function(msg) {});
					$('#packing_price').html(0);
					sessionStorage.removeItem(use_elem);
					// recalculate();
				}
			}
			
		});
	}

	// function recalculate() {
		// var use_insurance 	= $('input[name="use_insurance"]');
		// var use_packing 	= $('input[name="use_packing"]');
		// var insurance_value = $('input[name="insurance_value"]');
		// var packing_value 	= $('input[name="packing_value"]');
		// var insurance_price = $('#insurance_price');
		// var packing_price 	= $('#packing_price');
		// var total = 0;
		
		// var temp_total = parseInt($('#amount_price').val());
		
		// total += temp_total;
		// if (use_insurance.is(':checked')) {
			// insurance_price.text(format_rupiah(insurance_value.val()));
			// total += parseInt(insurance_value.val());
			// localStorage.setItem('use_insurance', 1);
		// } else {
			// insurance_price.text(0);
			// localStorage.removeItem('use_insurance');
		// }
		
		// if (use_packing.is(':checked')) {
			// packing_price.text(format_rupiah(packing_value.val()));
			// total += parseInt(packing_value.val());
			// // localStorage.setItem('use_packing', 1);
		// } else {
			// packing_price.text(0);
			// // localStorage.removeItem('use_packing');
		// }
		
		// $('#total_price').text(format_rupiah(total));
	// }
	
	function format_rupiah(str, symbol = '') {
		return accounting.formatMoney(str, symbol, 0, ".", ",");
	}	

	function add_attr_req(elem) {
		elem.attr('required', true);
	}
	
	function remove_attr_req(elem) {
		elem.removeAttr('required');
	}
	
</script>
SCRIPT;
$script_js['cart'] = ob_get_clean();
?>
