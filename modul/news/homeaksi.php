<?php
if ($jenis=='category') {
	list($homecatid,$homemaxnews)=explode(';',$isi);
	if ($homemaxnews=='') $homemaxnews=5;
	$sql = "SELECT id, cat_id, tglmuat, thumb, judulberita, summary, isiberita, tglberita, ishot FROM newsdata WHERE publish='1'";
	if ($homecatid>0) $sql .= " AND cat_id='$homecatid'";
	$sql .= " ORDER BY tglberita DESC LIMIT $homemaxnews";
	$result = $mysql->query($sql);
	if ($result and $mysql->num_rows($result)>0) {
		$x = 1;
		// $home .= "<div class=\"row\">\r\n";
		while (list($id, $cat_id, $tglmuat, $thumb, $judulberita, $summary, $isiberita, $tglberita, $ishot) = $mysql->fetch_row($result)) {
			$titleurl = array();
			$titleurl["pid"] = $judulberita;
			$home .= "<div class='col-sm-4 dept news-content'>";
			// list($thumb)=explode(":",$v['thumb']);
			if ($thumb!=""){
				$home .= "<div class=\"dept_image\"><img src=\"".$cfg_app_url."/file/news/large/".$thumb."\"></div>";
			}
			$home .= "<div class=\"newstitle\" itemprop=\"name\"><a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\">$judulberita</a></div>\r\n";
			if ($isdateshown) $home .= "<span class=\"newsdate\"><meta itemprop=\"datePublished\" content=\"$tglberita\">".tglformat($tglberita)."</span>\r\n";
			if ($issummary && $summary!='') $home .= "<div class=\"newsshortdesc\" temprop=\"description\">$isiberita</div>\r\n";
			$home .= "<a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\" class=\"btn btn-default more\">"._LEARNMORE."</a></div>\r\n";
			
		}
		$titleurl = array();
		$titleurl["pid"] = get_news_catname($homecatid);
		$home .= "<div class='col-sm-12'><a class=\"btn btn-default more\" href=\"".$urlfunc->makePretty("?p=news&action=latest&pid=$homecatid", $titleurl)."\">".get_news_catname($homecatid)." Lain</a></div>";
		// $home .= "</div>\r\n";
	} else {
		$home .= "<p>"._NONEWS."</p>";
	}
}
?>
