<?php
define(_NONEWS,"Tidak ada artikel");
define(_PUBLISHEDDATE,"Tanggal muat");
define(_NEWSSOURCE,"Sumber artikel");
define(_NEWSDATE,"Tanggal artikel");
define(_NEWSCONTENT,"Berita");
define(_EDITNEWS,"Edit artikel");
define(_DELNEWS,"Hapus artikel");
define(_YEAR,"Tahun");
define(_ADDNEWS,"Tambah artikel");
define(_CLEANBELOWMAX,"Jumlah artikel masih belum mencapai batas maksimum.");
define(_CLEANNEWS,"Hapus artikel lama");
define(_NUMEXISTINGNEWS,"Tahun #tahun : #jumlah artikel ");
define(_NUMREMAININGNEWS,"Jumlah artikel yang akan tersisa");
define(_NUMTOBEDELETEDNEWS,"Jumlah artikel yang akan dihapus");
define(_PUBLISHEDAT,"Dipublikasikan di");
define(_ONDATE,"pada tanggal");
define(_MAXDISPLAYED,"Jumlah artikel");
define(_NEWSSUMMARY,"Ringkasan");
define(_READ,"Dibaca");
define(_KATEGORI,"Kategori");
define(_BROWSEBYCATEGORY,"Tampilkan berdasarkan kategori");
define(_ALLCATEGORY,"Semua kategori");
define(_HALUTAMA,"Halaman utama");
define(_INSTALLATION_PICTURE,"Gambar");
define(_THERAPISTNAME, 'Nama Terapis');
?>