<?php

if (!$isloadfromindex) {
    include ("../../kelola/urasi.php");
    include ("../../kelola/fungsi.php");
    include ("../../kelola/lang/$lang/definisi.php");
    pesan(_ERROR, _NORIGHT);
}

$keyword = fiestolaundry($_GET['keyword'], 100);
$screen = fiestolaundry($_GET['screen'], 11);
$pid = fiestolaundry($_REQUEST['pid'], 11);

$judul = fiestolaundry($_POST['judul'], 200);
$isi = fiestolaundry($_POST['isi'], 0, TRUE);
$action = fiestolaundry($_REQUEST['action'], 10);

$modulename = $_GET['p'];
if (isset($_POST['back'])) {
    header("Location:?p=$modulename");
}

if ($action == '') {
    $sql = "SELECT id FROM page";

    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($notification == "addsuccess") {
        $admincontent .= createnotification(_ADDSUCCESS, _SUCCESS, "success");
    } else if ($notification == "editsuccess") {
        $admincontent .= createnotification(_EDITSUCCESS, _SUCCESS, "success");
    } else if ($notification == "delsuccess") {
        $admincontent .= createnotification(_DELETESUCCESS, _SUCCESS, "success");
    } else if ($notification == "dberror") {
        $admincontent .= createnotification(_DBERROR, _ERROR, "error");
    }

    if ($mysql->num_rows($result) > 0) {
        $start = $screen * $max_page_list;
        $sql = "SELECT id,judul,isi FROM page LIMIT $start, $max_page_list";
        $result = $mysql->query($sql);

        if ($pages > 1)
            $adminpagination = pagination($namamodul, $screen);
        $admincontent .= "<table class=\"stat-table\">\n";
        $admincontent .= "<tr><th>" . _TITLE . "</th>";
        $admincontent .= "<th align=\"center\">" . _EDIT . "</th><th align=\"center\">" . _DEL . "</th></tr>\n";
        while (list($id, $judul, $isi) = $mysql->fetch_row($result)) {
            $admincontent .= "<tr>\n";
            $admincontent .= "<td>$judul</td>";
            $admincontent .= "<td align=\"center\"><a href=\"?p=page&action=modify&pid=$id\">";
            $admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
            $admincontent .= "<td align=\"center\"><a href=\"?p=page&action=remove&pid=$id\">";
            $admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            $admincontent .= "</tr>\n";
        }
        $admincontent .= "</table>";
    } else {
        $admincontent = "<p>" . _NOPAGE . "</p>";
    }
    $specialadmin .= "<a class=\"buton\" href=\"?p=page&action=add\">" . _ADDPAGE . "</a>";
}

if ($action == 'search') {
    $admintitle = _SEARCHRESULTS;
    $sql = "SELECT id FROM page WHERE judul LIKE '%$keyword%' OR isi LIKE '%$keyword%'";
    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($mysql->num_rows($result) > 0) {
        $start = $screen * $max_page_list;
        $sql = "SELECT id,judul,isi FROM page WHERE judul LIKE '%$keyword%' OR isi LIKE '%$keyword%' LIMIT $start, $max_page_list";
        $result = $mysql->query($sql);

        if ($pages > 1)
            $adminpagination = pagination($namamodul, $screen);
        $admincontent .= "<table class=\"stat-table\">\n";
        $admincontent .= "<tr><th>" . _TITLE . "</th>";
        $admincontent .= "<th align=\"center\">" . _EDIT . "</th><th align=\"center\">" . _DEL . "</th></tr>\n";
        while (list($id, $judul, $isi) = $mysql->fetch_row($result)) {
            $admincontent .= "<tr>\n";
            $admincontent .= "<td>$judul</td>";
            $admincontent .= "<td align=\"center\"><a href=\"?p=page&action=modify&pid=$id\">";
            $admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
            $admincontent .= "<td align=\"center\"><a href=\"?p=page&action=remove&pid=$id\">";
            $admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            $admincontent .= "</tr>\n";
        }
        $admincontent .= "</table>";
    } else {
        $admincontent .= _NOSEARCHRESULTS;
    }
}

if ($action == "add") {
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($judul, _TITLE, false);

        if ($notificationbuilder != "") {
            $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
        } else {
            $sql = "INSERT INTO page (judul, isi) VALUES ('$judul', '$isi')";
            $result = $mysql->query($sql);
            if ($result) {
                header("Location:?p=$modulename&r=$random&notification=addsuccess");
            } else {
                $builtnotification = createnotification(_DBERROR, _ERROR, "error");
            }
        }
    } else {
        /* default value untuk form */
        $judul = "";
        $isi = "";
    }
    $admintitle = _ADDPAGE;
    $admincontent .= '
	<form class=\"form-horizontal\" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add">
            ' . $builtnotification . '
            <div class="control-group">
                    <label class="control-label">' . _TITLE . '</label>
                    <div class="controls">
                            <input type="text" name="judul" placeholder="' . _TITLE . '" class="span12" value="' . $judul . '">
                    </div>
            </div>
            <div class="control-group">
                    <label class="control-label">' . _PAGECONTENT . '</label>
                    <div class="controls">
                            <textarea name="isi" rows="20" class="usetiny span12">' . $isi . '</textarea>
                    </div>
            </div>
            <div class="control-group">
                    <div class="controls">
                            <input type="submit" name="submit" class="buton" value="' . _ADD . '">
                            <input type="submit" name="back" class="btn" value="' . _BACK . '">
                    </div>
            </div>
	</form>
    ';
}

if ($action == "modify") {
    $sql = "SELECT id, judul, isi FROM page WHERE id='$pid'";
    $result = $mysql->query($sql);
    $result_list = $result;

    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($judul, _TITLE, false);
        
        if ($notificationbuilder != "") {
            $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
        } else {
            $sql = "UPDATE page SET judul='$judul', isi='$isi' WHERE id='$pid'";
            $result = $mysql->query($sql);
            if ($result) {
                header("Location:?p=$modulename&notification=editsuccess");
            } else {
                $builtnotification = createnotification(_DBERROR, _ERROR, "error");
            }
        }
    } else {
        /* default value untuk form */
        list($id, $judul, $isi) = $mysql->fetch_row($result);
        $publishstatus = ($publish == 1) ? 'checked' : '';
    }

    $admintitle = _EDITPAGE;
    if ($mysql->num_rows($result_list) > 0) {
        $admincontent .= '
		  <form class=\"form-horizontal\" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
                        <input type="hidden" name="action" value="modify">
                        <input type="hidden" name="pid" value="' . $pid . '">
                        ' . $builtnotification . '
                        <div class="control-group">
                                <label class="control-label">' . _TITLE . '</label>
                                <div class="controls">
                                        <input type="text" name="judul" placeholder="' . _TITLE . '" class="span12" value="' . $judul . '">
                                </div>
                        </div>
                        <div class="control-group">
                                <label class="control-label">' . _PAGECONTENT . '</label>
                                <div class="controls">
                                        <textarea name="isi" rows="20" class="usetiny span12">' . $isi . '</textarea>
                                </div>
                        </div>
                        <div class="control-group">
                                <div class="controls">
                                        <input type="submit" name="submit" class="buton" value="' . _EDIT . '">
                                        <input type="submit" name="back" class="btn" value="' . _BACK . '">
                                </div>
                        </div>
		</form>';
    } else {
        $admincontent .= createstatus(_NOPAGE, "error");
    }

}

if ($action == "ubahdata") {
    $judul = checkrequired($judul, _TITLE);
    $sql = "UPDATE page SET judul='$judul', isi='$isi' WHERE id='$pid'";
    $result = $mysql->query($sql);
    if ($result) {
        pesan(_SUCCESS, _DBSUCCESS, "?p=page&r=$random");
    } else {
        pesan(_ERROR, _DBERROR);
    }
}

// DELETE LINK
if ($action == "remove" || $action == "delete") { //errhandler utk antisipasi pengetikan URL langsung di browser
    $admintitle = _DELPAGE;
    $sql = "SELECT id FROM page WHERE id='$pid'";
    $result = $mysql->query($sql);
    if ($mysql->num_rows($result) == "0") {
        $admincontent .= createstatus(_NOPAGE, "error");
    } else {
        list($id) = $mysql->fetch_row($result);
        if ($action == "remove") {
            $admincontent .= "<h5>" . _PROMPTDEL . "</h5>";
            $admincontent .= '<div class="control-group">
                                <div class="controls">
                                        <a class="buton" href="?p=' . $modulename . '&action=delete&pid=' . $id . '">' . _YES . '</a>
                                        <a class="btn" href="javascript:history.go(-1)">' . _NO . '</a></p>
                                </div>
                        </div>';
        } else {
            $sql = "DELETE FROM page WHERE id='$pid'";
            $result = $mysql->query($sql);
            if ($result) {
                header("Location:?p=$modulename&notification=delsuccess");
            } else {
                header("Location:?p=$modulename&notification=dberror");
            }
        }
    }
}

//if ($specialadmin == '')
//    $specialadmin = "<a href=\"?p=page\">" . _BACK . "</a>";
?>
