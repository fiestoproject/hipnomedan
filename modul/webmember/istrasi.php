<?php
if (!$isloadfromindex) {
    include ("../../kelola/urasi.php");
    include ("../../kelola/fungsi.php");
    include ("../../kelola/lang/$lang/definisi.php");
    pesan(_ERROR, _NORIGHT);
}
$modulename = $_GET['p'];
if (isset($_POST['back'])) {
    header("Location:?p=$modulename");
}

$action = fiestolaundry($_REQUEST['action'], 20);
$status = fiestolaundry($_REQUEST['status'], 1);
$keyword = fiestolaundry($_GET['keyword'], 100);
$screen = fiestolaundry($_GET['screen'], 11);

if ($screen == '') {
    $screen = 0;
}
$pid = fiestolaundry($_REQUEST['pid'], 11);

$postuser = fiestolaundry($_POST['txtUser'], 12);
$postpass1 = trim($_POST['txtPass1']);
$postpass2 = trim($_POST['txtPass2']);
$postnama = fiestolaundry($_POST['txtNama'], 50);
$postperusahaan = fiestolaundry($_POST['txtPerusahaan'], 50);
$postalamat1 = fiestolaundry($_POST['txtAlamat1'], 50);
$postalamat2 = fiestolaundry($_POST['txtAlamat2'], 50);
$postkota = fiestolaundry($_POST['txtKota'], 30);
$postkodepos = fiestolaundry($_POST['txtKodepos'], 10);
$posttelepon = fiestolaundry($_POST['txtTelepon'], 20);
$postponsel1 = fiestolaundry($_POST['txtPonsel1'], 20);
$postponsel2 = fiestolaundry($_POST['txtPonsel2'], 20);
$postfax = fiestolaundry($_POST['txtFax'], 20);
$postemail = fiestolaundry($_POST['txtEmail'], 50);

$postnickname = fiestolaundry($_POST['txtNickname'], 30);
$postgender = fiestolaundry($_POST['rdGender'], 1);
$postplaceofbirth = fiestolaundry($_POST['txtPlaceofbirth'], 255);
$postdbegindate = $_POST['dbegindate'];
$postmbegindate = $_POST['mbegindate'];
$postybegindate = $_POST['ybegindate'];
$postnoktp = fiestolaundry($_POST['txtNoktp'], 16);
$postprovinsi1 = fiestolaundry($_POST['txtProvinci1'], 30);
$postprovinsi2 = fiestolaundry($_POST['txtProvinci2'], 30);
$postkota2= fiestolaundry($_POST['txtKota2'],30);
$posteducation = fiestolaundry($_POST['txtEducation'],11);
$postpendidikanlain = fiestolaundry($_POST['txtPendidikanlain'],11);
$postfacebook = fiestolaundry($_POST['txtFacebook'],255);
$postinstagram = fiestolaundry($_POST['txtInstagram'],255);
$posttwitter = fiestolaundry($_POST['txtTwitter'],255);
$postgoogle = fiestolaundry($_POST['txtGoogle'],255);
$postno_whatsapp = fiestolaundry($_POST['no_whatsapp'],15);
$postkategori_hipnoterapi = fiestolaundry($_POST['rdKategori'],1);

$notification = fiestolaundry(($_GET['notification']));
$now = date('Y-m-d');

if ($status == '') {
    $status = 2;
}
$screen = fiestolaundry($_GET['screen'], 11);

if ($action == '' or $action == 'search') {
/*
Status == 0 --> Baru
Status == 1 --> Aktif
Status == 2 --> Expired
Status == 3 --> Banned

========================
Status == 0 --> Baru
Status == 1 --> Belum konfirmasi
Status == 2 --> Aktif
Status == 3 --> Banned
Status == 4 --> Sudah konfirmasi
*/
    $jumlah_kolom = 5;
    if ($status == "0") {
        $judulstatus = _NEWMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='0' ";
        $sql .= "AND activedate='0000-00-00 00:00:00' ";
        /*
          if($expired_periode > 0)
          {	$sql .= "AND DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'), DATE_FORMAT(activedate,'%Y-%m-%d')) <= '$expired_periode' ";
          }
         */
        if ($action == 'search') {
            $sql = "AND (nickname LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        }
        $sql .= "ORDER BY user_regdate ASC ";
    } elseif ($status == "1") {
        // $judulstatus = _ACTIVEMEMBER;
        $judulstatus = _CONFIRMMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='1' ";
        if ($action == 'search') {
            $sql .= "AND (nickname LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        }
        $sql .= "ORDER BY nickname ASC ";
        $jumlah_kolom = 6;
    } elseif ($status == "2") {
        $judulstatus = _ACTIVEMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='2' ";
        // $sql .= "AND activedate='0000-00-00 00:00:00' ";

        // if ($expired_periode > 0) {
            // $sql .= "AND DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'), DATE_FORMAT(activedate,'%Y-%m-%d')) > '$expired_periode' ";
        // }
        if ($action == 'search') {
            $sql .= "AND (nickname LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        }
        $sql .= "ORDER BY user_regdate ASC ";
    } elseif ($status == "3") {
        $judulstatus = _BANNEDMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='0' ";
        $sql .= "AND bandate!='0000-00-00 00:00:00' ";
        if ($action == 'search') {
            $sql .= "AND (nickname LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        }
        $sql .= "ORDER BY bandate DESC ";
        $jumlah_kolom = 5;
    }
	
    $admintitle .= $judulstatus;

    $urlstatus = "index.php?p=$namamodul&action=$action&status=";
    $admincontent .= _STATUS . " : ";

    $admincontent .= "<select name=\"status\" id=\"status\" onchange=\"window.location='$urlstatus'+this.value;\">";
    $admincontent .= "<option value=\"0\" " . (($status == 0) ? "selected=\"selected\" " : "") . " >" . _NEWMEMBER . "</option>";
    $admincontent .= "<option value=\"1\" " . (($status == 1) ? "selected=\"selected\" " : "") . " >" . _CONFIRMMEMBER . "</option>";
    $admincontent .= "<option value=\"2\" " . (($status == 2) ? "selected=\"selected\" " : "") . " >" . _ACTIVEMEMBER . "</option>";
    // $admincontent .= "<option value=\"3\" " . (($status == 3) ? "selected=\"selected\" " : "") . " >" . _BANNEDMEMBER . "</option>";
    $admincontent .= "</select>";
    $admincontent .= "<br/><br/>";

    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($notification == "addsuccess") {
        $admincontent .= createnotification(_ADDSUCCESS, _SUCCESS, "success");
    } else if ($notification == "editsuccess") {
        $admincontent .= createnotification(_EDITSUCCESS, _SUCCESS, "success");
    } else if ($notification == "delsuccess") {
        $admincontent .= createnotification(_DELETESUCCESS, _SUCCESS, "success");
    } else if ($notification == "dberror") {
        $admincontent .= createnotification(_DBERROR, _ERROR, "error");
    } else if ($notification == "activationsuccess") {
        $admincontent .= createnotification(_ACTIVATIONSUCCESS, _SUCCESS, "success");
    } else if ($notification == "sendcodesuccess") {
        $admincontent .= createnotification(_SENDCODESUCCESS, _SUCCESS, "success");
    } else if ($notification == "bansuccess") {
        $admincontent .= createnotification(_BANSUCCESS, _SUCCESS, "success");
    }
	
    if ($mysql->num_rows($result) > 0) {
        $start = $screen * $max_page_list;
        $sql .= "LIMIT $start, $max_page_list ";
        $result = $mysql->query($sql);

        if ($pages > 1) {
            $adminpagination = pagination($namamodul, $screen, "&action=&status=$status");
        }

        $admincontent .= "<form name=\"myform\" id=\"myform\" method=\"POST\" action=\"$thisfile\" >";
		if ($status == 0) {
			$admincontent .= "<input type=\"hidden\" name=\"action\" value=\"konfirmasi\">";
		}
		if ($status == 1 || $status == 2) {
			$admincontent .= "<input type=\"hidden\" name=\"action\" value=\"aktivasi\">";
		}

        $admincontent .= "<table class=\"stat-table\">\n";
        $admincontent .= "<tr>";
        $admincontent .= "<th>&nbsp;</th>";
        $admincontent .= "<th>" . _NAME . "</th>";
        $admincontent .= "<th>" . _EMAIL . "</th>";
        // $admincontent .= "<th>" . _LEVEL . "</th>";
        $admincontent .= "<th>" . _TIMEREGISTER . "</th>";
        $admincontent .= "<th>" . _TIMEEXPIRED . "</th>";
        // if ($status == "1") {
            // $admincontent .= "<th>" . _TIMEACTIVE . "</th>";
        // }
        if ($status == "3") {
            $admincontent .= "<th>" . _TIMEBANNED . "</th>";
        }
        $admincontent .= "<th align=\"center\">" . _ACTION . "</th>";
        //$admincontent .= "<th>"._DEL."</th>";
        $admincontent .= "</tr>\n";

        while ($data_member = $mysql->fetch_assoc($result)) {

            $member_id = $data_member["user_id"];
            $admincontent .= "<tr valign=\"top\">";
            $admincontent .= "<td>";
            $admincontent .= "<input type=\"checkbox\" name='cekMember[]' id='cekMember[]'  value='" . $member_id . "'>";
            $admincontent .= "</td>";
            $admincontent .= "<td>" . $data_member["fullname"] . "&nbsp;</td>";
            $admincontent .= "<td>" . $data_member["user_email"] . "</td>";
            // $admincontent .= "<td>" . view_level_name($data_member["level"]) . "</td>";
            // $admincontent .= "<td>" . $data_member["level"] . "</td>";
            $admincontent .= "<td>" . tglformat($data_member["user_regdate"]) . "</td>";
            $admincontent .= "<td>". tglformat($data_member['expired'])."</td>";
            // if ($status == "1") {
                // $admincontent .= "<td>" . tglformat($data_member["activedate"]) . "</td>";
            // }
            if ($status == "3") {
                $admincontent .= "<td>" . tglformat($data_member["bandate"]) . "</td>";
            }
            // $admincontent .= "<td align=\"center\"><a href=\"?p=webmember&action=modify&pid=$member_id\">";
            // $admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";

			$admincontent .= "<td align=\"center\" class=\"action-ud\">";
			$admincontent .= "<a href=\"".param_url("?p=$modulename&action=modify&pid=$member_id")."\"><img alt=\"" . _EDIT . "\" border=\"0\" src=\"../images/modify.gif\"></a>\r\n";

			if ($status == 2) {
				$admincontent .= "<a href=\"".param_url("?p=$modulename&action=pdf&pid=$member_id")."\"><img alt=\"" . _PDF . "\" border=\"0\" src=\"../images/pdf-icon.svg\" width=\"20px\"></a></td>\n";
			}
			$admincontent .= "</tr>\n";

            //$admincontent .= "<td>"._EDIT."</td>";
            //$admincontent .= "<td>"._DEL."</td>";
            $admincontent .= "</tr>\n";
        }

        $admincontent .= "<tr>";
        $admincontent .= "<td>";
        $admincontent .= "&nbsp;";
        $admincontent .= "</td>";
        $admincontent .= "<td valign=\"top\" colspan=\"$jumlah_kolom\">";
        if ($status == 0) {
            /* $event = "document.myform.elements['action'].value='aktivasi'; document.getElementById('myform').submit(); ";
            $admincontent .= "<input class=\"btn btn-success\" type=\"button\" value=\"" . _ACTIVATION . "\" onclick=\"$event\" />\r\n";
            $admincontent .= "&nbsp;&nbsp;"; */

			$event = "document.myform.elements['action'].value='konfirmasi'; document.getElementById('myform').submit(); ";
            $admincontent .= "<input class=\"btn btn-success\" type=\"button\" value=\"" . _KONFIRMASI . "\" onclick=\"$event\" />\r\n";
            $admincontent .= "&nbsp;&nbsp;";

            /* if ($is_public_activation) {
                $event = "document.myform.elements['action'].value='resendcode'; document.getElementById('myform').submit(); ";
                $admincontent .= "<input class=\"btn btn-info\" type=\"button\" value=\"" . _RESENDCODE . "\" onclick=\"$event\" />\r\n";
                $admincontent .= "&nbsp;&nbsp;";
            } */

            $event = "document.myform.elements['action'].value='ban'; document.getElementById('myform').submit(); ";
            $admincontent .= "<input class=\"buton\" type=\"button\" value=\"" . _BAN . "\" onclick=\"$event\" />\r\n";
            $admincontent .= "&nbsp;&nbsp;";
        }
        if ($status == 1) {
            $event = "document.myform.elements['action'].value='aktivasi'; document.getElementById('myform').submit(); ";
            $admincontent .= "<input class=\"btn btn-success\" type=\"button\" value=\"" . _ACTIVATION . "\" onclick=\"$event\" />\r\n";
            $admincontent .= "&nbsp;&nbsp;";
        }
        if ($status == 3) {
            $event = "document.myform.elements['action'].value='aktivasi'; document.getElementById('myform').submit(); ";
            $admincontent .= "<input class=\"btn btn-success\" type=\"button\" value=\"" . _RESTORE . "\" onclick=\"$event\" />\r\n";
            $admincontent .= "&nbsp;&nbsp;";
        }
        $event = "document.myform.elements['action'].value='remove'; document.getElementById('myform').submit(); ";
        $admincontent .= "<input class=\"btn btn-warning\" type=\"button\" value=\"" . _DELETE . "\"  onclick=\"$event\" />\r\n";
        $admincontent .= "</td>";
        $admincontent .= "</tr>";
        $admincontent .= "</table>";
        $admincontent .= "</form>";
    } else {
        if ($action == 'search') {
            $admincontent .= createstatus(_NOSEARCHRESULTS, "error");
        } else {
            $admincontent .= createstatus(_NOMEMBER, "error");
        }
    }
    if ($action == '') {
        $specialadmin .= "<a class=\"buton\" href=\"?p=webmember&action=add\">" . _ADDWEBMEMBER . "</a>\n";
    }
	// if ($status == 2) {
		$specialadmin .= "<a class=\"buton\" href=\"?p=webmember&action=excel&status=$status\">" . _IMPORTEXCEL . "</a>\n";
	// }
}

if ($action == 'aktivasi') {
    $daftar_id = $_POST["cekMember"];
    $daftar_id = checkrequired($daftar_id, _NOCHECK);
    $list_id = implode(",", $daftar_id);
    $sql = "UPDATE $tabelwebmember SET activity='2',activedate=NOW(), bandate='' WHERE user_id IN ($list_id) ";

	if (!$mysql->query($sql)) {
        header("Location:?p=$modulename&notification=dberror");
    } else {
		if (count($daftar_id) > 0) {
			$http_server = $cfg_app_url;
			$title_tag = $config_site_titletag;
			// $subject = 'Email sebentar lagi aktif, silakan melakukan transfer';
			foreach($daftar_id as $user_id) {
				$sql = "SELECT user_email, fullname, kategori_hipnoterapi, gender, kode_digit, expired FROM webmember WHERE user_id=$user_id";
				$result = $mysql->query($sql);
				list($user_email, $fullname, $kategori_hipnoterapi, $gender, $kode_digit) = $mysql->fetch_row($result);

				// include("$cfg_app_path/modul/$namamodul/lang/$lang/mailactv.php");

				// $html = array(
					// '/#titletag/' => $title_tag,
					// '/#httpserver/' => '<a href="'.$http_server.'">'.$http_server.'</a>'
				// );
				// $message = preg_replace(array_keys($html), array_values($html), $email_admin_sent1);
				if ($gender != 0) {
					$gender_prefix = ($gender == 1) ? _BAPAK : _IBU;
				}

				$nominal = ($kategori_hipnoterapi == 1) ? substr($amountcats[$kategori_hipnoterapi], 0, 3) . $kode_digit : substr($amountcats[$kategori_hipnoterapi], 0, 4) . $kode_digit;
				$terbilang = bilangan($nominal);
				$kode = base64_encode($user_id);
				$html = array(
					'/#namaanggota/' => $fullname,
					'/#gender/' => $gender_prefix,
					'/#expired/' => tglformat($expired),
					'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>",
					'/#nominalkategori/' => "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>",
					'/#httpserver/' => '<a href="'.$http_server.$kode.'">'.$http_server.$kode.'</a>'
				);
				$message = preg_replace(array_keys($html), array_values($html), $email_admin_konfirmasi);
				// $message = $email_admin_konfirmasi;

				fiestophpmailer($user_email,_SUBJECTADMINKONFIRMASI,$message,$smtpuser,$sendername,$smtpuser, $message);
			}
		}

        header("Location:?p=$modulename&r=$random&notification=activationsuccess");
    }
}

if ($action == 'konfirmasi') {

	$daftar_id = $_POST["cekMember"];
    $daftar_id = checkrequired($daftar_id, _NOCHECK);
    $list_id = implode(",", $daftar_id);
    $sql = "UPDATE $tabelwebmember SET activity='1' WHERE user_id IN ($list_id) ";
	if (!$mysql->query($sql)) {
		header("Location:?p=$modulename&notification=dberror");
    } else {
		if (count($daftar_id) > 0) {
			$http_server = "$cfg_app_url/webmember/confirm_payment/";
			$title_tag = $config_site_titletag;
			// $subject = 'Email sebentar lagi aktif, silakan melakukan transfer';
			foreach($daftar_id as $user_id) {
				$sql = "SELECT user_email, fullname, kategori_hipnoterapi, gender, kode_digit, expired FROM webmember WHERE user_id=$user_id";
				$result = $mysql->query($sql);
				list($user_email, $fullname, $kategori_hipnoterapi, $gender, $kode_digit) = $mysql->fetch_row($result);

				// include("$cfg_app_path/modul/$namamodul/lang/$lang/mailactv.php");
				if ($gender != 0) {
					$gender_prefix = ($gender == 1) ? _BAPAK : _IBU;
				}

				$nominal = ($kategori_hipnoterapi == 1) ? substr($amountcats[$kategori_hipnoterapi], 0, 3) . $kode_digit : substr($amountcats[$kategori_hipnoterapi], 0, 4) . $kode_digit;
				$terbilang = bilangan($nominal);
				$kode = base64_encode($user_id);
				$html = array(
					'/#namaanggota/' => $fullname,
					'/#gender/' => $gender_prefix,
					'/#expired/' => tglformat($expired),
					'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>",
					'/#nominalkategori/' => "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>",
					'/#httpserver/' => '<a href="'.$http_server.$kode.'">'.$http_server.$kode.'</a>'
				);
				$message = preg_replace(array_keys($html), array_values($html), $email_anggota_konfirmasi);
				fiestophpmailer($user_email,_SUBJECTANGGOTAKONFIRMASI,$message,$smtpuser,$sendername,$smtpuser, $message);
			}
		}

        header("Location:?p=$modulename&r=$random&notification=activationsuccess");
    }
}

if ($action == 'resendcode') {
    $option = "From: $client_email";

    $daftar_id = $_POST["cekMember"];
    $daftar_id = checkrequired($daftar_id, _NOCHECK);
    $list_id = implode(",", $daftar_id);
    $sql = "SELECT * FROM $tabelwebmember WHERE user_id IN ($list_id) ";
    $result = $mysql->query($sql);
    if ($mysql->num_rows($result) > 0) {
        while ($data_member = $mysql->fetch_assoc($result)) {
            $postemail = $data_member["user_email"];
            $activationcode = $data_member["activationcode"];
            include("$cfg_app_path/modul/$namamodul/lang/$lang/mailactvcode.php");
            fiestomail($postemail, $subject, $message, $option, $islocal);
        }
    }
    header("Location:?p=$modulename&r=$random&notification=sendcodensuccess");
}

if ($action == 'ban') {
    $daftar_id = $_POST["cekMember"];
    $daftar_id = checkrequired($daftar_id, _NOCHECK);
    $list_id = implode(",", $daftar_id);
    $sql = "UPDATE $tabelwebmember SET activity='0',bandate=NOW() WHERE user_id IN ($list_id) ";
    if (!$mysql->query($sql)) {
        header("Location:?p=$modulename&notification=dberror");
    } else {

		if (count($daftar_id) > 0) {
			foreach($daftar_id as $user_id) {
				$sql = "SELECT user_email, fullname, kategori_hipnoterapi, gender FROM webmember WHERE user_id=$user_id";
				$result = $mysql->query($sql);
				list($user_email, $fullname, $kategori_hipnoterapi, $gender) = $mysql->fetch_row($result);

				// include("$cfg_app_path/modul/$namamodul/lang/$lang/mailactv.php");
				if ($gender != 0) {
					$gender_prefix = ($gender == 1) ? _BAPAK : _IBU;
				}

				$html = array(
					'/#namaanggota/' => $fullname,
					'/#gender/' => $gender_prefix,
					'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>"
				);
				$message = preg_replace(array_keys($html), array_values($html), $email_anggota_ditolak);

				fiestophpmailer($user_email,_SUBJECTANGGOTADITOLAK,$message,$smtpuser,$sendername,$smtpuser, $message);
			}
		}

        header("Location:?p=$modulename&r=$random&notification=bansuccess");
    }
}

if ($action == 'remove') {
    $daftar_id = $_POST["cekMember"];
    $daftar_id = checkrequired($daftar_id, _NOCHECK);
    $admintitle = _DELMEMBER;
    $admincontent .= "<h5>" . _PROMPTDEL . "</h5>";
    $admincontent .= "<form name=\"myform\" id=\"myform\" method=\"POST\" action=\"$thisfile\" >";
    $admincontent .= "<input type=\"hidden\" name=\"action\" value=\"hapus\">";
    foreach ($daftar_id as $key => $value) {
        $admincontent .= "<input type=\"hidden\" name='cekMember[]' id='cekMember[]'  value='" . $value . "'>";
    }
    $admincontent .= "<div class=\"control-group\"><div class=\"controls\">";
    $admincontent .= "<a class=\"buton\" href=\"javascript:document.myform.submit();\">" . _YES . "</a> "
            . "<a class=\"btn\" href=\"?p=$modulename\">" . _NO . "</a>";
    $admincontent .= "</div></div>";
    $admincontent .= "</form>";
}

if ($action == 'hapus') {
    $daftar_id = $_POST["cekMember"];
    $daftar_id = checkrequired($daftar_id, _NOCHECK);
    $list_id = implode(",", $daftar_id);

	$sql = "SELECT pas_foto,fotoktp,fotosertifikat,filename_lain1,filename_lain2 FROM $tabelwebmember WHERE user_id IN ($list_id) ";
	$result=$mysql->query($sql);
	list($pas_foto,$fotoktp,$fotosertifikat,$filename_lain1,$filename_lain2) = $mysql->fetch_row($result);
	if ($pas_foto!='' && file_exists("$cfg_fullsizepics_path/$pas_foto")) unlink("$cfg_fullsizepics_path/$pas_foto");
	if ($pas_foto!='' && file_exists("$cfg_thumb_path/$pas_foto")) unlink("$cfg_thumb_path/$pas_foto");
	if ($fotoktp!='' && file_exists("$cfg_fullsizepics_path/$fotoktp")) unlink("$cfg_fullsizepics_path/$fotoktp");
	if ($fotoktp!='' && file_exists("$cfg_thumb_path/$fotoktp")) unlink("$cfg_thumb_path/$fotoktp");
	if ($fotosertifikat!='' && file_exists("$cfg_fullsizepics_path/$fotosertifikat")) unlink("$cfg_fullsizepics_path/$fotosertifikat");
	if ($fotosertifikat!='' && file_exists("$cfg_thumb_path/$fotosertifikat")) unlink("$cfg_thumb_path/$fotosertifikat");
	if ($filename_lain1!='' && file_exists("$cfg_fullsizepics_path/$filename_lain1")) unlink("$cfg_fullsizepics_path/$filename_lain1");
	if ($filename_lain1!='' && file_exists("$cfg_thumb_path/$filename_lain1")) unlink("$cfg_thumb_path/$filename_lain1");
	if ($filename_lain2!='' && file_exists("$cfg_fullsizepics_path/$filename_lain2")) unlink("$cfg_fullsizepics_path/$filename_lain2");
	if ($filename_lain2!='' && file_exists("$cfg_thumb_path/$filename_lain2")) unlink("$cfg_thumb_path/$filename_lain2");

    $sql = "DELETE FROM $tabelwebmember WHERE user_id IN ($list_id) ";
    $result = $mysql->query($sql);
    if ($result) {
        header("Location:?p=$modulename&notification=delsuccess");
    } else {
        header("Location:?p=$modulename&notification=dberror");
    }
}

if ($action == 'add') {
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($postnama, _FULLNAME, false);
        $notificationbuilder .= validation($postponsel1, _MOBILE1, false);
        $notificationbuilder .= emailvalidation($postemail, true);
        //cek apakah email sudah terpakai
        $sql = "SELECT user_id FROM $tabelwebmember WHERE user_email='$postemail'";
        $result = $mysql->query($sql);
        if ($mysql->num_rows($result) > 0) {
            $notificationbuilder .= _EMAIL . " " . _ALREADYEXIST . "!<br/>";
        }

		if($_FILES['pasfoto']['name'] != '') {

			$hasilupload = fiestoupload('pasfoto', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if($hasilupload != _SUCCESS) {
				$notificationbuilder .= $hasilupload;
			}

			//ambil informasi basename dan extension
			$temp = explode(".",$_FILES['pasfoto']['name']);
			$extension_pasfoto = $temp[count($temp)-1];
			$basename_pasfoto = '';
			for ($i=0;$i<count($temp)-1;$i++) {
				$basename_pasfoto .= $temp[$i];
			}
		}

		if($_FILES['fotoktp']['name'] != '') {

			$hasilupload = fiestoupload('fotoktp', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if($hasilupload != _SUCCESS) {
				$notificationbuilder .= $hasilupload;
			}

			//ambil informasi basename dan extension
			$temp = explode(".",$_FILES['fotoktp']['name']);
			$extension_fotoktp = $temp[count($temp)-1];
			$basename_fotoktp = '';
			for ($i=0;$i<count($temp)-1;$i++) {
				$basename_fotoktp .= $temp[$i];
			}
		}

		if($_FILES['fotosertifikat']['name'] != '') {

			$hasilupload = fiestoupload('fotosertifikat', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if($hasilupload != _SUCCESS) {
				$notificationbuilder .= $hasilupload;
			}

			//ambil informasi basename dan extension
			$temp = explode(".",$_FILES['fotosertifikat']['name']);
			$extension_fotosertifikat = $temp[count($temp)-1];
			$basename_fotosertifikat = '';
			for ($i=0;$i<count($temp)-1;$i++) {
				$basename_fotosertifikat .= $temp[$i];
			}
		}

		if($_FILES['fotolain1']['name'] != '') {

			$hasilupload = fiestoupload('fotolain1', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if($hasilupload != _SUCCESS) {
				$notificationbuilder .= $hasilupload;
			}

			//ambil informasi basename dan extension
			$temp = explode(".",$_FILES['fotolain1']['name']);
			$extension_fotolain1 = $temp[count($temp)-1];
			$basename_fotolain1 = '';
			for ($i=0;$i<count($temp)-1;$i++) {
				$basename_fotolain1 .= $temp[$i];
			}
		}

		if($_FILES['fotolain2']['name'] != '') {

			$hasilupload = fiestoupload('fotolain2', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if($hasilupload != _SUCCESS) {
				$notificationbuilder .= $hasilupload;
			}

			//ambil informasi basename dan extension
			$temp = explode(".",$_FILES['fotolain2']['name']);
			$extension_fotolain2 = $temp[count($temp)-1];
			$basename_fotolain2 = '';
			for ($i=0;$i<count($temp)-1;$i++) {
				$basename_fotolain2 .= $temp[$i];
			}
		}

        // $cookedpass = fiestopass($postpass1);
        if ($notificationbuilder != "") {
            $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
        } else {

			$sql="INSERT INTO $tabelwebmember SET username='$postuser',
				fullname='$postnama', level='1',
				address1='$postalamat1',
				address2='$postalamat2', city='$postkota', zip='$postkodepos',
				telephone='$posttelepon', cellphone1='$postponsel1',
				cellphone2='$postponsel2', user_email='$postemail',
				user_regdate=NOW(), state='$postprovinsi1', nickname='$postnickname', gender='$postgender', placeofbirth='$postplaceofbirth', dateofbirth='$dateofbirth', noktp='$postnoktp', city2='$postkota2', state2='$postprovinsi2', pendidikan='$posteducation', facebook='$postfacebook', instagram='$postinstagram', twitter='$posttwitter', google='$postgoogle', kategori_hipnoterapi='$postkategori_hipnoterapi', no_whatsapp='$postno_whatsapp'";
            $result = $mysql->query($sql);

            if ($result) {
                $uid = $mysql->insert_id();

				if ($basename_pasfoto != '') {
					$pasfotofilename = "$basename_pasfoto-$uid.$extension_pasfoto";
					rename("$cfg_fullsizepics_path/".$_FILES['pasfoto']['name'],"$cfg_fullsizepics_path/$pasfotofilename");

					$sql = "UPDATE $tabelwebmember SET pas_foto='$pasfotofilename' WHERE user_id='$uid'";
					$result = $mysql->query($sql);
				}
				if ($basename_fotoktp != '') {
					$fotoktpfilename = "$basename_fotoktp-$uid.$extension_pasfoto";
					rename("$cfg_fullsizepics_path/".$_FILES['fotoktp']['name'],"$cfg_fullsizepics_path/$fotoktpfilename");

					$sql = "UPDATE $tabelwebmember SET fotoktp='$fotoktpfilename' WHERE user_id='$uid'";
					$result = $mysql->query($sql);
				}
				if ($basename_fotosertifikat != '') {
					$fotosertifikatfilename = "$basename_fotosertifikat-$uid.$extension_pasfoto";
					rename("$cfg_fullsizepics_path/".$_FILES['fotosertifikat']['name'],"$cfg_fullsizepics_path/$fotosertifikatfilename");

					$sql = "UPDATE $tabelwebmember SET fotosertifikat='$fotosertifikatfilename' WHERE user_id='$uid'";
					$result = $mysql->query($sql);
				}
				if ($basename_fotolain1 != '') {
					$fotolain1filename = "$basename_fotolain1-$uid.$extension_pasfoto";
					rename("$cfg_fullsizepics_path/".$_FILES['fotolain1']['name'],"$cfg_fullsizepics_path/$fotolain1filename");

					$sql = "UPDATE $tabelwebmember SET filename_lain1='$fotolain1filename' WHERE user_id='$uid'";
					$result = $mysql->query($sql);
				}
				if ($basename_fotolain2 != '') {
					$fotolain2filename = "$basename_fotolain2-$uid.$extension_pasfoto";
					rename("$cfg_fullsizepics_path/".$_FILES['fotolain2']['name'],"$cfg_fullsizepics_path/$fotolain2filename");

					$sql = "UPDATE $tabelwebmember SET filename_lain2='$fotolain2filename' WHERE user_id='$uid'";
					$result = $mysql->query($sql);
				}
                header("Location:?p=$modulename&r=$random&notification=addsuccess");
            } else {
                $builtnotification = createnotification(_DBERROR, _ERROR, "error");
            }
        }
    } else {
        /* default value untuk form */
        $postuser = "";
        $postpass1 = "";
        $postpass2 = "";
        $postnama = "";
        $postperusahaan = "";
        $postalamat1 = "";
        $postalamat2 = "";
        $postkota = "";
        $postkodepos = "";
        $posttelepon = "";
        $postponsel1 = "";
        $postponsel2 = "";
        $postfax = "";
        $postemail = "";

    }

	$select_provinci1 = '<select name="txtProvinci1">'.get_provinci().'</select>';
	$select_provinci2 = '<select name="txtProvinci2">'.get_provinci().'</select>';
	$select_pendidikan_formal = '<select name="txtEducation">'.get_educations().'</select>';

    $admintitle = _ADDMEMBER;
    $admincontent .= '
	<form class=\"form-horizontal\" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add">
            ' . $builtnotification . '
            <span class="label label-warning">' . _REQNOTE . '</span>
            <div class="control-group">
				<label class="control-label">' . _FULLNAME . '*</label>
				<div class="controls">
					<input type="text" name="txtNama" placeholder="' . _FULLNAME . '" class="span12" value="' . $row['fullname'] . '" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _NICKNAME . '</label>
				<div class="controls">
					<input type="text" name="txtNickname" placeholder="' . _NICKNAME . '" class="span12" value="' . $row['nickname'] . '">
				</div>
			</div>
			<div class="control-group">
				<span class="control-label">'._GENDER.' <span class="reqsign"></span></span>
				<label><input class="form-controlx" name="rdGender" type="radio" value="1" '.(($row['gender'] == 1) ? "checked" : "").'><span>'._LAKI.'</span></label><label><input class="form-controlx" name="rdGender" type="radio" value="2" '.(($row['gender'] == 2) ? "checked" : "").'><span>'._PEREMPUAN.'</span></label>
			</div>
			<div class="control-group">
				<label class="control-label">' . _PLACEOFBIRTH . '</label>
				<div class="controls">
					<input type="text" name="txtPlaceofbirth" placeholder="' . _PLACEOFBIRTH . '" class="span12" value="' . $row['placeofbirth'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _DATEEOFBIRTH . '</label>
				<div class="controls">';

				$dob = explode('-', $row['dateofbirth']);

				$admincontent .= "<select class=\"tanggal\" name=\"dbegindate\">";
				for ($i = 1; $i <= 31; $i++) {
					if ($dob[2] == $i) {
						$admincontent .= "<option value=\"$i\" selected>$i</option>\n";
					} else {
						$admincontent .= "<option value=\"$i\">$i</option>\n";
					}
				}
				$admincontent .= "</select>";

				$admincontent .= "<select class=\"bulan\" name=\"mbegindate\">";
				for ($i = 1; $i <= 12; $i++) {
					$j = $i - 1;
					if ($dob[1] == $i) {
						$admincontent .= "<option value=\"$i\" selected>$namabulan[$j]</option>\n";
					} else {
						$admincontent .= "<option value=\"$i\">$namabulan[$j]</option>\n";
					}
				}
				$admincontent .= "</select>";

				$admincontent .= "<select class=\"tahun\" name=\"ybegindate\">";
				for ($i = $prefix_tahun; $i <= date('Y'); $i++) {
					if ($dob[0] == $i) {
						$admincontent .= "<option value=\"$i\" selected>$i</option>\n";
					} else {
						$admincontent .= "<option value=\"$i\">$i</option>\n";
					}
				}
				$admincontent .= "</select>";
			$admincontent .= '
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _NOKTP . '</label>
				<div class="controls">
					<input type="text" name="txtNoktp" placeholder="' . _NOKTP . '" class="span12" value="' . $row['noktp'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _ADDRESS . '</label>
				<div class="controls">
					<input type="text" name="txtAlamat1" placeholder="' . _ADDRESS . '" class="span12" value="' . $row['address1'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _CITY . '</label>
				<div class="controls">
					<input type="text" name="txtKota" placeholder="' . _CITY . '" class="span12" value="' . $row['city'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _PROVINCI . '</label>
				<div class="controls">
					'.$select_provinci1.'
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _ADDRESSTHISTIME . '</label>
				<div class="controls">
					<input type="text" name="txtAlamat2" placeholder="' . _ADDRESSTHISTIME . '" class="span12" value="' . $row['address2'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _CITY . '</label>
				<div class="controls">
					<input type="text" name="txtKota2" placeholder="' . _CITY . '" class="span12" value="' . $row['city2'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _PROVINCI . '</label>
				<div class="controls">
					'.$select_provinci2.'
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _PENDIDIKANFORMAL . '</label>
				<div class="controls">
					'.$select_pendidikan_formal.'
				</div>
			</div>
			 <div class="control-group">
				<label class="control-label">' . _TELP1 . '</label>
				<div class="controls">
					<input type="text" name="txtTelepon" placeholder="' . _TELP1 . '" class="span12" value="' . $row['telephone'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _MOBILE1 . '*</label>
				<div class="controls">
					<input type="text" name="txtPonsel1" placeholder="' . _MOBILE1 . '" class="span12" value="' . $row['cellphone1'] . '" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _NOWHATSAPP . '</label>
				<div class="controls">
					<input type="text" name="no_whatsapp" placeholder="' . _NOWHATSAPP . '" class="span12" value="' . $row['no_whatsapp'] . '" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _EMAIL . '*</label>
				<div class="controls">
					<input type="text" name="txtEmail" placeholder="' . _EMAIL . '" class="span12" value="' . $row['user_email'] . '" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _FACEBOOK . '</label>
				<div class="controls">
					<input type="text" name="txtFacebook" placeholder="' . _FACEBOOK . '" class="span12" value="' . $row['facebook'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _INSTAGRAM . '</label>
				<div class="controls">
					<input type="text" name="txtInstagram" placeholder="' . _INSTAGRAM . '" class="span12" value="' . $row['instagram'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _TWITTER . '</label>
				<div class="controls">
					<input type="text" name="txtTwitter" placeholder="' . _TWITTER . '" class="span12" value="' . $row['twitter'] . '">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _GOOGLE . '</label>
				<div class="controls">
					<input type="text" name="txtGoogle" placeholder="' . _GOOGLE . '" class="span12" value="' . $row['google'] . '">
				</div>
			</div>
			<div class="control-group">
				<span class="control-label">'._KATEGORI.' <span class="reqsign">*</span></span>
				<label><input class="form-controlx" name="rdKategori" type="radio" value="1" required '.(($row['kategori_hipnoterapi'] == 1) ? "checked" : "").'><span>'._HIPNOTERAPIS.'</span></label><label><input class="form-controlx" name="rdKategori" type="radio" value="2" required '.(($row['kategori_hipnoterapi'] == 2) ? "checked" : "").'><span>'._HIPNOTERAPISKLINIS.'</span></label>
			</div>
			<br>
			<div class="control-group">
				<label class="control-label">' . _PASFOTO . '</label>
				<div class="controls">';
				if ($row['pas_foto'] != '' && file_exists("$cfg_fullsizepics_path/".$row['pas_foto'])) {
					$pas_foto = $cfg_fullsizepics_url . '/' . $row['pas_foto'];
					$admincontent .= '<div><img src="'.$pas_foto.'" class="img-responsive" width="100px"></div>';
				}
				$admincontent .= '
					<input class="form-control" name="pasfoto" type="file" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _FOTOKTP . '</label>
				<div class="controls">';
				if ($row['fotoktp'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotoktp'])) {
					$fotoktp = $cfg_fullsizepics_url . '/' . $row['fotoktp'];
					$admincontent .= '<div><img src="'.$fotoktp.'" class="img-responsive" width="100px"></div>';
				}
				$admincontent .= '
					<input class="form-control" name="fotoktp" type="file" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _FOTOSERTIFIKAT . '</label>
				<div class="controls">';
				if ($row['fotosertifikat'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotosertifikat'])) {
					$fotosertifikat = $cfg_fullsizepics_url . '/' . $row['fotosertifikat'];
					$admincontent .= '<div><img src="'.$fotosertifikat.'" class="img-responsive" width="100px"></div>';
				}
				$admincontent .= '
					<input class="form-control" name="fotosertifikat" type="file" required>
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _FOTOLAIN1 . '</label>
				<div class="controls">';
				if ($row['filename_lain1'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain1'])) {
					$filename_lain1 = $cfg_fullsizepics_url . '/' . $row['filename_lain1'];
					$admincontent .= '<div><img src="'.$filename_lain1.'" class="img-responsive" width="100px"></div>';
				}
				$admincontent .= '
					<input class="form-control" name="fotolain1" type="file">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _FOTOLAIN2 . '</label>
				<div class="controls">';
				if ($row['filename_lain2'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain2'])) {
					$filename_lain2 = $cfg_fullsizepics_url . '/' . $row['filename_lain2'];
					$admincontent .= '<div><img src="'.$filename_lain2.'" class="img-responsive" width="100px"></div>';
				}
				$admincontent .= '
					<input class="form-control" name="fotolain2" type="file">
				</div>
			</div>

            <div class="control-group">
                    <div class="controls">
                            <input type="submit" name="submit" class="buton" value="' . _ADD . '">
                            <input type="submit" name="back" class="btn" value="' . _BACK . '">
                    </div>
            </div>
	</form>
    ';
}

if ($action == "modify") {
    if (isset($_POST['submit'])) {
        if ($_POST['active'] == "profile") {
            $notificationbuilder = "";
            $notificationbuilder .= validation($postnama, _FULLNAME, false);
            // $notificationbuilder .= validation($postalamat1, _ADDRESS, false);
            // $notificationbuilder .= validation($postkota, _CITY, false);
            $notificationbuilder .= validation($postponsel1, _MOBILE1, false);
            $notificationbuilder .= emailvalidation($postemail, true);
            $notificationbuilder .= validation($postkategori_hipnoterapi, _KATEGORI, false);
            //cek apakah email sudah terpakai
            $sql = "SELECT user_id FROM $tabelwebmember WHERE user_email='$postemail' AND user_id<>$pid";
            $result = $mysql->query($sql);
            if ($mysql->num_rows($result) > 0) {
                $notificationbuilder .= _EMAIL . " " . _ALREADYEXIST . "!<br/>";
            }

			if($_FILES['pasfoto']['name'] != '') {

				$hasilupload = fiestoupload('pasfoto', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
				if($hasilupload != _SUCCESS) {
					$notificationbuilder .= $hasilupload;
				}

				//ambil informasi basename dan extension
				$temp = explode(".",$_FILES['pasfoto']['name']);
				$extension_pasfoto = $temp[count($temp)-1];
				$basename_pasfoto = '';
				for ($i=0;$i<count($temp)-1;$i++) {
					$basename_pasfoto .= $temp[$i];
				}
			}

			if($_FILES['fotoktp']['name'] != '') {

				$hasilupload = fiestoupload('fotoktp', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
				if($hasilupload != _SUCCESS) {
					$notificationbuilder .= $hasilupload;
				}

				//ambil informasi basename dan extension
				$temp = explode(".",$_FILES['fotoktp']['name']);
				$extension_fotoktp = $temp[count($temp)-1];
				$basename_fotoktp = '';
				for ($i=0;$i<count($temp)-1;$i++) {
					$basename_fotoktp .= $temp[$i];
				}
			}

			if($_FILES['fotosertifikat']['name'] != '') {

				$hasilupload = fiestoupload('fotosertifikat', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
				if($hasilupload != _SUCCESS) {
					$notificationbuilder .= $hasilupload;
				}

				//ambil informasi basename dan extension
				$temp = explode(".",$_FILES['fotosertifikat']['name']);
				$extension_fotosertifikat = $temp[count($temp)-1];
				$basename_fotosertifikat = '';
				for ($i=0;$i<count($temp)-1;$i++) {
					$basename_fotosertifikat .= $temp[$i];
				}
			}

			if($_FILES['fotolain1']['name'] != '') {

				$hasilupload = fiestoupload('fotolain1', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
				if($hasilupload != _SUCCESS) {
					$notificationbuilder .= $hasilupload;
				}

				//ambil informasi basename dan extension
				$temp = explode(".",$_FILES['fotolain1']['name']);
				$extension_fotolain1 = $temp[count($temp)-1];
				$basename_fotolain1 = '';
				for ($i=0;$i<count($temp)-1;$i++) {
					$basename_fotolain1 .= $temp[$i];
				}
			}

			if($_FILES['fotolain2']['name'] != '') {

				$hasilupload = fiestoupload('fotolain2', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
				if($hasilupload != _SUCCESS) {
					$notificationbuilder .= $hasilupload;
				}

				//ambil informasi basename dan extension
				$temp = explode(".",$_FILES['fotolain2']['name']);
				$extension_fotolain2 = $temp[count($temp)-1];
				$basename_fotolain2 = '';
				for ($i=0;$i<count($temp)-1;$i++) {
					$basename_fotolain2 .= $temp[$i];
				}
			}

            //check for errors
            if ($notificationbuilder != "") {
                $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
            } else {
                /* $sql = "UPDATE $tabelwebmember SET fullname='$postnama',
					company='$postperusahaan', address1='$postalamat1', address2='$postalamat2',
					city='$postkota', zip='$postkodepos', telephone='$posttelepon',
					cellphone1='$postponsel1', cellphone2='$postponsel2', fax='$postfax',
					user_email='$postemail' WHERE user_id='$pid'"; */
				$dateofbirth = "$postybegindate-$postmbegindate-$postdbegindate";

				$sql="UPDATE $tabelwebmember SET
				fullname='$postnama', level='1',
				address1='$postalamat1',
				address2='$postalamat2', city='$postkota', zip='$postkodepos',
				telephone='$posttelepon', cellphone1='$postponsel1',
				cellphone2='$postponsel2', user_email='$postemail',
				user_regdate=NOW(), state='$postprovinsi1', nickname='$postnickname', gender='$postgender', placeofbirth='$postplaceofbirth', dateofbirth='$dateofbirth', noktp='$postnoktp', city2='$postkota2', state2='$postprovinsi2', pendidikan='$posteducation', pendidikan_lain='$postpendidikanlain', facebook='$postfacebook', instagram='$postinstagram', twitter='$posttwitter', google='$postgoogle', kategori_hipnoterapi='$postkategori_hipnoterapi', no_whatsapp='$postno_whatsapp' WHERE user_id='$pid' ";
                $result = $mysql->query($sql);
                if ($result) {

					if ($basename_pasfoto != '') {
						$pasfotofilename = "$basename_pasfoto-$pid.$extension_pasfoto";
						rename("$cfg_fullsizepics_path/".$_FILES['pasfoto']['name'],"$cfg_fullsizepics_path/$pasfotofilename");

						$sql = "UPDATE $tabelwebmember SET pas_foto='$pasfotofilename' WHERE user_id='$pid'";
						$result = $mysql->query($sql);
					}
					if ($basename_fotoktp != '') {
						$fotoktpfilename = "$basename_fotoktp-$pid.$extension_fotoktp";
						rename("$cfg_fullsizepics_path/".$_FILES['fotoktp']['name'],"$cfg_fullsizepics_path/$fotoktpfilename");

						$sql = "UPDATE $tabelwebmember SET fotoktp='$fotoktpfilename' WHERE user_id='$pid'";
						$result = $mysql->query($sql);
					}
					if ($basename_fotosertifikat != '') {
						$fotosertifikatfilename = "$basename_fotosertifikat-$pid.$extension_fotosertifikat";
						rename("$cfg_fullsizepics_path/".$_FILES['fotosertifikat']['name'],"$cfg_fullsizepics_path/$fotosertifikatfilename");

						$sql = "UPDATE $tabelwebmember SET fotosertifikat='$fotosertifikatfilename' WHERE user_id='$pid'";
						$result = $mysql->query($sql);
					}
					if ($basename_fotolain1 != '') {
						$fotolain1filename = "$basename_fotolain1-$pid.$extension_fotolain1";
						rename("$cfg_fullsizepics_path/".$_FILES['fotolain1']['name'],"$cfg_fullsizepics_path/$fotolain1filename");

						$sql = "UPDATE $tabelwebmember SET filename_lain1='$fotolain1filename' WHERE user_id='$pid'";
						$result = $mysql->query($sql);
					}
					if ($basename_fotolain2 != '') {
						$fotolain2filename = "$basename_fotolain2-$pid.$extension_fotolain2";
						rename("$cfg_fullsizepics_path/".$_FILES['fotolain2']['name'],"$cfg_fullsizepics_path/$fotolain2filename");

						$sql = "UPDATE $tabelwebmember SET filename_lain2='$fotolain2filename' WHERE user_id='$pid'";
						$result = $mysql->query($sql);
					}

                    header("Location:?p=$modulename&r=$random&notification=editsuccess");
                } else {
                    $builtnotification = createnotification(_DBERROR, _ERROR, "error");
                }
            }
            $activeProfile = "active";
            $activePassword = "";
        } else if ($_POST['active'] == "password") {
            $sql_get_username = "SELECT username FROM $tabelwebmember WHERE user_id='$pid'";
            $result_get_username = $mysql->query($sql_get_username);
            $row = $mysql->fetch_assoc($result_get_username);
            $notificationbuilder = "";
            $notificationbuilder .= validation($postpass1, _PASSWORD, false);
            $notificationbuilder .= validation($postpass2, _RETYPEPASS, false);
            if ($postpass1 != null || $postpass1 != "") {
                if ($postpass2 != null || $postpass2 != "") {
                    if ($postpass1 != $postpass2) {
                        $notificationbuilder .= _PASSWORDANDRETYPENOTSAME . "!<br/>";
                    }
                }
                if (strlen($postpass1) < 6) {
                    $notificationbuilder .= _CORRECTPASS . "!<br/>";
                }
                $pos = strpos($postpass1, $row['username']);
                if ($pos !== false) {
                    $notificationbuilder .= _USERINPASSWORD . "!<br/>";
                }
            }
            //cek apakah password termasuk kata-kata terlarang
            foreach ($prohibitedpasses as $prohibitedpass) {
                if (strtolower($postpass1) == $prohibitedpass) {
                    $notificationbuilder .= _PROHIBITEDPASS . "!<br/>";
                    break;
                }
            }
            //check for errors
            if ($notificationbuilder != "") {
                $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
            } else {
                $new_pass1 = fiestopass($postpass1);
                $sql = "UPDATE $tabelwebmember SET user_password='$new_pass1'
						WHERE user_id='$pid'";
                $result = $mysql->query($sql);
                if ($result) {
                    header("Location:?p=$modulename&r=$random&notification=editsuccess");
                } else {
                    $builtnotification = createnotification(_DBERROR, _ERROR, "error");
                }
            }
            $activeProfile = "";
            $activePassword = "active";
        }
    } else {
        /* default value untuk form */
        $sql = "SELECT *
			FROM $tabelwebmember WHERE user_id='$pid'";
        $result = $mysql->query($sql);
        $row = $mysql->fetch_assoc($result);
        $activeProfile = "active";
        $activePassword = "";
    }

	$select_provinci1 = '<select name="txtProvinci1" required>'.get_provinci($row['state']).'</select>';
	$select_provinci2 = '<select name="txtProvinci2" required>'.get_provinci($row['state2']).'</select>';
	$select_pendidikan_formal = '<select name="txtEducation">'.get_educations($row['pendidikan']).'</select>';

    $admintitle = _EDITNONPASSWORD;
    $admincontent .= '
        <ul class="nav nav-tabs" id="myTab2">
                    <li class="' . $activeProfile . '"><a href="#profile">' . _EDITNONPASSWORD . '</a></li>
                    <li class="' . $activePassword . '"><a href="#password">' . _EDITPASSWORD . '</a></li>
            </ul>
            <div class="tab-content">
                    <div class="tab-pane ' . $activeProfile . '" id="profile">
                        <form class=\"form-horizontal\" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="modify">
                            <input type="hidden" name="active" value="profile">
                            ' . $builtnotification . '
                            <span class="label label-warning">' . _REQNOTE . '</span>
                            <div class="control-group">
								<label class="control-label">' . _FULLNAME . '*</label>
								<div class="controls">
									<input type="text" name="txtNama" placeholder="' . _FULLNAME . '" class="span12" value="' . $row['fullname'] . '" required>
								</div>
                            </div>
                            <div class="control-group">
								<label class="control-label">' . _NICKNAME . '</label>
								<div class="controls">
									<input type="text" name="txtNickname" placeholder="' . _NICKNAME . '" class="span12" value="' . $row['nickname'] . '">
								</div>
                            </div>
							<div class="control-group">
								<span class="control-label">'._GENDER.' <span class="reqsign"></span></span>
								<label><input class="form-controlx" name="rdGender" type="radio" value="1" '.(($row['gender'] == 1) ? "checked" : "").'><span>'._LAKI.'</span></label><label><input class="form-controlx" name="rdGender" type="radio" value="2" '.(($row['gender'] == 2) ? "checked" : "").'><span>'._PEREMPUAN.'</span></label>
							</div>
							<div class="control-group">
								<label class="control-label">' . _PLACEOFBIRTH . '</label>
								<div class="controls">
									<input type="text" name="txtPlaceofbirth" placeholder="' . _PLACEOFBIRTH . '" class="span12" value="' . $row['placeofbirth'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _DATEEOFBIRTH . '</label>
								<div class="controls">';

								$dob = explode('-', $row['dateofbirth']);

								$admincontent .= "<select class=\"tanggal\" name=\"dbegindate\">";
								for ($i = 1; $i <= 31; $i++) {
									if ($dob[2] == $i) {
										$admincontent .= "<option value=\"$i\" selected>$i</option>\n";
									} else {
										$admincontent .= "<option value=\"$i\">$i</option>\n";
									}
								}
								$admincontent .= "</select>";

								$admincontent .= "<select class=\"bulan\" name=\"mbegindate\">";
								for ($i = 1; $i <= 12; $i++) {
									$j = $i - 1;
									if ($dob[1] == $i) {
										$admincontent .= "<option value=\"$i\" selected>$namabulan[$j]</option>\n";
									} else {
										$admincontent .= "<option value=\"$i\">$namabulan[$j]</option>\n";
									}
								}
								$admincontent .= "</select>";

								$admincontent .= "<select class=\"tahun\" name=\"ybegindate\">";
								for ($i = $prefix_tahun; $i <= date('Y'); $i++) {
									if ($dob[0] == $i) {
										$admincontent .= "<option value=\"$i\" selected>$i</option>\n";
									} else {
										$admincontent .= "<option value=\"$i\">$i</option>\n";
									}
								}
								$admincontent .= "</select>";
							$admincontent .= '
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _NOKTP . '</label>
								<div class="controls">
									<input type="text" name="txtNoktp" placeholder="' . _NOKTP . '" class="span12" value="' . $row['noktp'] . '">
								</div>
                            </div>
                            <div class="control-group">
								<label class="control-label">' . _ADDRESS . '</label>
								<div class="controls">
									<input type="text" name="txtAlamat1" placeholder="' . _ADDRESS . '" class="span12" value="' . $row['address1'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _CITY . '</label>
								<div class="controls">
									<input type="text" name="txtKota" placeholder="' . _CITY . '" class="span12" value="' . $row['city'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _PROVINCI . '</label>
								<div class="controls">
									'.$select_provinci1.'
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _ADDRESSTHISTIME . '</label>
								<div class="controls">
									<input type="text" name="txtAlamat2" placeholder="' . _ADDRESSTHISTIME . '" class="span12" value="' . $row['address2'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _CITY . '</label>
								<div class="controls">
									<input type="text" name="txtKota2" placeholder="' . _CITY . '" class="span12" value="' . $row['city2'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _PROVINCI . '</label>
								<div class="controls">
									'.$select_provinci2.'
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _PENDIDIKANFORMAL . '</label>
								<div class="controls">
									'.$select_pendidikan_formal.'
									<input type="text" name="txtPendidikanlain" class="span12" value="' . $row['pendidikan_lain'] . '" style="display:'.(($row['pendidikan_lain'] != "" && $row['pendidikan'] == 5) ? "block" : "none").'">
								</div>
                            </div>
							 <div class="control-group">
								<label class="control-label">' . _TELP1 . '</label>
								<div class="controls">
									<input type="text" name="txtTelepon" placeholder="' . _TELP1 . '" class="span12" value="' . $row['telephone'] . '">
								</div>
                            </div>
                            <div class="control-group">
								<label class="control-label">' . _MOBILE1 . '*</label>
								<div class="controls">
									<input type="text" name="txtPonsel1" placeholder="' . _MOBILE1 . '" class="span12" value="' . $row['cellphone1'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _NOWHATSAPP . '</label>
								<div class="controls">
									<input type="text" name="no_whatsapp" placeholder="' . _NOWHATSAPP . '" class="span12" value="' . $row['no_whatsapp'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _EMAIL . '*</label>
								<div class="controls">
									<input type="text" name="txtEmail" placeholder="' . _EMAIL . '" class="span12" value="' . $row['user_email'] . '">
								</div>
                            </div>
                            <div class="control-group">
								<label class="control-label">' . _FACEBOOK . '</label>
								<div class="controls">
									<input type="text" name="txtFacebook" placeholder="' . _FACEBOOK . '" class="span12" value="' . $row['facebook'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _INSTAGRAM . '</label>
								<div class="controls">
									<input type="text" name="txtInstagram" placeholder="' . _INSTAGRAM . '" class="span12" value="' . $row['instagram'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _TWITTER . '</label>
								<div class="controls">
									<input type="text" name="txtTwitter" placeholder="' . _TWITTER . '" class="span12" value="' . $row['twitter'] . '">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _GOOGLE . '</label>
								<div class="controls">
									<input type="text" name="txtGoogle" placeholder="' . _GOOGLE . '" class="span12" value="' . $row['google'] . '">
								</div>
                            </div>
                            <div class="control-group">
								<span class="control-label">'._KATEGORI.' <span class="reqsign">*</span></span>
								<label><input class="form-controlx" name="rdKategori" type="radio" value="1" required '.(($row['kategori_hipnoterapi'] == 1) ? "checked" : "").'><span>'._HIPNOTERAPIS.'</span></label><label><input class="form-controlx" name="rdKategori" type="radio" value="2" required '.(($row['kategori_hipnoterapi'] == 2) ? "checked" : "").'><span>'._HIPNOTERAPISKLINIS.'</span></label>
							</div>
							<br>
							<div class="control-group">
								<label class="control-label">' . _PASFOTO . '</label>
								<div class="controls">';
								if ($row['pas_foto'] != '' && file_exists("$cfg_fullsizepics_path/".$row['pas_foto'])) {
									$pas_foto = $cfg_fullsizepics_url . '/' . $row['pas_foto'];
									$admincontent .= '<div><img src="'.$pas_foto.'" class="img-responsive" width="100px"></div>';
								}
								$admincontent .= '
									<input class="form-control" name="pasfoto" type="file">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _FOTOKTP . '</label>
								<div class="controls">';
								if ($row['fotoktp'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotoktp'])) {
									$fotoktp = $cfg_fullsizepics_url . '/' . $row['fotoktp'];
									$admincontent .= '<div><img src="'.$fotoktp.'" class="img-responsive" width="100px"></div>';
								}
								$admincontent .= '
									<input class="form-control" name="fotoktp" type="file">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _FOTOSERTIFIKAT . '</label>
								<div class="controls">';
								if ($row['fotosertifikat'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotosertifikat'])) {
									$fotosertifikat = $cfg_fullsizepics_url . '/' . $row['fotosertifikat'];
									$admincontent .= '<div><img src="'.$fotosertifikat.'" class="img-responsive" width="100px"></div>';
								}
								$admincontent .= '
									<input class="form-control" name="fotosertifikat" type="file">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _FOTOLAIN1 . '</label>
								<div class="controls">';
								if ($row['filename_lain1'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain1'])) {
									$filename_lain1 = $cfg_fullsizepics_url . '/' . $row['filename_lain1'];
									$admincontent .= '<div><img src="'.$filename_lain1.'" class="img-responsive" width="100px"></div>';
								}
								$admincontent .= '
									<input class="form-control" name="fotolain1" type="file">
								</div>
                            </div>
							<div class="control-group">
								<label class="control-label">' . _FOTOLAIN2 . '</label>
								<div class="controls">';
								if ($row['filename_lain2'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain2'])) {
									$filename_lain2 = $cfg_fullsizepics_url . '/' . $row['filename_lain2'];
									$admincontent .= '<div><img src="'.$filename_lain2.'" class="img-responsive" width="100px"></div>';
								}
								$admincontent .= '
									<input class="form-control" name="fotolain2" type="file">
								</div>
                            </div>

                            <div class="control-group">
								<div class="controls">
										<input type="submit" name="submit" class="buton" value="' . _EDIT . '">
										<input type="submit" name="back" class="btn" value="' . _BACK . '">
								</div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane ' . $activePassword . '" id="password">
                        <form class=\"form-horizontal\" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="modify">
                            <input type="hidden" name="active" value="password">
                            ' . $builtnotification . '
                            <div class="control-group">
                                    <label class="control-label">' . _ENTERNEWPASSWORD1 . '*</label>
                                    <div class="controls">
                                            <input type="password" name="txtPass1" placeholder="' . _ENTERNEWPASSWORD1 . '" class="span12" value="' . $postpass1 . '">
                                    </div>
                            </div>
                            <div class="control-group">
                                    <label class="control-label">' . _ENTERNEWPASSWORD2 . '</label>
                                    <div class="controls">
                                            <input type="password" name="txtPass2" placeholder="' . _ENTERNEWPASSWORD2 . '" class="span12" value="' . $postpass2 . '">
                                    </div>
                            </div>
                            <div class="control-group">
                                    <div class="controls">
                                            <input type="submit" name="submit" class="buton" value="' . _EDIT . '">
                                            <input type="submit" name="back" class="btn" value="' . _BACK . '">
                                    </div>
                            </div>
                        </form>
                    </div>
            </div>
            ';
}

if ($action == 'excel') {

	 if ($status == "0") {
        $judulstatus = _NEWMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='0' ";
        $sql .= "AND activedate='0000-00-00 00:00:00' ";
        /*
          if($expired_periode > 0)
          {	$sql .= "AND DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'), DATE_FORMAT(activedate,'%Y-%m-%d')) <= '$expired_periode' ";
          }
         */
        if ($action == 'search') {
            $sql = "AND (username LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        }
        $sql .= "ORDER BY user_regdate ASC ";
    } elseif ($status == "1") {
        $judulstatus = _ACTIVEMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='1' ";
        if ($action == 'search') {
            $sql .= "AND (username LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        }
        $sql .= "ORDER BY username ASC ";
        $jumlah_kolom = 6;
    } elseif ($status == "2") {
        $judulstatus = _EXPIREDMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='2' ";
        // $sql .= "AND activedate='0000-00-00 00:00:00' ";

        /* if ($expired_periode > 0) {
            $sql .= "AND DATEDIFF(DATE_FORMAT(NOW(),'%Y-%m-%d'), DATE_FORMAT(activedate,'%Y-%m-%d')) > '$expired_periode' ";
        }
        if ($action == 'search') {
            $sql .= "AND (username LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        } */
        $sql .= "ORDER BY user_regdate ASC ";
    } elseif ($status == "3") {
        $judulstatus = _BANNEDMEMBER;
        $sql = "SELECT * FROM $tabelwebmember WHERE activity='0' ";
        $sql .= "AND bandate!='0000-00-00 00:00:00' ";
        if ($action == 'search') {
            $sql .= "AND (username LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
        }
        $sql .= "ORDER BY bandate DESC ";
        $jumlah_kolom = 5;
    }

	/** Error reporting */
	ob_get_clean();

	/** Include PHPExcel */
	//require_once "$cfg_app_path/aplikasi/phpexcel/Classes/PHPExcel.php";
	require_once dirname(__FILE__) . '/../../aplikasi/phpexcel/Classes/PHPExcel.php';

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	$objPHPExcel->setActiveSheetIndex(0);
	// Initialise the Excel row number
	$rowCount = 1;

	if ($result = $mysql->query($sql)) {
		$total_records = $mysql->num_rows($result);
		if ($total_records == 0) {
			$action = createmessage(_NOTRANSACTION, _INFO, "info", "");
		} else {

			$temp_columns = array(
				_FULLNAME,
				_NICKNAME,
				_GENDER,
				_PLACEOFBIRTH,
				_DATEEOFBIRTH,
				_NOKTP,
				_ADDRESS,
				_CITY,
				_PROVINCI,
				_ADDRESSTHISTIME,
				_CITY,
				_PROVINCI,
				_PENDIDIKANFORMAL,
				_TELP1,
				_MOBILE1,
				_NOWHATSAPP,
				_EMAIL,
				_FACEBOOK,
				_INSTAGRAM,
				_TWITTER,
				_GOOGLE,
				_KATEGORI
			);

			$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $temp_columns[0]);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $temp_columns[1]);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $temp_columns[2]);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $temp_columns[3]);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $temp_columns[4]);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $temp_columns[5]);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $temp_columns[6]);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $temp_columns[7]);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $temp_columns[8]);
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $temp_columns[9]);
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $temp_columns[10]);
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $temp_columns[11]);
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $temp_columns[12]);
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $temp_columns[13]);
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $temp_columns[14]);
			$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $temp_columns[15]);
			$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $temp_columns[16]);
			$objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $temp_columns[17]);
			$objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $temp_columns[18]);
			$objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, $temp_columns[19]);
			$objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, $temp_columns[20]);
			$objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, $temp_columns[21]);
			$rowCount++;

			while($row = $mysql->fetch_assoc($result)) {
				// $no_urut++;

				$gender = ($row['gender'] == 1) ? _LAKI : _PEREMPUAN;
				$dateofbirth = $row['dateofbirth'];
				$kategori = ($row['kategori_hipnoterapi'] == 1) ? _HIPNOTERAPIS : _HIPNOTERAPISKLINIS;

				$pendidikan_terakhir = ($row['pendidikan'] == 5 && $row['pendidikan_lain'] != '') ? $row['pendidikan_lain'] : $row['pendidikan'];

				$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $row['fullname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $row['nickname']);
				$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $gender);
				$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $row['placeofbirth']);
				$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $dateofbirth);
				$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $row['noktp']);
				$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $row['address1']);
				$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $row['city']);
				$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, get_provinci_name($row['state']));
				$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $row['address2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $row['city2']);
				$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, get_provinci_name($row['state2']));
				$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $pendidikan_terakhir);
				$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $row['telephone']);
				$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $row['no_whatsapp']);
				$objPHPExcel->getActiveSheet()->SetCellValue('P'.$rowCount, $row['cellphone1']);
				$objPHPExcel->getActiveSheet()->SetCellValue('Q'.$rowCount, $row['user_email']);
				$objPHPExcel->getActiveSheet()->SetCellValue('R'.$rowCount, $row['facebook']);
				$objPHPExcel->getActiveSheet()->SetCellValue('S'.$rowCount, $row['instagram']);
				$objPHPExcel->getActiveSheet()->SetCellValue('T'.$rowCount, $row['twitter']);
				$objPHPExcel->getActiveSheet()->SetCellValue('U'.$rowCount, $row['google']);
				$objPHPExcel->getActiveSheet()->SetCellValue('V'.$rowCount, $kategori);

				/* switch ($status_order) {
					case '0':
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, _ORDERCANCEL);
						break;
					case '1':
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, _WAITINGFORPAYMENT);
						break;
					case '2':
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, _ORDERCONFIRMED);
						break;
					case '3':
						$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, _WAITINGTOSEND);
						break;
					case '4':
						$no_pengiriman = $row['no_pengiriman'];
						if ($no_pengiriman == '') {
							$admincontent .= _SENT;
							$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, _SENT);
						} else {
							$send_to_courier = _SENT . ", " . _NOPENGIRIMAN . " $no_pengiriman";
							$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $send_to_courier);
						}
						break;
				} */

				// Increment the Excel row counter
				// $objPHPExcel->getActiveSheet()->getColumnDimension('A'.$rowCount)->setAutoSize(true);
				$nCols = 11; //set the number of columns

				foreach (range(0, $nCols) as $col) {
					$objPHPExcel->getActiveSheet()->getColumnDimensionByColumn($col)->setAutoSize(true);
				}

				$rowCount++;
			}

			$objPHPExcel->getActiveSheet()->getStyle("A1:V1")->getFont()->setBold(true);

			$ext = ".xls";
			$filename = "anggota".date('Y-m-d H:i:s').$ext;

		}
	}

	// Redirect output to a client’s web browser (Excel5)
	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="'.$filename.'"');
	header('Cache-Control: max-age=0');
	// If you're serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');

	// // If you're serving to IE over SSL, then the following may be needed
	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header ('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	exit;
}

if ($action == 'pdf') {
	$sql = "SELECT * FROM $tabelwebmember WHERE activity='2' AND user_id='$pid' ";
	if ($action == 'search') {
		$sql .= "AND (nicknam LIKE '%$keyword%' OR fullname LIKE '%$keyword%') ";
	}
	$result = $mysql->query($sql);

	if ($mysql->num_rows($result) > 0) {

		$row = $mysql->fetch_assoc($result);
		// Include the main TCPDF library (search for installation path).
		require_once('../aplikasi/tcpdf/config/tcpdf_include.php');


		// Extend the TCPDF class to create custom Header and Footer
		class MYPDF extends TCPDF {

			//Page header
			public function Header() {
				// Logo
			}

			public function Footer() {

			}

		}

		// create new PDF document
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		$sql = "SELECT judul FROM module WHERE nama='webmember'";
		$result = $mysql->query($sql);
		list($title) = $mysql->fetch_row($result);

		// set document information
		$pdf->SetCreator('Fiesto');
		$pdf->SetAuthor('Fiesto');
		$pdf->SetTitle($title);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins

		$pdf->SetAutoPageBreak(true, 30);
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP +17, PDF_MARGIN_RIGHT);

		// set image scale factor
		// $pdf->SetAutoPageBreak(TRUE, 30)
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('dejavusans', '', 10, '', true);

		// add a page
		$pdf->AddPage();

		$pendidikan_terakhir = ($row['pendidikan'] != "" && $row['pendidikan_lain'] != "") ? $row['pendidikan_lain'] : $educations[$row['pendidikan']];

		$htmlmsg = "
		<h1 style=\"text-align:center\">FORMULIR PENDAFTARAN KEANGGOTAAN</h1>
		<table>
		<tr>
			<td style=\"width:65%\">
				<div class=\"panel panel-default\">
					<h3>"._DATADIRI."</h3>
					<div class=\"panel-body\">
						<table cellpadding=\"3\">
							<tbody>
								<tr>
									<td style=\"width:45%\">"._NAME."</td>
									<td>: ".$row['fullname']."</td>
								</tr>
								<tr>
									<td>"._NICKNAME."</td>
									<td>: ".$row['nickname']."</td>
								</tr>
								<tr>
									<td>"._GENDER."</td>
									<td>: ".($row['gender'] == 1 ? _LAKI : _PEREMPUAN)."</td>
								</tr>
								<tr>
									<td>"._PLACEOFBIRTH."</td>
									<td>: ".$row['placeofbirth']."</td>
								</tr>
								<tr>
									<td>"._DATEEOFBIRTH."</td>
									<td>: ".tglformat($row['dateofbirth'])."</td>
								</tr>
								<tr>
									<td>"._NOKTP."</td>
									<td>: ".$row['noktp']."</td>
								</tr>
								<tr>
									<td>"._ADDRESS."</td>
									<td>: ".$row['address1']."</td>
								</tr>
								<tr>
									<td>"._CITY."</td>
									<td>: ".$row['city']."</td>
								</tr>
								<tr>
									<td>"._PROVINCI."</td>
									<td>: ".get_provinci_name($row['state'])."</td>
								</tr>
								<tr>
									<td>"._ADDRESSTHISTIME."</td>
									<td>: ".$row['address2']."</td>
								</tr>
								<tr>
									<td>"._CITY."</td>
									<td>: ".$row['city2']."</td>
								</tr>
								<tr>
									<td>"._PROVINCI."</td>
									<td>: ".get_provinci_name($row['state2'])."</td>
								</tr>
								<tr>
									<td>"._PENDIDIKANFORMAL."</td>
									<td>: ".$pendidikan_terakhir."</td>
								</tr>
								<tr>
									<td>"._TELP1."</td>
									<td>: ".$row['telephone']."</td>
								</tr>
								<tr>
									<td>"._NOWHATSAPP."</td>
									<td>: ".$row['no_whatsapp']."</td>
								</tr>
								<tr>
									<td>"._MOBILE1."</td>
									<td>: ".$row['cellphone1']."</td>
								</tr>
								<tr>
									<td>"._EMAIL."</td>
									<td>: ".$row['user_email']."</td>
								</tr>";
						if ($row['facebook'] != "") {
							$htmlmsg .= "
								<tr>
									<td>"._FACEBOOK."</td>
									<td>: <a href=\"".$row['facebook']."\">".$row['facebook']."</a></td>
								</tr>
							";
						}

						if ($row['instagram'] != "") {
							$htmlmsg .= "
								<tr>
									<td>"._INSTAGRAM."</td>
									<td>: <a href=\"".$row['instagram']."\">".$row['instagram']."</a></td>
								</tr>
							";
						}

						if ($row['twitter'] != "") {
							$htmlmsg .= "
								<tr>
									<td>"._TWITTER."</td>
									<td>: <a href=\"".$row['twitter']."\">".$row['twitter']."</a></td>
								</tr>
							";
						}

						if ($row['google'] != "") {
							$htmlmsg .= "
								<tr>
									<td>"._GOOGLE."</td>
									<td>: <a href=\"".$row['google']."\">".$row['google']."</a></td>
								</tr>
							";
						}

						$htmlmsg .= "
								<tr>
									<td>"._KATEGORI."</td>
									<td>: ".($row['kategori_hipnoterapi'] == 1 ? _HIPNOTERAPIS : _HIPNOTERAPISKLINIS)."</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</td>
		";

		$htmlmsg .= "
			<td style=\"width:35%\">
				<div class=\"panel panel-default\">
					<h3 style=\"text-align:center\">".PERSYARATAN."</h3>
					<table cellpadding=\"5\" style=\"text-align:center;width:100%\">
					<tr>
					";
					if ($row['pas_foto'] != '' && file_exists("$cfg_fullsizepics_path/".$row['pas_foto'])) {
						$pas_foto = $cfg_fullsizepics_url . '/' . $row['pas_foto'];
						$htmlmsg .= '
						<td style="width:48%">
							<img width="60px" src="'.$pas_foto.'" class="img-responsive"><br/>
							<span>Pas Foto</span>
							<p></p>
						</td>
						';
					}
					$htmlmsg .= '<td style="width:4%"></td>';
					if ($row['fotoktp'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotoktp'])) {
						$fotoktp = $cfg_fullsizepics_url . '/' . $row['fotoktp'];
						$htmlmsg .= '
						<td class="col-xs-6 col-md-3" style="width:48%">
							<img width="60px" src="'.$fotoktp.'" class="img-responsive"><br/>
							<span>Foto KTP</span>
							<p></p>
						</td>
						';
					}
					$htmlmsg .= '</tr>
					<tr>';
					if ($row['fotosertifikat'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotosertifikat'])) {
						$fotosertifikat = $cfg_fullsizepics_url . '/' . $row['fotosertifikat'];
						$htmlmsg .= '
						<td class="col-xs-6 col-md-3">
							<img width="60px" src="'.$fotosertifikat.'" class="img-responsive"><br/>
							<span>Foto Sertifikat</span>
							<p></p>
						</td>
						';
					}
					$htmlmsg .= '<td></td>';
					if ($row['filename_lain1'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain1'])) {
						$filename_lain1 = $cfg_fullsizepics_url . '/' . $row['filename_lain1'];
						$htmlmsg .= '
						<td class="col-xs-6 col-md-3">
							<img width="60px" src="'.$filename_lain1.'" class="img-responsive"><br/>
							<span>Foto lain 1</span>
							<p></p>
						</td>
						';
					}
					$htmlmsg .= '</tr>
					<tr>';
					if ($row['filename_lain2'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain2'])) {
						$filename_lain2 = $cfg_fullsizepics_url . '/' . $row['filename_lain2'];
						$htmlmsg .= '
						<td class="col-xs-6 col-md-3">
							<img width="60px" src="'.$filename_lain2.'" class="img-responsive"><br/>
							<span>Foto lain 2</span>
							<p></p>
						</td>
						';
					}
					$htmlmsg.= "
					</tr>
					</table>
				</div>
			</td>
			</tr>
		</table>
		";

		$htmlmsg .= ob_get_clean();

		$pdf->writeHTMLCell(0, 0, '', '', $htmlmsg, 0, 1, 0, true, '', true);

		$str_filename = str_replace('/','_',$txid);
		$filename = "$str_filename.pdf";

		$pdf->Output($filename, 'I');
		#end region quotation pdf
	}

}

if ($action == 'renew') {
	
	$daftar_id = $_POST["cekMember"];
    $daftar_id = checkrequired($daftar_id, _NOCHECK);
    $list_id = implode(",", $daftar_id);
	
	if (in_array($bulan_sekarang, $kelompok_awal)) {
		$expired = date('Y-m-d', $tahun_sekarang);
	} else {
		$expired = date('Y-m-d', $tahun_depan);
	}

    $sql = "UPDATE $tabelwebmember SET expired='$expired', alert='0', isrenew='0' WHERE user_id IN ($list_id) ";
	if (!$mysql->query($sql)) {
		header("Location:?p=$modulename&notification=dberror");
    } else {
		if (count($daftar_id) > 0) {
			$http_server = "$cfg_app_url/webmember/confirm_payment/";
			$title_tag = $config_site_titletag;
			// $subject = 'Email sebentar lagi aktif, silakan melakukan transfer';
			foreach($daftar_id as $user_id) {
				$sql = "SELECT user_email, fullname, kategori_hipnoterapi, gender, kode_digit FROM webmember WHERE user_id=$user_id";
				$result = $mysql->query($sql);
				list($user_email, $fullname, $kategori_hipnoterapi, $gender, $kode_digit) = $mysql->fetch_row($result);

				// include("$cfg_app_path/modul/$namamodul/lang/$lang/mailactv.php");
				if ($gender != 0) {
					$gender_prefix = ($gender == 1) ? _BAPAK : _IBU;
				}

				$nominal = ($kategori_hipnoterapi == 1) ? substr($amountcats[$kategori_hipnoterapi], 0, 3) . $kode_digit : substr($amountcats[$kategori_hipnoterapi], 0, 4) . $kode_digit;
				$terbilang = bilangan($nominal);
				$kode = base64_encode($user_id);
				$html = array(
					'/#namaanggota/' => $fullname,
					'/#gender/' => $gender_prefix,
					'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>",
					'/#nominalkategori/' => "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>",
					'/#httpserver/' => '<a href="'.$http_server.$kode.'">'.$http_server.$kode.'</a>'
				);
				$message = preg_replace(array_keys($html), array_values($html), $email_anggota_konfirmasi);
				fiestophpmailer($user_email,_SUBJECTANGGOTAKONFIRMASIRENEW,$message,$smtpuser,$sendername,$smtpuser, $message);
			}
		}

        header("Location:?p=$modulename&r=$random&notification=activationsuccess");
    }
}

if ($action == 'listrenew') {
	
	$admintitle = _LISTRENEW;
	
	$sql = "SELECT * FROM webmember WHERE isrenew=1";
	$result = $mysql->query($sql);

	if ($mysql->num_rows($result) > 0) {
        $start = $screen * $max_page_list;
        $sql .= " LIMIT $start, $max_page_list ";
        $result = $mysql->query($sql);

        if ($pages > 1) {
            $adminpagination = pagination($namamodul, $screen, "&action=&status=$status");
        }

        $admincontent .= "<form name=\"myform\" id=\"myform\" method=\"POST\" action=\"$thisfile\" >";
		$admincontent .= "<input type=\"hidden\" name=\"action\" value=\"renew\">";
		/* if ($status == 0) {
			$admincontent .= "<input type=\"hidden\" name=\"action\" value=\"konfirmasi\">";
		}
		if ($status == 1 || $status == 2) {
			$admincontent .= "<input type=\"hidden\" name=\"action\" value=\"aktivasi\">";
		} */

        $admincontent .= "<table class=\"stat-table\">\n";
        $admincontent .= "<tr>";
        $admincontent .= "<th>&nbsp;</th>";
        $admincontent .= "<th>" . _NAME . "</th>";
        $admincontent .= "<th>" . _EMAIL . "</th>";
        // $admincontent .= "<th>" . _LEVEL . "</th>";
        $admincontent .= "<th>" . _TIMEEXPIRED . "</th>";
        $admincontent .= "<th>" . _TIMENEXTEXPIRED . "</th>";
        $admincontent .= "<th align=\"center\">" . _ACTION . "</th>";
        //$admincontent .= "<th>"._DEL."</th>";
        $admincontent .= "</tr>\n";

        while ($data_member = $mysql->fetch_assoc($result)) {

            $member_id = $data_member["user_id"];
            $admincontent .= "<tr valign=\"top\">";
            $admincontent .= "<td>";
            $admincontent .= "<input type=\"checkbox\" name='cekMember[]' id='cekMember[]'  value='" . $member_id . "'>";
            $admincontent .= "</td>";
            $admincontent .= "<td>" . $data_member["fullname"] . "&nbsp;</td>";
            $admincontent .= "<td>" . $data_member["user_email"] . "</td>";
            // $admincontent .= "<td>" . view_level_name($data_member["level"]) . "</td>";
            // $admincontent .= "<td>" . $data_member["level"] . "</td>";
            $admincontent .= "<td>" . tglformat($data_member["expired"]) . "</td>";
            $admincontent .= "<td>". tglformat(get_expired())."</td>";
            // if ($status == "1") {
                // $admincontent .= "<td>" . tglformat($data_member["activedate"]) . "</td>";
            // }
            if ($status == "3") {
                $admincontent .= "<td>" . tglformat($data_member["bandate"]) . "</td>";
            }
            // $admincontent .= "<td align=\"center\"><a href=\"?p=webmember&action=modify&pid=$member_id\">";
            // $admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";

			$admincontent .= "<td align=\"center\" class=\"action-ud\">";
			$admincontent .= "<a href=\"".param_url("?p=$modulename&action=modify&pid=$member_id")."\"><img alt=\"" . _EDIT . "\" border=\"0\" src=\"../images/modify.gif\"></a>\r\n";

			if ($status == 2) {
				$admincontent .= "<a href=\"".param_url("?p=$modulename&action=pdf&pid=$member_id")."\"><img alt=\"" . _PDF . "\" border=\"0\" src=\"../images/pdf-icon.svg\" width=\"20px\"></a></td>\n";
			}
			$admincontent .= "</tr>\n";

            //$admincontent .= "<td>"._EDIT."</td>";
            //$admincontent .= "<td>"._DEL."</td>";
            $admincontent .= "</tr>\n";
        }

		$jumlah_kolom = 6;
        $admincontent .= "<tr>";
        $admincontent .= "<td>";
        $admincontent .= "&nbsp;";
        $admincontent .= "</td>";
        $admincontent .= "<td valign=\"top\" colspan=\"$jumlah_kolom\">";
		
		$event = "document.myform.elements['action'].value='ban'; document.getElementById('myform').submit(); ";
		$admincontent .= "<input class=\"buton\" type=\"button\" value=\"" . _DELETE . "\" onclick=\"$event\" />\r\n";
		$admincontent .= "&nbsp;&nbsp;";
		
        $event = "document.myform.elements['action'].value='renew'; document.getElementById('myform').submit(); ";
		$admincontent .= "<input class=\"btn btn-success\" type=\"button\" value=\"" . _CONFIRM . "\" onclick=\"$event\" />\r\n";
		$admincontent .= "&nbsp;&nbsp;";
        $admincontent .= "</td>";
        $admincontent .= "</tr>";
        $admincontent .= "</table>";
        $admincontent .= "</form>";
    } else {
        if ($action == 'search') {
            $admincontent .= createstatus(_NOSEARCHRESULTS, "error");
        } else {
            $admincontent .= createstatus(_NOMEMBER, "error");
        }
    }
}