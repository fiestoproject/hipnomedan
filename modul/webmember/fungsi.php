<?php
function get_nama_provider($id)
{
	$sql = "SELECT nama FROM provider WHERE id='$id'";
	$result = $mysql->query($sql);
	$hasil = $mysql->fetch_row($result);
	return $hasil[0];
}

function get_nominal_produk($id)
{
	$sql = "SELECT FLOOR(b.nominal) FROM provider a INNER JOIN produk b ON a.id=b.id_provider WHERE b.id='$id'";
	$result = $mysql->query($sql);
	$hasil = $mysql->fetch_row($result);
	return $hasil[0];
}

function get_provinci($provinci_id = '') {
	global $mysql;
	$sql = "SELECT id, name FROM provinsi ORDER BY name";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) > 0) {
		$data = '';
		while($row = $mysql->fetch_assoc($result)) {
			$selected  = ($row['id'] == $provinci_id) ? "selected" : "";
			$data .= '<option value="'.$row['id'].'" '.$selected.'>'.$row['name'].'</option>';
		}
	}
	
	return $data;
}

function get_provinci_name($provinci_id = '') {
	global $mysql;
	$sql = "SELECT id, name FROM provinsi WHERE id='$provinci_id'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) > 0) {
		list($id, $name) = $mysql->fetch_row($result);
		return $name;
	}
}

function get_educations($education_id = '') {
	global $educations;
	if (count($educations) > 0) {
		$select_pendidikan_formal = '';
		foreach($educations as $idx => $edu) {
			if ($education_id != '') {
				$selected  = ($idx == $education_id) ? "selected" : "";
			}
			$select_pendidikan_formal .= '<option value="'.$idx.'" '.$selected.'>'.$edu.'</option>';
		}
	}
	
	return $select_pendidikan_formal;
}

function selisihHari($day1, $day2) {
	$temp = ($day1-$day2) / (3600*24);
	return $temp;
}

function get_expired() {
	global $tahun_sekarang, $tahun_depan, $bulan_sekarang, $kelompok_awal;
	if (in_array($bulan_sekarang, $kelompok_awal)) {
		$expired = date('Y-m-d', $tahun_sekarang);
	} else {
		$expired = date('Y-m-d', $tahun_depan);
	}	
	
	return $expired;
}
// function get_dropdown_level($level = '') {
	// $sql = "SELECT level, nama FROM webmember_level ORDER BY level";
	// $result = $mysql->query($sql);
	// if ($mysql->num_rows($result) > 0) {
		// $data = '';
		// while($row = $mysql->fetch_assoc($result)) {
			// $selected  = ($row['level'] == $level) ? "selected" : "";
			// $data .= '<option value="'.$row['level'].'" '.$selected.'>'.$row['nama'].'</option>';
		// }
	// }
	
	// return $data;
// }

// function view_level_name($level) {
	// $q = $mysql->query("SELECT nama FROM webmember_level WHERE level = '$level' LIMIT 1");
	// if ($mysql->num_rows($q) > 0) {
		// $row = $mysql->fetch_assoc($q);
		// return $row['nama'];
	// }
// }
?>
