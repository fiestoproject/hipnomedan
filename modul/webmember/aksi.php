<?php
$userid = $_SESSION['member_uid'];
$action = fiestolaundry($_GET['action'],20);
$pid = fiestolaundry($_GET['pid'],20);

$postuser= fiestolaundry($_POST['txtUser'],50);
$postpass1= trim($_POST['txtPass1']);
$postpass2= trim($_POST['txtPass2']);
$postnama= fiestolaundry($_POST['txtNama'],50);
$postperusahaan= fiestolaundry($_POST['txtPerusahaan'],50);
$postalamat1= fiestolaundry($_POST['txtAlamat1'],50);
$postalamat2= fiestolaundry($_POST['txtAlamat2'],50);
$postkota= fiestolaundry($_POST['txtKota'],30);
$postkodepos= fiestolaundry($_POST['txtKodepos'],10);
$posttelepon= fiestolaundry($_POST['txtTelepon'],20);
$postponsel1= fiestolaundry($_POST['txtPonsel1'],20);
$postponsel2= fiestolaundry($_POST['txtPonsel2'],20);
$postfax= fiestolaundry($_POST['txtFax'],20);
$postemail= fiestolaundry($_POST['txtEmail'],50);

$postnickname = fiestolaundry($_POST['txtNickName'], 30);
$postgender = fiestolaundry($_POST['rdGender'], 1);
$postplaceofbirth = fiestolaundry($_POST['txtPlaceofbirth'], 255);
$postdbegindate = $_POST['dbegindate'];
$postmbegindate = $_POST['mbegindate'];
$postybegindate = $_POST['ybegindate'];
$postnoktp = fiestolaundry($_POST['txtNoktp'], 16);
$postprovinsi1 = fiestolaundry($_POST['txtProvinci1'], 30);
$postprovinsi2 = fiestolaundry($_POST['txtProvinci2'], 30);
$postkota2= fiestolaundry($_POST['txtKota2'],30);
$posteducation = fiestolaundry($_POST['txtEducation'],11);
$postpendidikan_lain = fiestolaundry($_POST['education_other'],20);
$postfacebook = fiestolaundry($_POST['txtFacebook'],255);
$postinstagram = fiestolaundry($_POST['txtInstagram'],255);
$posttwitter = fiestolaundry($_POST['txtTwitter'],255);
$postgoogle = fiestolaundry($_POST['txtGoogle'],255);
$postkategori_hipnoterapi = fiestolaundry($_POST['rdKategori'],1);
$postno_whatsapp = fiestolaundry($_POST['txtWhatsapp'],15);

$is_forced_login = false;

$postuserlogin = fiestolaundry($_POST['username'],50);

$sql = "SELECT judulfrontend FROM module WHERE nama='webmember'";
$result = $mysql->query($sql);
list($title) = $mysql->fetch_row($result);

if ($action=="")
{
	if($is_public_register)
	{
		if (!isset($_SESSION['member_uid']))
		{
			header("Location:".$urlfunc->makePretty("?p=webmember&action=register"));
		}
		else
		{
			// header("Location:".$cfg_app_url);
			// $content .= "<ul class=\"member_menu\">\r\n";
			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=webmember")."\">"._MEMBERHOME."</a></li>\r\n";
			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=webmember&action=editaccount")."\">"._EDITACCOUNT."</a></li>\r\n";
			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=order&action=history")."\">"._CEKSTATUSORDER."</a></li>\r\n";
			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=webmember&action=logout")."\">"._LOGOUT."</a></li>\r\n";
			// $content .= "</ul>\r\n";
			
			$user_id = $_SESSION['member_uid'];
			$sql = "SELECT alert, expired FROM webmember WHERE user_id='$user_id'";
			$result = $mysql->query($sql);
			list($alert, $expired) = $mysql->fetch_row($result);
			
			$tanggal1 = new DateTime($expired);
			$tanggal2 = new DateTime();
			$dayscompare = $tanggal2->diff($tanggal1)->format("%a");

			if ($alert > 0) {
				$user_id = base64_encode($user_id);
				$content .= "<div class=\"alert alert-warning text-center\">Akun Anda akan expired <strong>$dayscompare hari</strong> lagi, klik <a href=\"".$urlfunc->makePretty("?p=webmember&action=renew&pid=$user_id")."\">disini</a> untuk perpanjang masa aktif member</div>";
			}

			$content .= "<div class=\"panel panel-default\">";
			// $content .= "<div class=\"panel-heading\">Panel heading without title</div>";
			$content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=webmember")."\">"._MEMBERHOME."</a></div>";
			$content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=webmember&action=editaccount")."\">"._EDITACCOUNT."</a></div>";
			$content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=ebook")."\">"._EBOOK."</a></div>";
			$content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=webmember&action=logout")."\">"._LOGOUT."</a></div>";
			$content .= "</div>";

			// $content .= "<div class=\"panel panel-default\">";
			// $content .= "<div class=\"panel-heading\">"._POTITLE."</div>";
			// $content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=po&action=list")."\">"._POLIST."</a></div>";
			// // $content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=po&action=confirm")."\">"._POCONFIRMATION."</a></div>";
			// $content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=po&action=history")."\">"._POHISTORY."</a></div>";
			// $content .= "<div class=\"panel-body\"><a href=\"".$urlfunc->makePretty("?p=wishlist")."\">"._POWISHLIST."</a></div>";
			// $content .= "</div>";

			// $content .= "<h3>"._POTITLE."</h3>";
			// $content .= "<ul class=\"member_po\">";

			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=po&action=list")."\">"._POLIST."</a></li>\r\n";
			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=po&action=confirm")."\">"._POCONFIRMATION."</a></li>\r\n";
			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=po&action=history")."\">"._POHISTORY."</a></li>\r\n";
			// $content .= "<li><a href=\"".$urlfunc->makePretty("?p=po&action=whitelist")."\">"._POWHITELIST."</a></li>\r\n";
			// $content .= "</ul>";
		}
	}
	else
	{
		$content.=_CONTACTADMINTOREGISTER;
	}
}

if ($action=="checkout")
{

	if($is_public_register)
	{
		if (!isset($_SESSION['member_uid']))
		{
			$tambahan = ($pid != '') ? "&pid=".$pid : "";
			$content .= '
			<div id="formlogin" class="col-xs-12 col-sm-6 col-lg-6">
				<form class="box" action="'.$urlfunc->makePretty("?p=webmember&action=login".$tambahan).'" id="loginform" name="loginform" method="POST">
					<h3 class="page-subheading">'. _CHOSENLOGIN .'</h3>
					<div class="form_content clearfix">
						<div class="form-group">
							<label for="username">'._EMAIL.'</label>
							<input type="text" value="'.$postuserlogin.'" name="username" class="is_required validate account_input form-control"/>
						</div>
						<div class="form-group">
							<label for="password">'. _PASSWORD .'</label>
							<input type="password" value="" name="password" class="is_required validate account_input form-control"/>
						</div>
						<p class="lost_password form-group">
							<a id="webmember_forgot" href="'.$urlfunc->makePretty("?p=webmember&action=forgotpassword").'">'._DOYOUFORGET.'</a>
						</p>
						<table border="0">
							<tr>
								<td>
									<p class="submit">
										<input type="hidden" name="check_login" value="1"/>
										<button class="more button btn btn-default button-medium" name="submit_login" type="submit">
											<span>
												<i class="fa fa-lock left"></i>
												'._LOGIN.'
											</span>
										</button>
									</p>
								</td>
							</tr>
						</table>
					</div>
				</form>
			</div>';

			$content .= '
			<div class="col-xs-12 col-sm-6 col-lg-6">
				<form class="box">
					<h3 class="page-subheading">'._SHOPPINGASGUESTORMEMBER.'</h3>
					<div class="form_content clearfix">
						<div class="form-group">';
					if($harusmember==2)
					{
					$label=ucfirst(strtolower(_SHOPPINGWITHOUTREGISTER));
					$content .= "<p>".$label."</p>";
					$content .= '
								<a title="'._CONTINUECHECKOUT.'" class="more button-exclusive btn btn-default" href="'.$urlfunc->makePretty("?p=cart&action=checkout&do=checkout").'">
									'._CONTINUECHECKOUT.'
								</a>';
					}
					if($is_public_register)
					{
						$tambahan = ($pid != '') ? "&pid=".$pid : "";
						$content .= "<p>"._BECOMEAMEMBER."</p>";
						$content .= '
						<a title="'._REGISTER.'" class="more button-exclusive btn btn-default" href="'.$urlfunc->makePretty("?p=webmember&action=register".$tambahan).'">
							'._REGISTER.'
						</a>';
					}
			$content .= '</div>
					</div>
				</form>
			</div>';
		}
		else
		{
			$content .= "<ul class=\"member_menu\">\r\n";
			$content .= "<li><a href=\"".$urlfunc->makePretty("?p=webmember")."\">"._MEMBERHOME."</a></li>\r\n";
			$content .= "<li><a href=\"".$urlfunc->makePretty("?p=webmember&action=editaccount")."\">"._EDITACCOUNT."</a></li>\r\n";
			$content .= "<li><a href=\"".$urlfunc->makePretty("?p=order&action=history")."\">"._CEKSTATUSORDER."</a></li>\r\n";
			$content .= "<li><a href=\"".$urlfunc->makePretty("?p=topup_pulsa&action=history")."\">"._CEKSTATUSORDERPULSA."</a></li>\r\n";
			$content .= "<li><a href=\"".$urlfunc->makePretty("?p=webmember&action=logout")."\">"._LOGOUT."</a></li>\r\n";
			$content .= "</ul>\r\n";
		}
	}
	else
	{
		$content.=_CONTACTADMINTOREGISTER;
	}
}

if($action=="doregister" and $is_public_register)
{	$kondisi = true;
	// Start Cek Required
	$postuser = $postemail;

	if($postnama=='')
	{	$errormessages .= "<li>"._NAME." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}
	if($postpass1=='')
	{	$errormessages .= "<li>"._PASSWORD." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}

	if($postpass2=='')
	{	$errormessages .= "<li>"._RETYPEPASS." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}
	if($postponsel1=='')
	{	$errormessages .= "<li>"._MOBILE1." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}

	if($postemail=='')
	{	$errormessages .= "<li>"._EMAIL." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}

	if ($postkategori_hipnoterapi == '') {
		$errormessages .= "<li>"._KATEGORI." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}

	/* if(empty($_SESSION['6_letters_code'] ) || empty($_POST['6_letters_code']) || strcasecmp($_SESSION['6_letters_code'], $_POST['6_letters_code']) != 0) {
		if (empty($_POST['6_letters_code'])) {
			$errormessages .= "<li>"._CAPTCHAREQ."</li>\r\n";
			$kondisi = false;
		} else {
			$errormessages .= "<li>"._CAPTCHANOTMATCH."</li>\r\n";
			$kondisi = false;
		}
	} */

	/**
	* Google reCaptcha
	* Aly 2018-05-04
	*/
	if ($_POST['g-recaptcha-response'] == '' && $kondisi == true) {
		$errormessages .= "<li>Something went wrong (Error required Google reCaptcha)</li>";
		$kondisi = false;
	}
	if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] != '') {
		$recaptcha = new \ReCaptcha\ReCaptcha($secret);
		$resp = $recaptcha->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);
		if (!$resp->isSuccess()) {
			$errormessages = "<p>The following error was returned: ";
			foreach ($resp->getErrorCodes() as $code) {
				fiestolog($logtext."\t010\tThe following error was returned:\t$code",'logattack.txt');
				$errormessages .= '<li><kbd>' . $code . '</kbd></li>';
			}
			$kondisi = false;
		}
	}
	// End Cek Required

	if($kondisi==true)
	{	if(!validatemail($postemail))
		{	$errormessages .= "<li>"._INVALIDEMAIL."</li>\r\n";
			$kondisi = false;
		}

		if ($postpass1!=$postpass2)
		{	$errormessages .= "<li>"._PASSWORDANDRETYPENOTSAME."</li>\r\n";
			$kondisi = false;
		}

		if (strlen($postpass1)<6)
		{	$errormessages .= "<li>"._INVALIDPASSWORD."</li>\r\n";
			$kondisi = false;
		}

		$pos = strpos($postpass1, $postuser);
		if ($pos !== false)
		{	$errormessages .= "<li>"._USERINPASSWORD."</li>\r\n";
			$kondisi = false;
		}

		//cek apakah password termasuk kata-kata terlarang
		foreach ($prohibitedpasses as $prohibitedpass)
		{	if (strtolower($postpass1)==$prohibitedpass)
			{	$errormessages .= "<li>"._PROHIBITEDPASS."</li>\r\n";
				$kondisi = false;
			}
		}

		if (!$_SESSION['nomor_anggota']) {
			//cek apakah username sudah terpakai
			/* $sql = "SELECT user_id FROM $tabelwebmember WHERE username='$postuser'";
			$result = $mysql->query($sql);
			if ($mysql->num_rows($result)>0)
			{	$errormessages .= "<li>"._USERNAME." "._ALREADYEXIST."</li>\r\n";
				$kondisi = false;
			} */

			//cek apakah email sudah terpakai
			$sql = "SELECT user_id FROM $tabelwebmember WHERE user_email='$postemail'";
			$result = $mysql->query($sql);
			if ($mysql->num_rows($result)>0)
			{	$errormessages .= "<li>"._EMAIL." "._ALREADYEXIST."</li>\r\n";
				$kondisi = false;
			}
		}
	}


	if($_FILES['pasfoto']['name'] != '') {

		$hasilupload = fiestoupload('pasfoto', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['pasfoto']['name']);
		$extension_pasfoto = $temp[count($temp)-1];
		$basename_pasfoto = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_pasfoto .= $temp[$i];
		}
	}

	if($_FILES['fotoktp']['name'] != '') {

		$hasilupload = fiestoupload('fotoktp', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotoktp']['name']);
		$extension_fotoktp = $temp[count($temp)-1];
		$basename_fotoktp = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotoktp .= $temp[$i];
		}
	}

	if($_FILES['fotosertifikat']['name'] != '') {

		$hasilupload = fiestoupload('fotosertifikat', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotosertifikat']['name']);
		$extension_fotosertifikat = $temp[count($temp)-1];
		$basename_fotosertifikat = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotosertifikat .= $temp[$i];
		}
	}

	if($_FILES['fotolain1']['name'] != '') {

		$hasilupload = fiestoupload('fotolain1', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotolain1']['name']);
		$extension_fotolain1 = $temp[count($temp)-1];
		$basename_fotolain1 = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotolain1 .= $temp[$i];
		}
	}

	if($_FILES['fotolain2']['name'] != '') {

		$hasilupload = fiestoupload('fotolain2', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotolain2']['name']);
		$extension_fotolain2 = $temp[count($temp)-1];
		$basename_fotolain2 = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotolain2 .= $temp[$i];
		}
	}

	if($kondisi)
	{
		
		/* if (in_array($bulan_sekarang, $kelompok_awal)) {
			$expired = date('Y-m-d', $tahun_sekarang);
		} else {
			$expired = date('Y-m-d', $tahun_depan);
		} */
		
		$expired = get_expired();

		$dateofbirth = "$postybegindate-$postmbegindate-$postdbegindate";
		$kode_digit = substr($postponsel1, -3);
		$cookedpass = fiestopass($postpass1);
		if (isset($pid) && $pid != "" & $pid == $_SESSION['nomor_anggota']) {
			$sql="UPDATE $tabelwebmember SET username='$postuser',
				user_password='$cookedpass', fullname='$postnama', level='1',
				address1='$postalamat1', activity='0',
				address2='$postalamat2', city='$postkota', zip='$postkodepos',
				telephone='$posttelepon', cellphone1='$postponsel1',
				cellphone2='$postponsel2', user_email='$postemail',
				user_regdate=NOW(), state='$postprovinsi1', nickname='$postnickname', gender='$postgender', placeofbirth='$postplaceofbirth', dateofbirth='$dateofbirth', noktp='$postnoktp', city2='$postkota2', state2='$postprovinsi2', pendidikan='$posteducation', pendidikan_lain='$postpendidikan_lain', facebook='$postfacebook', instagram='$postinstagram', twitter='$posttwitter', google='$postgoogle', kategori_hipnoterapi='$postkategori_hipnoterapi', no_whatsapp='$postno_whatsapp', kode_digit='$kode_digit', expired='$expired' WHERE user_id='$pid' ";
		} else {
			$sql="INSERT INTO $tabelwebmember SET username='$postuser',
				user_password='$cookedpass', fullname='$postnama', level='1',
				address1='$postalamat1', activity='0',
				address2='$postalamat2', city='$postkota', zip='$postkodepos',
				telephone='$posttelepon', cellphone1='$postponsel1',
				cellphone2='$postponsel2', user_email='$postemail',
				user_regdate=NOW(), state='$postprovinsi1', nickname='$postnickname', gender='$postgender', placeofbirth='$postplaceofbirth', dateofbirth='$dateofbirth', noktp='$postnoktp', city2='$postkota2', state2='$postprovinsi2', pendidikan='$posteducation', pendidikan_lain='$postpendidikan_lain', facebook='$postfacebook', instagram='$postinstagram', twitter='$posttwitter', google='$postgoogle', kategori_hipnoterapi='$postkategori_hipnoterapi', no_whatsapp='$postno_whatsapp', kode_digit='$kode_digit', expired='$expired'";
		}
		$result = $mysql->query($sql);
		if($result)
		{
			$newid = $mysql->insert_id();

			if (isset($pid) && $pid != "" & $pid == $_SESSION['nomor_anggota']) {
				$_SESSION['nomor_anggota'] = $pid;
			} else {
				$_SESSION['nomor_anggota'] = $newid;
			}


			if ($basename_pasfoto != '') {
				$pasfotofilename = "$basename_pasfoto-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['pasfoto']['name'],"$cfg_fullsizepics_path/$pasfotofilename");

				$sql = "UPDATE $tabelwebmember SET pas_foto='$pasfotofilename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotoktp != '') {
				$fotoktpfilename = "$basename_fotoktp-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotoktp']['name'],"$cfg_fullsizepics_path/$fotoktpfilename");

				$sql = "UPDATE $tabelwebmember SET fotoktp='$fotoktpfilename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotosertifikat != '') {
				$fotosertifikatfilename = "$basename_fotosertifikat-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotosertifikat']['name'],"$cfg_fullsizepics_path/$fotosertifikatfilename");

				$sql = "UPDATE $tabelwebmember SET fotosertifikat='$fotosertifikatfilename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotolain1 != '') {
				$fotolain1filename = "$basename_fotolain1-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotolain1']['name'],"$cfg_fullsizepics_path/$fotolain1filename");

				$sql = "UPDATE $tabelwebmember SET filename_lain1='$fotolain1filename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotolain2 != '') {
				$fotolain2filename = "$basename_fotolain2-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotolain2']['name'],"$cfg_fullsizepics_path/$fotolain2filename");

				$sql = "UPDATE $tabelwebmember SET filename_lain2='$fotolain2filename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			// $sql = "UPDATE $tabelwebmember SET pas_foto='$pasfotofilename', fotoktp='$fotoktpfilename', fotosertifikat='$fotosertifikatfilename', filename_lain1='$fotolain1filename', filename_lain2='$fotolain2filename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
			// $result = $mysql->query($sql);

			$url = $urlfunc->makePretty("?p=webmember&action=review");
			header("Location: $url");
			exit;
		}
		else
		{	$content .= "<h1>"._ERROR."</h1>\r\n";
			$content  = "<p>result=".$result."</p>\r\n";
			$content  = "<p>"._DBERROR."</p>\r\n";
			$content .= "<p><a class=\"link_kembali\" href=\"javascript:history.go(-1)\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\r\n";
		}
	}
	else
	{
        $title = "Error";
        $content .= "<ul class=\"error\">\r\n";
        $content .= $errormessages;
        $content .= "</ul>\r\n";
		$content .= "<p><a class=\"link_kembali\" href=\"javascript:history.go(-1)\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\n";
	}
}
elseif($action=="doregister" and !$is_public_register)
{	$content .= "<h1>"._ERROR."</h1>\r\n";
	$content  = "<p>"._NORIGHT."</p>\r\n";
	$content .= "<p><a class=\"link_kembali\" href=\"javascript:history.go(-1)\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\r\n";
}

if($action=="register" and $is_public_register)
{	
	$title = _FORMPENDAFTARANANGGOTAN;
	if (isset($_SESSION['member_uid']))
	{	$content .= "<h1>"._ERROR."</h1>\r\n";
		$content  = "<p>"._ALREADYLOGIN."</p>\r\n";
		$content .= "<p><a class=\"link_kembali\" href=\"".$urlfunc->makePretty("?p=webmember")."\"><i class=\"fa fa-angle-left fa-3\"></i> "._GOTOMAIN."</a></p>\r\r\n";
	}
	else
	{

		if (isset($pid) && $pid != "" && $pid == $_SESSION['nomor_anggota']){
			$sql = "SELECT * FROM webmember WHERE user_id=" . $_SESSION['nomor_anggota'];
			$result = $mysql->query($sql);
			$row = $mysql->fetch_assoc($result);
		}

		$select_provinci1 = '<select name="txtProvinci1"><option value="" >'._PILIH.'</option>'.get_provinci($row['state']).'</select>';
		$select_provinci2 = '<select name="txtProvinci2"><option value="" >'._PILIH.'</option>'.get_provinci($row['state2']).'</select>';
		$select_pendidikan_formal = '<select name="txtEducation" id="txtEducation"><option value="" >'._PILIH.'</option>'.get_educations($row['pendidikan']).'</select>';

		$content .= "<div class=\"form-checkout\">";
		$content .= "
		<div class=\"form-register\">
			<form id=\"signupform\" class=\"form-group row\" method=\"POST\" action=\"".$urlfunc->makePretty("?p=webmember&action=doregister&pid=$pid")."\" enctype=\"multipart/form-data\">
				<div class=\"col-sm-6\">
					<div class=\"panel panel-default\">
						<div class=\"panel-heading\">"._DATADIRI."</div>
						<div class=\"panel-body\">

							<div class=\"form-group\">
								<span class=\"text-right req\">"._NAME." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtNama\" type=\"text\" size=\"40\" maxlength=\"40\" required=\"required\" value=\"".$row['fullname']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PASSWORD." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtPass1\" type=\"password\" size=\"20\" maxlength=\"40\" required=\"required\">
								<div class=\"notice\">"._CORRECTPASS."</div>
								<span class=\"reqsign\"></span>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._RETYPEPASS." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtPass2\" type=\"password\" size=\"20\" maxlength=\"40\" required=\"required\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._NICKNAME." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtNickName\" type=\"text\" size=\"40\" maxlength=\"40\" value=\"".$row['nickname']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._GENDER." <span class=\"reqsign\"></span></span>
								<label><input class=\"form-controlx\" name=\"rdGender\" type=\"radio\" value=\"1\" ".(($row['gender'] == 1) ? "checked" : "")."><span>"._LAKI."</span></label><label><input class=\"form-controlx\" name=\"rdGender\" type=\"radio\" value=\"2\" ".(($row['gender'] == 2) ? "checked" : "")."><span>"._PEREMPUAN."</span></label>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PLACEOFBIRTH." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtPlaceofbirth\" type=\"text\" size=\"40\" maxlength=\"40\" value=\"".$row['placeofbirth']."\">
							</div>
							<!--<div class=\"form-group\">
								<span class=\"text-right req\">"._DATEEOFBIRTH." <span class=\"reqsign\">* 'dd.mm.yyyy'</span></span>
								<input class=\"form-control\" name=\"txtDateofbirth\"id=\"txtDateofbirth\" type=\"text\" size=\"40\" maxlength=\"40\" required=\"required\">
							</div>-->";

						$content .= '
						<div class="form-group">
							<label class="control-label">' . _DATEEOFBIRTH . '</label>
							<div class="control">';

						$hariini = getdate();
						if (isset($pid) && $pid != '' && $pid == $_SESSION['nomor_anggota']) {
							$dob = explode('-', $row['dateofbirth']);

							$content .= "<select class=\"tanggal\" name=\"dbegindate\">";
							$content .= "<option value=\"\" >"._DAY."</option>\n";
							for ($i = 1; $i <= 31; $i++) {
								$content .= "<option value=\"$i\">$i</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"bulan\" name=\"mbegindate\">";
							$content .= "<option value=\"\" >"._MONTH."</option>\n";
							for ($i = 1; $i <= 12; $i++) {
								$j = $i - 1;
								$content .= "<option value=\"$i\">$namabulan[$j]</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"tahun\" name=\"ybegindate\">";
							$content .= "<option value=\"\" >"._YEAR."</option>\n";
							for ($i = $prefix_tahun; $i <= date('Y'); $i++) {
								$content .= "<option value=\"$i\">$i</option>\n";
							}
							$content .= "</select>";

						} else {
							$content .= "<select class=\"tanggal\" name=\"dbegindate\">";
							$content .= "<option value=\"\" >"._DAY."</option>\n";
							for ($i = 1; $i <= 31; $i++) {
								$content .= "<option value=\"$i\">$i</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"bulan\" name=\"mbegindate\">";
							$content .= "<option value=\"\" >"._MONTH."</option>\n";
							for ($i = 1; $i <= 12; $i++) {
								$j = $i - 1;
								$content .= "<option value=\"$i\">$namabulan[$j]</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"tahun\" name=\"ybegindate\">";
							$content .= "<option value=\"\" >"._YEAR."</option>\n";
							for ($i = $prefix_tahun; $i <= date('Y'); $i++) {
								$content .= "<option value=\"$i\">$i</option>\n";
							}
							$content .= "</select>";
						}


						$content .= "</div></div>";

						$content .= "<div class=\"form-group\">
								<span class=\"text-right req\">"._NOKTP." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtNoktp\" type=\"text\" size=\"40\" maxlength=\"40\" value=\"".$row['noktp']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._ADDRESS." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtAlamat1\" type=\"text\" size=\"40\" maxlength=\"50\" value=\"".$row['address1']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._CITY." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtKota\" type=\"text\" size=\"30\" maxlength=\"30\" value=\"".$row['city']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PROVINCI." <span class=\"reqsign\"></span></span>
								$select_provinci1
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._ADDRESSTHISTIME." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtAlamat2\" type=\"text\" size=\"40\" maxlength=\"50\" value=\"".$row['address2']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._CITYTHISTIME." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtKota2\" type=\"text\" size=\"30\" maxlength=\"30\" value=\"".$row['city2']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PROVINCI." <span class=\"reqsign\"></span></span>
								$select_provinci2
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PENDIDIKANFORMAL." <span class=\"reqsign\"></span></span>
								$select_pendidikan_formal
								<input type=\"text\" name=\"education_other\" id=\"education_other\" style=\"display:".(($row['pendidikan_lain'] != "") ? "block" : "none")."\" class=\"form-control\" value=\"".$row['pendidikan_lain']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._TELP1." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtTelepon\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"".$row['telephone']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._MOBILE1."<span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtPonsel1\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"".$row['cellphone1']."\" required>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._NOWHATSAPP."<span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtWhatsapp\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"".$row['no_whatsapp']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._EMAIL." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtEmail\" type=\"text\" size=\"40\" maxlength=\"50\" required=\"required\" value=\"".$row['user_email']."\">
								<div class=\"notice\">"._CORRECTEMAIL."</div>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req title-social-media\">"._SOCIALMEDIA."<span class=\"reqsign\"></span></span>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._FACEBOOK."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtFacebook\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['facebook']."\">
								</div>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._INSTAGRAM."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtInstagram\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['instagram']."\">
								</div>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._TWITTER."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtTwitter\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['twitter']."\">
								</div>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._GOOGLE."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtGoogle\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['google']."\">
								</div>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._KATEGORI."<span class=\"reqsign\">*</span></span>
								<div class=\"form-group\">
								<label><input class=\"form-controlx\" name=\"rdKategori\" type=\"radio\" value=\"1\" required ".(($row['kategori_hipnoterapi'] == 1) ? "checked" : "")."><span>"._HIPNOTERAPIS."</span></label>
									<div>
										<ul>
											<li>Praktisi hipnoterapi berusia minimal 28 tahun pada saat mendaftar.</li>
											<li>Telah mengikuti pendidikan dan pelatihan hipnoterapi dengan durasi minimal 2 hari atau lebih, namun kurang dari 9 hari atau 100 jam tatap muka di kelas.</li>
											<li>Memiliki bukti kesertaaan pendidikan berbentuk sertifikat CHt.</li>
											<li>Iuran tahunan Rp ".number_format($amountcats[1], 0, ',', '.').",-</li>
										</ul>
									</div>
								</div>
								<div class=\"form-group\">
								<label><input class=\"form-controlx\" name=\"rdKategori\" type=\"radio\" value=\"2\" required ".(($row['kategori_hipnoterapi'] == 2) ? "checked" : "")."><span>"._HIPNOTERAPISKLINIS."</span></label>
									<div>
										<ul>
											<li>Praktisi hipnoterapi berusia minimal 28 tahun pada saat mendaftar</li>
											<li>Telah mengikuti pendidikan dan pelatihan hipnoterapi minimal 9 hari atau 100 jam tatap muka di kelas.</li>
											<li>Memiliki bukti kesertaan pelatihan berbentuk sertifikat CHt.</li>
											<li>Iuran tahunan Rp ".number_format($amountcats[2], 0, ',', '.').",-</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class=\"col-sm-6\">
					<div class=\"panel panel-default\">
						<div class=\"panel-heading\">"._UPLOADPERSYARATAN."</div>
						<div class=\"panel-body\">";
						$content .= "<p><strong><small>"._MAXFILEUPLOAD."</small></strong></p>";
						if (isset($pid) && $pid != '' && $pid == $_SESSION['nomor_anggota']) {
							$content .= "
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PASFOTO."<span class=\"reqsign\"></span></span>";
								if ($row['pas_foto'] != '' && file_exists("$cfg_fullsizepics_path/".$row['pas_foto'])) {
									$content .= '
									<div>
										<img src="'.$cfg_fullsizepics_url.'/'.$row['pas_foto'].'" width="200px">
										<a class="foto-item-del" data-img="'.$row['pas_foto'].'" data-id="'.$pid.'" data-state="pas_foto"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
									</div>';
								}
								$content .= "
								<input class=\"form-control\" name=\"pasfoto\" id=\"pasfoto\" type=\"file\" onchange=\"readURL(this)\"\">
								<img id=\"imgresult-pasfoto\" class=\"thumbnail\">
								<small>"._FOTONOCHANGED."</small>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOKTP."<span class=\"reqsign\"></span></span>";
								if ($row['fotoktp'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotoktp'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['fotoktp'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['fotoktp'].'" data-id="'.$pid.'" data-state="fotoktp"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
								}
								$content .= "
								<input class=\"form-control\" name=\"fotoktp\" id=\"fotoktp\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotoktp\" class=\"thumbnail\">
								<small>"._FOTONOCHANGED."</small>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOSERTIFIKAT."<span class=\"reqsign\">*</span></span>";
								if ($row['fotosertifikat'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotosertifikat'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['fotosertifikat'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['fotosertifikat'].'" data-id="'.$pid.'" data-state="fotosertifikat"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
								}
								$content .= "
								<input class=\"form-control\" name=\"fotosertifikat\" id=\"fotosertifikat\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotosertifikat\" class=\"thumbnail\">
								<small>"._FOTONOCHANGED."</small>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN1."<span class=\"reqsign\"></span></span>";
								if ($row['filename_lain1'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain1'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['filename_lain1'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['filename_lain1'].'" data-id="'.$pid.'" data-state="fotolain1"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
									$label_changed = "<small>"._FOTONOCHANGED."</small>";
								}
								$content .= "
								<input class=\"form-control\" name=\"fotolain1\" id=\"fotolain1\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotolain1\" class=\"thumbnail\">
								".$label_changed."
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN2."<span class=\"reqsign\"></span></span>";
								if ($row['filename_lain2'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain2'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['filename_lain2'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['filename_lain2'].'" data-id="'.$pid.'" data-state="fotolain2"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
									$label_changed = "<small>"._FOTONOCHANGED."</small>";
								}
								$content .= "
								<input class=\"form-control\" name=\"fotolain2\" id=\"fotolain2\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotolain2\" class=\"thumbnail\">
								".$label_changed."
							</div>";
						} else {
							$content .= "
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PASFOTO."<span class=\"reqsign\">*</span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"pasfoto\" id=\"pasfoto\" type=\"file\" onchange=\"readURL(this)\" required>
								<input=\"hidden\" name=\"result-pasfoto\" id=\"result-pasfoto\">
								<img id=\"imgresult-pasfoto\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOKTP."<span class=\"reqsign\">*</span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotoktp\" id=\"fotoktp\" type=\"file\" onchange=\"readURL(this)\" required>
								<input=\"hidden\" name=\"result-fotoktp\" id=\"result-fotoktp\">
								<img id=\"imgresult-fotoktp\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOSERTIFIKAT."<span class=\"reqsign\">*</span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotosertifikat\" id=\"fotosertifikat\" type=\"file\" onchange=\"readURL(this)\" required>
								<input=\"hidden\" name=\"result-fotosertifikat\" id=\"result-fotosertifikat\">
								<img id=\"imgresult-fotosertifikat\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN1."<span class=\"reqsign\"></span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotolain1\" id=\"fotolain1\" type=\"file\" onchange=\"readURL(this)\">
								<input=\"hidden\" name=\"result-fotolain1\" id=\"result-fotolain1\">
								<img id=\"imgresult-fotolain1\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN2."<span class=\"reqsign\"></span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotolain2\" id=\"fotolain2\" type=\"file\" onchange=\"readURL(this)\">
								<input=\"hidden\" name=\"result-fotolain2\" id=\"result-fotolain2\">
								<img id=\"imgresult-fotolain2\" class=\"thumbnail-foto-uploaded\">
							</div>";
						}
					$content .= "
						</div>
					</div>

					<!--
					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select pas foto...</span>
						<input id=\"pasfoto\" type=\"file\" name=\"pasfoto\">
					</span>
					<br>
					<br>
					<div id=\"progress\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto ktp...</span>
						<input id=\"fotoktp\" type=\"file\" name=\"fotoktp\">
					</span>
					<br>
					<br>
					<div id=\"progressfotoktp\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto sertifikat...</span>
						<input id=\"fotosertifikat\" type=\"file\" name=\"fotosertifikat\">
					</span>
					<br>
					<br>
					<div id=\"progressfotosertifikat\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto lain 1...</span>
						<input id=\"fotolain1\" type=\"file\" name=\"fotolain1\">
					</span>
					<br>
					<br>
					<div id=\"progressfotolain1\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto lain 2...</span>
						<input id=\"fotolain2\" type=\"file\" name=\"fotolain2\">
					</span>
					<br>
					<br>
					<div id=\"progressfotolain2\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<div id=\"files\" class=\"files\"></div>
					-->
					<div class=\"block-border\">
						<p>Saya menyatakan bahwa informasi dan data yang dikirim ke AHKI adalah yang sebenarnya, dan menerima sepenuhnya keputusan AHKI mengenai penerimaan atau penolakan permohonan saya sebagai anggota AHKI</p>
						<p>Bila diterima sebagai anggota AHKI, saya bersedia mengikuti dan menaati semua aturan, kode etik, norma, serta tata tertib yang berlaku dan mengikat saya sebagai anggota AHKI</p>
						<div class=\"form-group\">
							<label><input type=\"checkbox\" name=\"agree\" value=\"1\" required> Saya setuju*</label>
						</div>
					</div>
				</div>

				<div class=\"col-sm-6\">

					<script type=\"text/javascript\" src=\"https://www.google.com/recaptcha/api.js?hl=$lang\"></script>
					<div class=\"form-group\">
						<div class=\"g-recaptcha\" data-sitekey=\"$siteKey\"></div>
					</div>
				</div>
				<div class=\"col-sm-6\">
					<div class=\"form-group\">
						<input class=\"btn btn-default more\" type=\"submit\" name=\"submit\" value=\""._REVIEW."\">
						<p>"._REQNOTE."</p>
					</div>
				</div>
			</form>
		</div>";
		$content .= "
		</div>";
	}
}
elseif($action=="register" and !$is_public_register)
{	$content .= "<h1>"._ERROR."</h1>\r\n";
	$content  = "<p>"._NORIGHT."</p>\r\n";
	$content .= "<p><a class=\"link_kembali\" href=\"javascript:history.go(-1)\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\r\n";
}

if($action=="login")
{
	//apabila belum login
	if (!isset($_SESSION['member_uid']))
	{

		// if($pid == 'pulsa')
			// $_SESSION['URL_BEFORE_LOGIN'] = "?p=topup_pulsa&action=topup&pid=new";
		// else if($pid == 'gadget')
			// $_SESSION['URL_BEFORE_LOGIN'] = "?p=cart&action=checkout";
		$error_notif = "";
		$is_valid = false;
		if($_POST['check_login'] || $is_forced_login)
		{
			if(!$is_forced_login)
			{
				$postusername= fiestolaundry($_POST['username'],50);
				$postpassword= fiestolaundry($_POST['password'],100);
			}

			if (!$postusername || !$postpassword)
			{
				$error_notif = _USERPASSEMPTY;
			}
			else
			{	$postpassword = fiestopass($postpassword);
				$sql = "SELECT * FROM $tabelwebmember
					WHERE user_email='$postusername' AND user_password='$postpassword'
					AND activity > 1"; 
				$result = $mysql->query($sql);
				if ($mysql->num_rows($result)>0)
				{	$dataUser = $mysql->fetch_assoc($result);
					$_SESSION['member_uname'] = $dataUser["username"];
					$_SESSION['member_uid'] = $dataUser["user_id"];
					$_SESSION['nomor_anggota'] = $dataUser["user_id"];

					$sql = "UPDATE $tabelwebmember SET lastlogin=NOW()
						WHERE user_id='".$dataUser["user_id"]."'";
					$result = $mysql->query($sql);

					if (isset($_SESSION['cart']) && count($_SESSION['cart'] > 0)) {
						header("Location: " . $urlfunc->makePretty("?p=cart&action=checkout"));
					} else {
						// header("Location:".$cfg_app_url);
						header("Location: " . $urlfunc->makePretty("?p=ebook"));
						exit;
					}

					// if(isset($_SESSION['URL_BEFORE_LOGIN']))
					// {
						// $temp_url_before_login=$_SESSION['URL_BEFORE_LOGIN'];
						// // header("Location:".$urlfunc->makePretty($temp_url_before_login));
						// header("Location: ".$temp_url_before_login);
					// }
					// else
					// {
						// header("Location:".$cfg_app_url);
					// }

				}
				else
				{
					$error_notif = _WRONGUSERPASSORNOTACTIVE;
				}
			}
		}

		//login form fayette
		if (isset($_GET['error']) && $_GET['error'] == 1) {
			$msg_error = "<div class=\"msg_error\">"._MEMBERALREADYEXISTS."</div>";
		}

		$content .= '<div class="row"><div id="formlogin" class="col-xs-12 col-sm-6 col-lg-6 col-sm-push-3">
						'.$msg_error.'
						<form class="box" action="'.$urlfunc->makePretty("?p=webmember&action=login").'"
						id="loginform" name="loginform" method="POST">
							<h3 class="page-subheading">'. _CHOSENLOGIN .'</h3>
							<div class="form_content clearfix">
								<div class="form-group">
									<label for="username">'._EMAIL.'</label>
									<input type="text" value="'.$postuserlogin.'" name="username"
									class="is_required validate account_input form-control"/>
								</div>
								<div class="form-group">
									<label for="password">'. _PASSWORD .'</label>
									<input type="password" value="" name="password"
									class="is_required validate account_input form-control"/>
								</div>
								<p class="lost_password form-group">
									<a id="webmember_forgot" href="'.$urlfunc->makePretty("?p=webmember&action=forgotpassword").'">'
									._DOYOUFORGET.
									'</a>';
					if($is_public_register)
					{
						$content .= "&nbsp;&nbsp;|&nbsp;&nbsp;<a id='webmember_register' class='button' href=\"".$urlfunc->makePretty("?p=webmember&action=register")."\">"
									._REGISTER.
									"</a>";
					}
						$content .= '</p>
								<table border="0">
								<tr>
								<td>
								<p class="submit">
									<input type="hidden" name="check_login" value="1"/>
									<button class="more button btn btn-default button-medium" name="submit_login" type="submit">
										<span>
											<i class="fa fa-lock left"></i>
											'._LOGIN.'
										</span>
									</button>
								</td>';
					//apabila proses login gagal maka munculkan notifikasi
					if(!$is_valid)
					{
						$content .=
								'<td>&nbsp;&nbsp;</td>
								<td valign="top"><font color="red">'.$error_notif.'</font></td>';
					}
						$content .=
								'</tr>
								</table>
								</p>
							</div>
						</form>
					</div>';
		// $content .= '
			// <div class="col-xs-12 col-sm-6 col-lg-6">
				// <form class="box">
					// <h3 class="page-subheading">'._BEAMEMBERTOCONTINUE.'</h3>
					// <div class="form_content clearfix">
						// <div class="form-group">';
					// if($is_public_register)
					// {
						// $tambahan = ($pid != '') ? "&pid=".$pid : "";
						// $content .= "<p>"._BECOMEAMEMBER."</p>";
						// $content .= '
						// <a title="'._REGISTER.'" class="more button-exclusive btn btn-default" href="'.$urlfunc->makePretty("?p=webmember&action=register".$tambahan).'">
							// '._REGISTER.'
						// </a>';
					// }
			// $content .= '</div>
					// </div>
				// </form>
			// </div></div>';
			$content .= '</div>';
	}
	//apabila sudah login, redirect ke halaman webmember
	else
	{
		header("Location: " . $urlfunc->makePretty("?p=webmember"));
		exit;
	}
}

if($action=="editaccount")
{	if (!isset($_SESSION['member_uid']))
	{	$content .= "<span class=\"error\">";
		$content .= _NORIGHT."<br/>";
		$content .= "</span>";
	}
	else
	{	$sql = "
			SELECT *
			FROM $tabelwebmember WHERE user_id='$userid'";
		$result = $mysql->query($sql);
		$row = $mysql->fetch_assoc($result);

		$select_provinci1 = '<select name="txtProvinci1"><option value="" >'._PILIH.'</option>'.get_provinci($row['state']).'</select>';
		$select_provinci2 = '<select name="txtProvinci2"><option value="" >'._PILIH.'</option>'.get_provinci($row['state2']).'</select>';
		$select_pendidikan_formal = '<select name="txtEducation" id="txtEducation"><option value="" >'._PILIH.'</option>'.get_educations($row['pendidikan']).'</select>';

		// list($fullname,$company,$address1,$address2,$city,$zip,$telephone,$cellphone1,$cellphone2,$fax,$email) = $mysql->fetch_row($result);

			$content .= "<h2>"._EDITACCOUNT."</h2>";
			$content .= "<script type=\"text/javascript\" src=\"$cfg_app_url/js/global.js\"></script>";
			$content .= "<div class=\"menu-edit-password\"><a class=\"btn btn-default more active\" href=\"javascript:showtab(0)\">"._EDITNONPASSWORD."</a>&nbsp;&nbsp;<a class=\"btn btn-default more\" href=\"javascript:showtab(1)\">"._EDITPASSWORD."</a></div>\r\n";
			$content .= "<div id=\"tab0\" style=\"display:block\">\r\n";
			$content .= '
				<form class="form-horizontal" name="editprofile" id="signupform" action="'.$urlfunc->makePretty("?p=webmember&action=editprofile").'" method="POST" enctype="multipart-formdata">
					<!--<fieldset>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._NAME.' <span class="reqsign">*:</span></span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtNama" type="text" size="40" maxlength="50" value="'.$fullname.'" required/></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._COMPANY.':</span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtPerusahaan" type="text" size="40" maxlength="50" value="'.$company.'" /></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._ADDRESS.' <span class="reqsign">*:</span></span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtAlamat1" type="text" size="40" maxlength="50" value="'.$address1.'" required/></div>
						</div>
						<div class="form-group">
							<div class="col-sm-10 col-md-8 col-md-offset-3 col-sm-offset-2"><input class="form-control" name="txtAlamat2" type="text" size="40" maxlength="50" value="'.$address2.'" /></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._CITY.' <span class="reqsign">*:</span></span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtKota" type="text" size="30" maxlength="30" value="'.$city.'" required/></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._ZIP.':</span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtKodepos" type="text" size="10" maxlength="10" value="'.$zip.'" /></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._TELP1.' <span class="reqsign">*:</span></span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtTelepon" type="text" size="20" maxlength="20" value="'.$telephone.'" /></div>

						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._MOBILE1.' <span class="reqsign">*:</span></span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtPonsel1" type="text" size="20" maxlength="20" value="'.$cellphone1.'" required /></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._MOBILE2.':</span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtPonsel2" type="text" size="20" maxlength="20" value="'.$cellphone2.'" /></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._FAX.':</span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtFax" type="text" size="20" maxlength="20" value="'.$fax.'" /></div>
						</div>
						<div class="form-group">
							<span class="col-sm-2 col-md-3 text-right req">'._EMAIL.' <span class="reqsign">*:</span></span>
							<div class="col-sm-10 col-md-8"><input class="form-control" name="txtEmail" type="text" size="40" maxlength="50" value="'.$email.'" disabled/></div>
						</div>
						<div class="form-group">
							<div class="col-md-10 col-md-offset-3 col-sm-offset-2"><input class="btn btn-default more" type="submit" name="submit" value="'._SAVE.'"><p>'._REQNOTE.'</p></div>
						</div>
					</fieldset>-->';

			$content .= "
			<fieldset id=\"field-profile\" style=\"padding-top:15px\">
				<div class=\"col-sm-6\">
					<div class=\"panel panel-default\">
						<div class=\"panel-heading\">"._DATADIRI."</div>
						<div class=\"panel-body\">

							<div class=\"form-group\">
								<span class=\"text-right req\">"._NAME." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtNama\" type=\"text\" size=\"40\" maxlength=\"40\" required=\"required\" value=\"".$row['fullname']."\">
							</div>
							<!--<div class=\"form-group\">
								<span class=\"text-right req\">"._PASSWORD." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtPass1\" type=\"password\" size=\"20\" maxlength=\"40\" required=\"required\">
								<div class=\"notice\">"._CORRECTPASS."</div>
								<span class=\"reqsign\"></span>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._RETYPEPASS." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtPass2\" type=\"password\" size=\"20\" maxlength=\"40\" required=\"required\">
							</div>-->
							<div class=\"form-group\">
								<span class=\"text-right req\">"._NICKNAME." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtNickName\" type=\"text\" size=\"40\" maxlength=\"40\" value=\"".$row['nickname']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._GENDER." <span class=\"reqsign\"></span></span>
								<label><input class=\"form-controlx\" name=\"rdGender\" type=\"radio\" value=\"1\" ".(($row['gender'] == 1) ? "checked" : "")."><span>"._LAKI."</span></label><label><input class=\"form-controlx\" name=\"rdGender\" type=\"radio\" value=\"2\" ".(($row['gender'] == 2) ? "checked" : "")."><span>"._PEREMPUAN."</span></label>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PLACEOFBIRTH." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtPlaceofbirth\" type=\"text\" size=\"40\" maxlength=\"40\" value=\"".$row['placeofbirth']."\">
							</div>
							<!--<div class=\"form-group\">
								<span class=\"text-right req\">"._DATEEOFBIRTH." <span class=\"reqsign\">* 'dd.mm.yyyy'</span></span>
								<input class=\"form-control\" name=\"txtDateofbirth\"id=\"txtDateofbirth\" type=\"text\" size=\"40\" maxlength=\"40\" required=\"required\">
							</div>-->";

						$content .= '
						<div class="form-group">
							<label class="control-label">' . _DATEEOFBIRTH . '</label>
							<div class="control">';

						$hariini = getdate();
						if (isset($_SESSION['nomor_anggota'])) {
							$dob = explode('-', $row['dateofbirth']);

							$content .= "<select class=\"tanggal\" name=\"dbegindate\">";
							$content .= "<option value=\"\" >"._DAY."</option>\n";
							for ($i = 1; $i <= 31; $i++) {
								$content .= "<option value=\"$i\" ".(($dob[2] == $i) ? "selected=selected" : "").">$i</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"bulan\" name=\"mbegindate\">";
							$content .= "<option value=\"\" >"._MONTH."</option>\n";
							for ($i = 1; $i <= 12; $i++) {
								$j = $i - 1;
								$content .= "<option value=\"$i\" ".(($dob[1] == $i) ? "selected=selected" : "").">$namabulan[$j]</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"tahun\" name=\"ybegindate\">";
							$content .= "<option value=\"\" >"._YEAR."</option>\n";
							for ($i = $prefix_tahun; $i <= date('Y'); $i++) {
								$content .= "<option value=\"$i\" ".(($dob[0] == $i) ? "selected=selected" : "").">$i</option>\n";
							}
							$content .= "</select>";

						} /* else {
							$content .= "<select class=\"tanggal\" name=\"dbegindate\">";
							$content .= "<option value=\"\" >"._DAY."</option>\n";
							for ($i = 1; $i <= 31; $i++) {
								$content .= "<option value=\"$i\">$i</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"bulan\" name=\"mbegindate\">";
							$content .= "<option value=\"\" >"._MONTH."</option>\n";
							for ($i = 1; $i <= 12; $i++) {
								$j = $i - 1;
								$content .= "<option value=\"$i\">$namabulan[$j]</option>\n";
							}
							$content .= "</select>";

							$content .= "<select class=\"tahun\" name=\"ybegindate\">";
							$content .= "<option value=\"\" >"._YEAR."</option>\n";
							for ($i = $prefix_tahun; $i <= date('Y'); $i++) {
								$content .= "<option value=\"$i\">$i</option>\n";
							}
							$content .= "</select>";
						} */


						$content .= "</div></div>";

						$content .= "<div class=\"form-group\">
								<span class=\"text-right req\">"._NOKTP." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtNoktp\" type=\"text\" size=\"40\" maxlength=\"40\" value=\"".$row['noktp']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._ADDRESS." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtAlamat1\" type=\"text\" size=\"40\" maxlength=\"50\" value=\"".$row['address1']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._CITY." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtKota\" type=\"text\" size=\"30\" maxlength=\"30\" value=\"".$row['city']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PROVINCI." <span class=\"reqsign\"></span></span>
								$select_provinci1
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._ADDRESSTHISTIME." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtAlamat2\" type=\"text\" size=\"40\" maxlength=\"50\" value=\"".$row['address2']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._CITYTHISTIME." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtKota2\" type=\"text\" size=\"30\" maxlength=\"30\" value=\"".$row['city2']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PROVINCI." <span class=\"reqsign\"></span></span>
								$select_provinci2
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PENDIDIKANFORMAL." <span class=\"reqsign\"></span></span>
								$select_pendidikan_formal
								<input type=\"text\" name=\"education_other\" id=\"education_other\" style=\"display:".(($row['pendidikan_lain'] != "") ? "block" : "none")."\" class=\"form-control\" value=\"".$row['pendidikan_lain']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._TELP1." <span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtTelepon\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"".$row['telephone']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._MOBILE1."<span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtPonsel1\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"".$row['cellphone1']."\" required>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._NOWHATSAPP."<span class=\"reqsign\"></span></span>
								<input class=\"form-control\" name=\"txtWhatsapp\" type=\"text\" size=\"20\" maxlength=\"20\" value=\"".$row['no_whatsapp']."\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._EMAIL." <span class=\"reqsign\">*</span></span>
								<input class=\"form-control\" name=\"txtEmail\" type=\"text\" size=\"40\" maxlength=\"50\" required=\"required\" value=\"".$row['user_email']."\">
								<div class=\"notice\">"._CORRECTEMAIL."</div>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req title-social-media\">"._SOCIALMEDIA."<span class=\"reqsign\"></span></span>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._FACEBOOK."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtFacebook\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['facebook']."\">
								</div>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._INSTAGRAM."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtInstagram\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['instagram']."\">
								</div>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._TWITTER."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtTwitter\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['twitter']."\">
								</div>
								<div class=\"form-group\">
									<span class=\"text-right req\">"._GOOGLE."<span class=\"reqsign\"></span></span>
									<input class=\"form-control\" name=\"txtGoogle\" type=\"text\" size=\"20\" maxlength=\"255\" value=\"".$row['google']."\">
								</div>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._KATEGORI."<span class=\"reqsign\">*</span></span>
								<div class=\"form-group\">
								<label><input class=\"form-controlx\" name=\"rdKategori\" type=\"radio\" value=\"1\" required ".(($row['kategori_hipnoterapi'] == 1) ? "checked" : "")."><span>"._HIPNOTERAPIS."</span></label>
									<div>
										<ul>
											<li>Praktisi hipnoterapi berusia minimal 28 tahun pada saat mendaftar.</li>
											<li>Telah mengikuti pendidikan dan pelatihan hipnoterapi dengan durasi minimal 2 hari atau lebih, namun kurang dari 9 hari atau 100 jam tatap muka di kelas.</li>
											<li>Memiliki bukti kesertaaan pendidikan berbentuk sertifikat CHt.</li>
											<li>Iuran tahunan Rp ".number_format($amountcats[1], 0, ',', '.').",-</li>
										</ul>
									</div>
								</div>
								<div class=\"form-group\">
								<label><input class=\"form-controlx\" name=\"rdKategori\" type=\"radio\" value=\"2\" required ".(($row['kategori_hipnoterapi'] == 2) ? "checked" : "")."><span>"._HIPNOTERAPISKLINIS."</span></label>
									<div>
										<ul>
											<li>Praktisi hipnoterapi berusia minimal 28 tahun pada saat mendaftar</li>
											<li>Telah mengikuti pendidikan dan pelatihan hipnoterapi minimal 9 hari atau 100 jam tatap muka di kelas.</li>
											<li>Memiliki bukti kesertaan pelatihan berbentuk sertifikat CHt.</li>
											<li>Iuran tahunan Rp ".number_format($amountcats[2], 0, ',', '.').",-</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class=\"col-sm-6\">
					<div class=\"panel panel-default\">
						<div class=\"panel-heading\">"._UPLOADPERSYARATAN."</div>
						<div class=\"panel-body\">";
						$content .= "<p><strong><small>"._MAXFILEUPLOAD."</small></strong></p>";
						if (isset($_SESSION['nomor_anggota'])) {
							$content .= "
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PASFOTO."<span class=\"reqsign\"></span></span>";
								if ($row['pas_foto'] != '' && file_exists("$cfg_fullsizepics_path/".$row['pas_foto'])) {
									$content .= '
									<div>
										<img src="'.$cfg_fullsizepics_url.'/'.$row['pas_foto'].'" width="200px">
										<a class="foto-item-del" data-img="'.$row['pas_foto'].'" data-id="'.$pid.'" data-state="pas_foto"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
									</div>';
								}
								$content .= "
								<input class=\"form-control\" name=\"pasfoto\" id=\"pasfoto\" type=\"file\" onchange=\"readURL(this)\"\">
								<img id=\"imgresult-pasfoto\" class=\"thumbnail\">
								<small>"._FOTONOCHANGED."</small>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOKTP."<span class=\"reqsign\"></span></span>";
								if ($row['fotoktp'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotoktp'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['fotoktp'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['fotoktp'].'" data-id="'.$pid.'" data-state="fotoktp"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
								}
								$content .= "
								<input class=\"form-control\" name=\"fotoktp\" id=\"fotoktp\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotoktp\" class=\"thumbnail\">
								<small>"._FOTONOCHANGED."</small>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOSERTIFIKAT."<span class=\"reqsign\">*</span></span>";
								if ($row['fotosertifikat'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotosertifikat'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['fotosertifikat'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['fotosertifikat'].'" data-id="'.$pid.'" data-state="fotosertifikat"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
								}
								$content .= "
								<input class=\"form-control\" name=\"fotosertifikat\" id=\"fotosertifikat\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotosertifikat\" class=\"thumbnail\">
								<small>"._FOTONOCHANGED."</small>
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN1."<span class=\"reqsign\"></span></span>";
								if ($row['filename_lain1'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain1'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['filename_lain1'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['filename_lain1'].'" data-id="'.$pid.'" data-state="fotolain1"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
									$label_changed = "<small>"._FOTONOCHANGED."</small>";
								}
								$content .= "
								<input class=\"form-control\" name=\"fotolain1\" id=\"fotolain1\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotolain1\" class=\"thumbnail\">
								".$label_changed."
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN2."<span class=\"reqsign\"></span></span>";
								if ($row['filename_lain2'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain2'])) {
									$content .= '
										<div>
											<img src="'.$cfg_fullsizepics_url.'/'.$row['filename_lain2'].'" width="200px">
											<a class="foto-item-del" data-img="'.$row['filename_lain2'].'" data-id="'.$pid.'" data-state="fotolain2"><img width="32" src="'.$cfg_app_url.'/images/garbage.svg"></a>
										</div>';
									$label_changed = "<small>"._FOTONOCHANGED."</small>";
								}
								$content .= "
								<input class=\"form-control\" name=\"fotolain2\" id=\"fotolain2\" type=\"file\" onchange=\"readURL(this)\">
								<img id=\"imgresult-fotolain2\" class=\"thumbnail\">
								".$label_changed."
							</div>";
						} /*else {
							$content .= "
							<div class=\"form-group\">
								<span class=\"text-right req\">"._PASFOTO."<span class=\"reqsign\">*</span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"pasfoto\" id=\"pasfoto\" type=\"file\" onchange=\"readURL(this)\" required>
								<input=\"hidden\" name=\"result-pasfoto\" id=\"result-pasfoto\">
								<img id=\"imgresult-pasfoto\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOKTP."<span class=\"reqsign\">*</span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotoktp\" id=\"fotoktp\" type=\"file\" onchange=\"readURL(this)\" required>
								<input=\"hidden\" name=\"result-fotoktp\" id=\"result-fotoktp\">
								<img id=\"imgresult-fotoktp\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOSERTIFIKAT."<span class=\"reqsign\">*</span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotosertifikat\" id=\"fotosertifikat\" type=\"file\" onchange=\"readURL(this)\" required>
								<input=\"hidden\" name=\"result-fotosertifikat\" id=\"result-fotosertifikat\">
								<img id=\"imgresult-fotosertifikat\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN1."<span class=\"reqsign\"></span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotolain1\" id=\"fotolain1\" type=\"file\" onchange=\"readURL(this)\">
								<input=\"hidden\" name=\"result-fotolain1\" id=\"result-fotolain1\">
								<img id=\"imgresult-fotolain1\" class=\"thumbnail-foto-uploaded\">
							</div>
							<div class=\"form-group\">
								<span class=\"text-right req\">"._FOTOLAIN2."<span class=\"reqsign\"></span><img class=\"check-images\" src=\"$cfg_app_url/images/check.png\"></span>
								<input class=\"form-control\" name=\"fotolain2\" id=\"fotolain2\" type=\"file\" onchange=\"readURL(this)\">
								<input=\"hidden\" name=\"result-fotolain2\" id=\"result-fotolain2\">
								<img id=\"imgresult-fotolain2\" class=\"thumbnail-foto-uploaded\">
							</div>";
						}*/
					$content .= "
						</div>
					</div>

					<!--
					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select pas foto...</span>
						<input id=\"pasfoto\" type=\"file\" name=\"pasfoto\">
					</span>
					<br>
					<br>
					<div id=\"progress\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto ktp...</span>
						<input id=\"fotoktp\" type=\"file\" name=\"fotoktp\">
					</span>
					<br>
					<br>
					<div id=\"progressfotoktp\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto sertifikat...</span>
						<input id=\"fotosertifikat\" type=\"file\" name=\"fotosertifikat\">
					</span>
					<br>
					<br>
					<div id=\"progressfotosertifikat\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto lain 1...</span>
						<input id=\"fotolain1\" type=\"file\" name=\"fotolain1\">
					</span>
					<br>
					<br>
					<div id=\"progressfotolain1\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<span class=\"btn btn-success fileinput-button\">
						<i class=\"glyphicon glyphicon-plus\"></i>
						<span>Select foto lain 2...</span>
						<input id=\"fotolain2\" type=\"file\" name=\"fotolain2\">
					</span>
					<br>
					<br>
					<div id=\"progressfotolain2\" class=\"progress\">
						<div class=\"progress-bar progress-bar-success\"></div>
					</div>

					<div id=\"files\" class=\"files\"></div>

					<div class=\"block-border\">
						<p>Saya menyatakan bahwa informasi dan data yang dikirim ke AHKI adalah yang sebenarnya, dan menerima sepenuhnya keputusan AHKI mengenai penerimaan atau penolakan permohonan saya sebagai anggota AHKI</p>
						<p>Bila diterima sebagai anggota AHKI, saya bersedia mengikuti dan menaati semua aturan, kode etik, norma, serta tata tertib yang berlaku dan mengikat saya sebagai anggota AHKI</p>
						<div class=\"form-group\">
							<label><input type=\"checkbox\" name=\"agree\" value=\"1\" required> Saya setuju*</label>
						</div>
					</div>
					-->

					<div class=\"col-sm-6\">
						<div class=\"form-group\">
							<input class=\"btn btn-default more\" type=\"submit\" name=\"submit\" value=\""._SAVE."\">
							<p>"._REQNOTE."</p>
						</div>
					</div>
				</div>
			</fieldset>
			";
			$content .= '
				</form>';
			$content .= "</div>\r\n";

			$content .= "<div id=\"tab1\" style=\"display:none\">\r\n";
			$content .= '
				<form method="POST" action="'.$urlfunc->makePretty("?p=webmember&action=editpassword").'" name="editpassword" id="editpassword">
				    <table border="0" cellspacing="1" cellpadding="3">
				      <tr>
				        <td>'._ENTEROLDPASSWORD.'</td>
					<td>:</td>
				        <td><input class="form-control" type="password" name="old_pass" size="30" value=""></td>
				      </tr>
				      <tr>
				        <td>'._ENTERNEWPASSWORD1.'</td>
					<td>:</td>
				        <td><input class="form-control" type="password" name="new_pass1" size="30" value=""></td>
				      </tr>
				      <tr>
				        <td>'._ENTERNEWPASSWORD2.'</td>
					<td>:</td>
				        <td><input class="form-control" type="password" name="new_pass2" size="30" value=""></td>
				      </tr>
				      <tr>
				        <td>&nbsp;</td>
				        <td>&nbsp;</td>
				        <td><input type="submit" value="'._SAVE.'" class="btn btn-default more"></td>
				      </tr>
				    </table>
				</form>
			    ';
			$content .= "</div>\r\n";
	}
}

if($action=="editprofile")
{
	$postuser = $postemail;
	$kondisi = true;
	if($postnama=='')
	{	$errormessages .= "<li>"._NAME." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}
	if($postponsel1=='')
	{	$errormessages .= "<li>"._MOBILE1." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}

	if($postemail=='')
	{	$errormessages .= "<li>"._EMAIL." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}

	if ($postkategori_hipnoterapi == '') {
		$errormessages .= "<li>"._KATEGORI." "._ISREQUIRED."</li>\r\n";
		$kondisi = false;
	}
	// End Cek Required

	// if($kondisi==true)
	// {
		// /* if(!validatemail($postemail))
		// {	$content .= "<span class=\"error\">";
			// $content .= _INVALIDEMAIL."<br/>";
			// $content .= "</span>";
			// $kondisi = false;
		// }


		// //cek apakah email sudah terpakai
		// $sql = "SELECT user_id FROM $tabelwebmember
			// WHERE user_email='$postemail' AND user_id!='$userid' ";
		// $result = mysql_query($sql);
		// if (mysql_num_rows($result)>0)
		// {	$content .= "<span class=\"error\">";
			// $content .= "<span class=\"fieldname\">"._EMAIL."</span> "._ALREADYEXIST."<br/>";
			// $content .= "</span>";
			// $kondisi = false;
		// } */
	// }

	if($_FILES['pasfoto']['name'] != '') {
		$hasilupload = fiestoupload('pasfoto', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['pasfoto']['name']);
		$extension_pasfoto = $temp[count($temp)-1];
		$basename_pasfoto = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_pasfoto .= $temp[$i];
		}
	}

	if($_FILES['fotoktp']['name'] != '') {

		$hasilupload = fiestoupload('fotoktp', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotoktp']['name']);
		$extension_fotoktp = $temp[count($temp)-1];
		$basename_fotoktp = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotoktp .= $temp[$i];
		}
	}

	if($_FILES['fotosertifikat']['name'] != '') {

		$hasilupload = fiestoupload('fotosertifikat', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotosertifikat']['name']);
		$extension_fotosertifikat = $temp[count($temp)-1];
		$basename_fotosertifikat = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotosertifikat .= $temp[$i];
		}
	}

	if($_FILES['fotolain1']['name'] != '') {

		$hasilupload = fiestoupload('fotolain1', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotolain1']['name']);
		$extension_fotolain1 = $temp[count($temp)-1];
		$basename_fotolain1 = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotolain1 .= $temp[$i];
		}
	}

	if($_FILES['fotolain2']['name'] != '') {

		$hasilupload = fiestoupload('fotolain2', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
		if($hasilupload != _SUCCESS) {
			$errormessages .= "<li>".$hasilupload."</li>\r\n";
			$kondisi = false;
		}

		//ambil informasi basename dan extension
		$temp = explode(".",$_FILES['fotolain2']['name']);
		$extension_fotolain2 = $temp[count($temp)-1];
		$basename_fotolain2 = '';
		for ($i=0;$i<count($temp)-1;$i++) {
			$basename_fotolain2 .= $temp[$i];
		}
	}

	if($kondisi==true)
	{
		// $sql = "UPDATE $tabelwebmember SET fullname='$postnama',
			// company='$postperusahaan', address1='$postalamat1', address2='$postalamat2',
			// city='$postkota', zip='$postkodepos', telephone='$posttelepon',
			// cellphone1='$postponsel1', cellphone2='$postponsel2', fax='$postfax' WHERE user_id='$userid'";
		$pid = $_SESSION['member_uid'];
		$dateofbirth = "$postybegindate-$postmbegindate-$postdbegindate";
		$kode_digit = substr($postponsel1, -3);
		$sql="UPDATE $tabelwebmember SET username='$postuser',
			fullname='$postnama', level='1',
			address1='$postalamat1',
			address2='$postalamat2', city='$postkota', zip='$postkodepos',
			telephone='$posttelepon', cellphone1='$postponsel1',
			cellphone2='$postponsel2', user_email='$postemail',
			state='$postprovinsi1', nickname='$postnickname', gender='$postgender', placeofbirth='$postplaceofbirth', dateofbirth='$dateofbirth', noktp='$postnoktp', city2='$postkota2', state2='$postprovinsi2', pendidikan='$posteducation', pendidikan_lain='$postpendidikan_lain', facebook='$postfacebook', instagram='$postinstagram', twitter='$posttwitter', google='$postgoogle', kategori_hipnoterapi='$postkategori_hipnoterapi', no_whatsapp='$postno_whatsapp', kode_digit='$kode_digit' WHERE user_id='$pid' ";
		$result = $mysql->query($sql);
		if($result)
		{

			if ($basename_pasfoto != '') {
				$pasfotofilename = "$basename_pasfoto-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['pasfoto']['name'],"$cfg_fullsizepics_path/$pasfotofilename");

				$sql = "UPDATE $tabelwebmember SET pas_foto='$pasfotofilename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotoktp != '') {
				$fotoktpfilename = "$basename_fotoktp-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotoktp']['name'],"$cfg_fullsizepics_path/$fotoktpfilename");

				$sql = "UPDATE $tabelwebmember SET fotoktp='$fotoktpfilename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotosertifikat != '') {
				$fotosertifikatfilename = "$basename_fotosertifikat-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotosertifikat']['name'],"$cfg_fullsizepics_path/$fotosertifikatfilename");

				$sql = "UPDATE $tabelwebmember SET fotosertifikat='$fotosertifikatfilename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotolain1 != '') {
				$fotolain1filename = "$basename_fotolain1-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotolain1']['name'],"$cfg_fullsizepics_path/$fotolain1filename");

				$sql = "UPDATE $tabelwebmember SET filename_lain1='$fotolain1filename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}
			if ($basename_fotolain2 != '') {
				$fotolain2filename = "$basename_fotolain2-{$_SESSION['nomor_anggota']}.$extension_pasfoto";
				rename("$cfg_fullsizepics_path/".$_FILES['fotolain2']['name'],"$cfg_fullsizepics_path/$fotolain2filename");

				$sql = "UPDATE $tabelwebmember SET filename_lain2='$fotolain2filename' WHERE user_id='{$_SESSION['nomor_anggota']}'";
				$result = $mysql->query($sql);
			}

			//$content .= "<h1>"._TITLEACCTCHANGED."</h1>\r\n";
			$content .= "<p>"._ACCTCHANGED."</p>\r\n";
			$content .= "<p><a class=\"link_kembali\" href=\"".$urlfunc->makePretty("?p=webmember")."\"><i class=\"fa fa-angle-left fa-3\"></i> "._GOTOMAIN."</a></p>\r\n";
		}
		else
		{	//$content .= "<h1>"._ERROR."</h1>\r\n";
			$content  = "<p>"._DBERROR."</p>\r\n";
			$content .= "<p><a class=\"link_kembali\" href=\"javascript:history.go(-1)\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\r\n";
		}
	}
	else
	{	$content .= "<p><a class=\"link_kembali\" href=\"javascript:history.go(-1)\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\r\n";
	}
}

if($action=="editpassword")
{	$sql = "SELECT user_id, user_password, username
			FROM $tabelwebmember WHERE user_id='$userid'";
	$result = $mysql->query($sql);
	list($cid,$cpassword,$cusername) = $mysql->fetch_row($result);
	$kondisi = true;

	$old_pass = trim($_POST['old_pass']);
	$new_pass1 = trim($_POST['new_pass1']);
	$new_pass2 = trim($_POST['new_pass2']);

	if ($old_pass == "" || $new_pass1 == "" || $new_pass2 == "")
	{	$content .= "<span class=\"error\">";
		$content .= _REQUIREDFIELDS."<br/>";
		$content .= "</span>";
		$kondisi = false;
	}

	if (fiestopass($old_pass) != $cpassword)
	{	$content .= "<span class=\"error\">";
		$content .= _WRONGOLDPASSWORD."<br/>";
		$content .= "</span>";
		$kondisi = false;
	}

	if ($new_pass1 != $new_pass2)
	{	$content .= "<span class=\"error\">";
		$content .= _WRONGNEWPASSWORD."<br/>";
		$content .= "</span>";
		$kondisi = false;
	}

	//cek apakah password >=6
	if (strlen($new_pass1)<6)
	{	$content .= "<span class=\"error\">";
		$content .= _INVALIDPASSWORD."<br/>";
		$content .= "</span>";
		$kondisi = false;
	}

	//cek apakah password mengandung username
	$pos = strpos($new_pass1, $cusername);
	if ($pos !== false)
	{	$content .= "<span class=\"error\">";
		$content .= _USERINPASSWORD."<br/>";
		$content .= "</span>";
		$kondisi = false;
	}

	//cek apakah password termasuk kata-kata terlarang
	foreach ($prohibitedpasses as $prohibitedpass)
	{	if (strtolower($new_pass1)==$prohibitedpass)
		{	$content .= "<span class=\"error\">";
			$content .= _PROHIBITEDPASS."<br/>";
			$content .= "</span>";
			$kondisi = false;
		}
	}

	if($kondisi)
	{	$new_pass1 = fiestopass($new_pass1);
		$sql = "UPDATE $tabelwebmember SET user_password='$new_pass1'
			WHERE user_id='$userid'";
		$result = $mysql->query($sql);
		if ($result)
		{	$content .= "<h1>"._TITLEPASSCHANGED."</h1>\r\n";
			$content  = "<p>"._PASSCHANGED."</p>\r\n";
			$content .= "<p><a class=\"link_kembali\" href=\"".$urlfunc->makePretty("?p=webmember")."\"><i class=\"fa fa-angle-left fa-3\"></i> "._GOTOMAIN."</a></p>\r\n";
		}
		else
		{	$content .= "<h1>"._ERROR."</h1>\r\n";
			$content  = "<p>"._DBERROR."</p>\r\n";
			$content .= "<p><a class=\"link_kembali\" href=\"javascript:history.go(-1)\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\r\n";
		}
	}
	else
	{	$content .= "<p><a href=\"javascript:history.go(-1)\">"._BACK."</a></p>\r\r\n";
	}
}

if  ($action=='forgotpassword') {
	$content .= "<p>"._FORGOTPASSCONTENT."</p>";
	$content .= '
		<form action="'.$urlfunc->makePretty("?p=webmember&action=sendpassword").'" name="forgotpass" id="forgotpass" method="POST">
		    <table border="0" cellspacing="1" cellpadding="3">
		      <tr>
		        <td align="right">'._EMAIL.':</td>
		        <td><input class="form-control" type="text" name="txtEmail" size="50" value=""></td>
		      </tr>
		      <tr>
		        <td align="right">&nbsp;</td>
		        <td><input class="more" type="submit" value="'._SUBMIT.'"></td>
		      </tr>
		    </table>
		</form>';
}
if ($action=='sendpassword') {
	$mailpass=$_POST['txtEmail'];
	if (validatemail($mailpass)) {
		$sql="SELECT user_email FROM $tabelwebmember WHERE user_email='$mailpass'";
		$result = $mysql->query($sql);
		if ($mysql->num_rows($result) > 0) {
			list($user_email) = $mysql->fetch_row($result);
			$newpass = rand(0,9).time().rand(0,9);
			$message  = _WEHAVERESET."\r\n";
			$message .= _EMAIL.": $user_email\r\n";
			$message .= _PASSWORD.": $newpass\r\n\r\n";
			$message .= _YOUCANCHANGE;
			$newpass = fiestopass($newpass);
			$sql = "UPDATE $tabelwebmember SET user_password='$newpass' WHERE user_email='$user_email'";
			$mysql->query($sql);
			if (fiestophpmailer($mailpass, _FORGOTPASSSUBJ, $message, $smtpuser, $smtpuser)) {
				$content = "<p>"._MAILSENT."</p><p><a class=\"link_kembali\" href=\"$cfg_app_url\"><i class=\"fa fa-angle-left fa-3\"></i> "._GOTOMAIN."</a></p>\r\n";
			} else {
				$content = "<p>"._CANTSENDMAIL."</p><p><a class=\"link_kembali\" href=\"javascript:history.back()\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\n";
			}
		} else {
			$content = "<p>"._CANTFINDMAIL."</p><p><a class=\"link_kembali\" href=\"javascript:history.back()\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\n";
		}
	} else {
		$content = "<p>"._INVALIDEMAIL."</p><p><a class=\"link_kembali\" href=\"javascript:history.back()\"><i class=\"fa fa-angle-left fa-3\"></i> "._BACK."</a></p>\r\n";
	}
}


if($action=="logout")
{	unset($_SESSION['member_uname']);
	unset($_SESSION['member_uid']);
	unset($_SESSION['nomor_anggota']);
	header("Location:$cfg_app_url");
}

if ($action == 'review') {

	$title = _REVIEWFORMPENDAFTARANANGGOTAN;

	$sql = "SELECT * FROM webmember WHERE user_id=" . $_SESSION['nomor_anggota'];
	$result = $mysql->query($sql);
	$row = $mysql->fetch_assoc($result);

	$select_provinci1 = '<select name="txtProvinci1" required disabled>'.get_provinci($row['state']).'</select>';
	$select_provinci2 = '<select name="txtProvinci2" required disabled>'.get_provinci($row['state2']).'</select>';
	$select_pendidikan_formal = '<select name="txtEducation" disabled>'.get_educations($row['pendidikan']).'</select>';

	$content .= "<div class=\"form-checkout\">";
	$content .= "
		<div class=\"form-register\">
			<form id=\"signupform\" class=\"form-group row\" method=\"POST\" action=\"".$urlfunc->makePretty("?p=webmember&action=success")."\" enctype=\"multipart-formdata\">

				<div class=\"col-sm-6\">
					<div class=\"panel panel-default\">
						<div class=\"panel-heading\">"._DATADIRI."</div>
						<div class=\"panel-body\">
							<table class=\"table-confirm\">
								<tbody>
									<tr class=\"first noborder\">
										<td class=\"first-child\">"._NAME."</td>
										<td>".$row['fullname']."</td>
									</tr>
									<tr class=\"noborder\">
										<td class=\"first-child\">"._NICKNAME."</td>
										<td>".$row['nickname']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._GENDER."</td>
										<td>".($row['gender'] == 1 ? _LAKI : _PEREMPUAN)."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._PLACEOFBIRTH."</td>
										<td>".$row['placeofbirth']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._DATEEOFBIRTH."</td>
										<td>".(($row['dateofbirth'] != '0000-00-00') ? tglformat($row['dateofbirth']) : "")."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._NOKTP."</td>
										<td>".$row['noktp']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._ADDRESS."</td>
										<td>".$row['address1']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._CITY."</td>
										<td>".$row['city']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._PROVINCI."</td>
										<td>".get_provinci_name($row['state'])."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._ADDRESSTHISTIME."</td>
										<td>".$row['address2']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._CITY."</td>
										<td>".$row['city2']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._PROVINCI."</td>
										<td>".get_provinci_name($row['state2'])."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._PENDIDIKANFORMAL."</td>
										<td>".$educations[$row['pendidikan']]."".(($row['pendidikan_lain'] != "") ? ": " . $row['pendidikan_lain'] : "")."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._TELP1."</td>
										<td>".$row['telephone']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._MOBILE1."</td>
										<td>".$row['cellphone1']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._NOWHATSAPP."</td>
										<td>".$row['no_whatsapp']."</td>
									</tr>
									<tr>
										<td class=\"first-child\">"._EMAIL."</td>
										<td>".$row['user_email']."</td>
									</tr>";
							if ($row['facebook'] != "") {
								$content .= "
									<tr>
										<td class=\"first-child\">"._FACEBOOK."</td>
										<td><a href=\"".$row['facebook']."\">".$row['facebook']."</a></td>
									</tr>
								";
							}

							if ($row['instagram'] != "") {
								$content .= "
									<tr>
										<td class=\"first-child\">"._INSTAGRAM."</td>
										<td><a href=\"".$row['instagram']."\">".$row['instagram']."</a></td>
									</tr>
								";
							}

							if ($row['twitter'] != "") {
								$content .= "
									<tr>
										<td class=\"first-child\">"._TWITTER."</td>
										<td><a href=\"".$row['twitter']."\">".$row['twitter']."</a></td>
									</tr>
								";
							}

							if ($row['google'] != "") {
								$content .= "
									<tr>
										<td class=\"first-child\">"._GOOGLE."</td>
										<td><a href=\"".$row['google']."\">".$row['google']."</a></td>
									</tr>
								";
							}

							$content .= "
									<tr>
										<td class=\"first-child\">"._KATEGORI."</td>
										<td>".($row['kategori_hipnoterapi'] == 1 ? _HIPNOTERAPIS : _HIPNOTERAPISKLINIS)."</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>";

				$content .= '
				<div class="col-sm-6">
					<div class="panel panel-default">
						<div class="panel-heading">'._UPLOADPERSYARATAN.'</div>
						<div class="panel-body">';

				if ($row['pas_foto'] != '' && file_exists("$cfg_fullsizepics_path/".$row['pas_foto'])) {
					$pas_foto = $cfg_fullsizepics_url . '/' . $row['pas_foto'];
					$content .= '
					<div class="col-xs-12">
						<a class="thumbnail">
							<img src="'.$pas_foto.'" class="img-responsive">
						</a>
						<p class="text-center"><small>Pas Foto</small> <img src="'.$cfg_app_url.'/images/check.png"></p>
					</div>
					';
				}
				if ($row['fotoktp'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotoktp'])) {
					$fotoktp = $cfg_fullsizepics_url . '/' . $row['fotoktp'];
					$content .= '
					<div class="col-xs-12">
						<a class="thumbnail">
							<img src="'.$fotoktp.'" class="img-responsive">
						</a>
						<p class="text-center"><small>Foto KTP</small> <img src="'.$cfg_app_url.'/images/check.png"></p>
					</div>
					';
				}
				if ($row['fotosertifikat'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotosertifikat'])) {
					$fotosertifikat = $cfg_fullsizepics_url . '/' . $row['fotosertifikat'];
					$content .= '
					<div class="col-xs-12">
						<a class="thumbnail">
							<img src="'.$fotosertifikat.'" class="img-responsive">
						</a>
						<p class="text-center"><small>Foto Sertifikat</small> <img src="'.$cfg_app_url.'/images/check.png"></p>
					</div>
					';
				}
				if ($row['filename_lain1'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain1'])) {
					$filename_lain1 = $cfg_fullsizepics_url . '/' . $row['filename_lain1'];
					$content .= '
					<div class="col-xs-12">
						<a class="thumbnail">
							<img src="'.$filename_lain1.'" class="img-responsive">
						</a>
						<p class="text-center"><small>Foto lain 1</small> <img src="'.$cfg_app_url.'/images/check.png"></p>
					</div>
					';
				}
				if ($row['filename_lain2'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain2'])) {
					$filename_lain2 = $cfg_fullsizepics_url . '/' . $row['filename_lain2'];
					$content .= '
					<div class="col-xs-12">
						<a class="thumbnail">
							<img src="'.$filename_lain2.'" class="img-responsive">
						</a>
						<p class="text-center"><small>Foto lain 2</small> <img src="'.$cfg_app_url.'/images/check.png"></p>
					</div>
					';
				}
				$content .= "
					</div>
				</div>

					<div class=\"block-border\">
						<p>Bila mau memperbaiki data, klik <a class=\"btn btn-default more tombol-back\" href=\"".$urlfunc->makePretty("?p=webmember&action=register&pid=" . $_SESSION['nomor_anggota'])."\">"._BACK."</a><br><br>
						Bila data data sudah benar semua, klik <input class=\"btn btn-default more\" type=\"submit\" name=\"submit\" value=\""._SUBMIT."\"></p>
					</div>
				";
				$content .= "</div>";

				$content .= "
				<div class=\"col-sm-6\">
				</div>

				<!--<div class=\"col-sm-6\">

					<script type=\"text/javascript\" src=\"https://www.google.com/recaptcha/api.js?hl=$lang\"></script>
					<div class=\"form-group\">
						<div class=\"g-recaptcha\" data-sitekey=\"$siteKey\"></div>
					</div>
				</div>-->
				<!--<div class=\"col-sm-6\">
					<div class=\"form-group\">
						<input class=\"btn btn-default more\" type=\"submit\" name=\"submit\" value=\""._SUBMIT."\"><br>
						<a class=\"btn btn-default more tombol-back\" href=\"".$urlfunc->makePretty("?p=webmember&action=register&pid=" . $_SESSION['nomor_anggota'])."\">"._BACK."</a>
					</div>
				</div>
				<div class=\"col-sm-6\">
					<div class=\"form-group\">
						<span class=\"text-right req\">"._NOTE."<span class=\"reqsign\"></span></span>
						<ul>
							<li>Kami akan mengirimkan pemberitahuan melalui email setelah menerima aplikasi permohonan keanggotaan Anda.</li>
							<li>Keputusan penerimaan atau penolakan permohonan keanggotaan AHKI yang Anda ajukan disampaikan melalui email.</li>
							<li>Apabila permohonan keanggotaan Anda dikabulkan, maka akan ada pemberitahuan lanjutan untuk melakukan transfer iuran tahunan.</li>
						</ul>
					</div>
				</div>-->
			</form>
		</div>";
		$content .= "
		</div>";
}

if ($action == 'success') {

	$sql = "SELECT * FROM webmember WHERE user_id=" . $_SESSION['nomor_anggota'];
	$result = $mysql->query($sql);
	$title = "";
	if ($mysql->num_rows($result) > 0) {
		$row = $mysql->fetch_assoc($result);

		$htmlemail = $email_anggota_baru;

		$htmlmsg = "
		<html>
		<head>
			<style>
				.noborder {border-top:0px solid !important}
				img{max-width:100%;height:auto}
				table.table-confirm tr {
					width: 50%;
					font-weight: 100;
					float: left;
					margin-bottom: 9px;
					border-top: 1px solid #eee;
					padding-top: 10px;
					color:#999
				}
				table.table-confirm,table.table-confirm tbody,table.table-confirm tr,table.table-confirm td {
					display: block;
				}
				table.table-confirm {overflow:hidden}
				table.table-confirm td:first-child,.first-child {
					font-weight: bold;
					padding-bottom: 0;
					color:#000;
				}
				table.table-confirm tr:nth-child(2n+1) {
					clear: both;
				}
.thumbnail{display:block}
.col-xs-6.col-md-3 {
    margin-bottom: 20px;
    text-align: center;
	width:100%;
	padding:0 15px;
	float:left;
	-webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.panel {
    margin-bottom: 20px;
    background-color: #fff;
    border: 1px solid transparent;
    border-radius: 4px;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel-default {
    border-color: #ddd;
}
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
}
.panel-default>.panel-heading {
    color: #333;
    background-color: #f5f5f5;
    border-color: #ddd;
}
.panel-body {
    padding: 15px;
	overflow:hidden;
}
.row {
    margin-right: -15px;
    margin-left: -15px;
}
.col-md-6 {
    width: 50%;
	padding:0 15px;
	float:left;
	-webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.block-border {
    background: #f5f5f5;
    padding: 20px 30px 1px;
    margin-bottom: 20px;
    border: 1px solid #ccc;
}
p {
    margin: 0 0 20px;
}
@media(max-width:767px){
	.col-md-6{width:100%;}
}
			</style>
		</head>
		<body>
		<div class=\"row\">
			<div class=\"col-md-12\">
				<div class=\"thanks block-border\">
					<p>Terima kasih, permohonan Anda sudah dikirim ke AHKI.</p>
					<p>Kami akan mengirimkan pemberitahuan melalui email setelah menerima aplikasi permohonan keanggotaan Anda.</p>
					<p>Keputusan penerimaan atau penolakan permohonan keanggotaan AHKI yang Anda ajukan disampaikan melalui email.</p>
					<p>Apabila permohonan keanggotaan Anda dikabulkan, maka akan ada pemberitahuan lanjutan untuk melakukan transfer iuran tahunan.</p>
				</div>
				<!--<div class=\"panel panel-default\">
					<div class=\"panel-heading\">"._DATADIRI."</div>
					<div class=\"panel-body\">
						<table class=\"table-confirm\">
							<tbody>
								<tr class=\"first noborder\">
									<td class=\"first-child\">"._NAME."</td>
									<td>".$row['fullname']."</td>
								</tr>
								<tr class=\"noborder\">
									<td class=\"first-child\">"._NICKNAME."</td>
									<td>".$row['nickname']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._GENDER."</td>
									<td>".($row['gender'] == 1 ? _LAKI : _PEREMPUAN)."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._PLACEOFBIRTH."</td>
									<td>".$row['placeofbirth']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._DATEEOFBIRTH."</td>
									<td>".tglformat($row['dateofbirth'])."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._NOKTP."</td>
									<td>".$row['noktp']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._ADDRESS."</td>
									<td>".$row['address1']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._CITY."</td>
									<td>".$row['city']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._PROVINCI."</td>
									<td>".get_provinci_name($row['state'])."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._ADDRESSTHISTIME."</td>
									<td>".$row['address2']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._CITY."</td>
									<td>".$row['city2']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._PROVINCI."</td>
									<td>".get_provinci_name($row['state2'])."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._PENDIDIKANFORMAL."</td>
									<td>".$educations[$row['pendidikan']]."".(($row['pendidikan_lain'] != "") ? ": " . $row['pendidikan_lain'] : "")."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._TELP1."</td>
									<td>".$row['telephone']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._MOBILE1."</td>
									<td>".$row['cellphone1']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._NOWHATSAPP."</td>
									<td>".$row['no_whatsapp']."</td>
								</tr>
								<tr>
									<td class=\"first-child\">"._EMAIL."</td>
									<td>".$row['user_email']."</td>
								</tr>";
						if ($row['facebook'] != "") {
							$htmlmsg .= "
								<tr>
									<td class=\"first-child\">"._FACEBOOK."</td>
									<td><a href=\"".$row['facebook']."\">".$row['facebook']."</a></td>
								</tr>
							";
						}

						if ($row['instagram'] != "") {
							$htmlmsg .= "
								<tr>
									<td class=\"first-child\">"._INSTAGRAM."</td>
									<td><a href=\"".$row['instagram']."\">".$row['instagram']."</a></td>
								</tr>
							";
						}

						if ($row['twitter'] != "") {
							$htmlmsg .= "
								<tr>
									<td class=\"first-child\">"._TWITTER."</td>
									<td><a href=\"".$row['twitter']."\">".$row['twitter']."</a></td>
								</tr>
							";
						}

						if ($row['google'] != "") {
							$htmlmsg .= "
								<tr>
									<td class=\"first-child\">"._GOOGLE."</td>
									<td><a href=\"".$row['google']."\">".$row['google']."</a></td>
								</tr>
							";
						}

						$htmlmsg .= "
								<tr>
									<td class=\"first-child\">"._KATEGORI."</td>
									<td>".($row['kategori_hipnoterapi'] == 1 ? _HIPNOTERAPIS : _HIPNOTERAPISKLINIS)."</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>-->
			</div>
		";

		$attachments = array();
		$htmlmsg .= "
			<!--<div id=\"signupform\"  class=\"col-md-6\">
				<div class=\"panel panel-default\">
					<div class=\"panel-heading\">"._UPLOADPERSYARATAN."</div>
					<div class=\"panel-body\">";
					if ($row['pas_foto'] != '' && file_exists("$cfg_fullsizepics_path/".$row['pas_foto'])) {
						$pas_foto = $cfg_fullsizepics_url . '/' . $row['pas_foto'];
						$attachments[] = "$cfg_fullsizepics_path/".$row['pas_foto'];
						$htmlmsg .= '
						<div class="col-xs-6 col-md-3">
							<a class="thumbnail">
								<img src="'.$pas_foto.'" class="img-responsive">
							</a>
							<small>Pas Foto</small>
						</div>
						';
					}
					if ($row['fotoktp'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotoktp'])) {
						$fotoktp = $cfg_fullsizepics_url . '/' . $row['fotoktp'];
						$attachments[] = "$cfg_fullsizepics_path/".$row['fotoktp'];
						$htmlmsg .= '
						<div class="col-xs-6 col-md-3">
							<a class="thumbnail">
								<img src="'.$fotoktp.'" class="img-responsive">
							</a>
							<small>Foto KTP</small>
						</div>
						';
					}
					if ($row['fotosertifikat'] != '' && file_exists("$cfg_fullsizepics_path/".$row['fotosertifikat'])) {
						$fotosertifikat = $cfg_fullsizepics_url . '/' . $row['fotosertifikat'];
						$attachments[] = "$cfg_fullsizepics_path/".$row['fotosertifikat'];
						$htmlmsg .= '
						<div class="col-xs-6 col-md-3">
							<a class="thumbnail">
								<img src="'.$fotosertifikat.'" class="img-responsive">
							</a>
							<small>Foto Sertifikat</small>
						</div>
						';
					}
					if ($row['filename_lain1'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain1'])) {
						$filename_lain1 = $cfg_fullsizepics_url . '/' . $row['filename_lain1'];
						$attachments[] = "$cfg_fullsizepics_path/".$row['filename_lain1'];
						$htmlmsg .= '
						<div class="col-xs-6 col-md-3">
							<a class="thumbnail">
								<img src="'.$filename_lain1.'" class="img-responsive">
							</a>
							<small>Foto lain 1</small>
						</div>
						';
					}
					if ($row['filename_lain2'] != '' && file_exists("$cfg_fullsizepics_path/".$row['filename_lain2'])) {
						$filename_lain2 = $cfg_fullsizepics_url . '/' . $row['filename_lain2'];
						$attachments[] = "$cfg_fullsizepics_path/".$row['filename_lain2'];
						$htmlmsg .= '
						<div class="col-xs-6 col-md-3">
							<a class="thumbnail">
								<img src="'.$filename_lain2.'" class="img-responsive">
							</a>
							<small>Foto lain 2</small>
						</div>
						';
					}
					$htmlmsg.= "
					</div>
				</div>
			</div>-->
		</div>
		</body>
		</html>
		";


		$content .= $htmlmsg;
		$content .= "<div class=\"col-sm-12 text-center\"><a class=\"btn btn-default more tombol-back\" href=\"$cfg_app_url\">"._BACK."</a></div>";

		if ($row['gender'] != 0) {
			$gender_prefix = ($row['gender'] == 1) ? _BAPAK : _IBU;
		}
		$html = array(
			'/#namaanggota/' => $row['fullname'],
			'/#gender/' => $gender_prefix
		);
		$email_anggota_baru = preg_replace(array_keys($html), array_values($html), $email_anggota_baru);

		// $htmlmsg = $email_anggota_baru.$htmlmsg;
		$htmlmsg = $email_anggota_baru;

		$attachment = join(',',$attachments);

		$postnama = $row['fullname'];
		$postemail = $row['user_email'];
		$postuser = $row['nickname'];

		include("$cfg_app_path/modul/$namamodul/lang/$lang/mailactvcode.php");

		// $mailer = fiestophpmailer($postemail, _FORMPENDAFTARANANGGOTAN, $htmlmsg, $smtpuser, $sendername, $smtpuser, $htmlmsg, $attachment);
		$mailer = fiestophpmailer($postemail, _FORMPENDAFTARANANGGOTAN, $htmlmsg, $smtpuser, $sendername, $smtpuser, $htmlmsg);
		if ($mailer) {

			fiestophpmailer($umbalemail, _FORMPENDAFTARANANGGOTAN, $message_admin, $smtpuser, $postemail, $smtpuser);
		}
	}

}

if ($action == 'confirm_payment') {
	$title = _FORMKONFIRMASI;

	$pid = base64_decode($pid);

	$sql = "SELECT * FROM webmember WHERE user_id='$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) > 0) {
		$content .= '<form id="confirm_payment" method="POST" action="'.$urlfunc->makePretty("?p=webmember&action=do_confirm&pid=$txid").'" enctype="multipart/form-data">';
		$content .= '<input type="hidden" name="action" value="do_confirm">';
		$content .= '<input type="hidden" name="pid" value="'.$pid.'">';
		$content .= '<input type="hidden" id="confirm_msg" name="confirm_msg" value="'._MSGCONFIRM.'">';
		$content .= '<table>';
		$content .= '
		<tr>
			<td>'._BUKTITRANSFER.' *</td>
			<td><input type="file" name="filename" accept="image/*,.pdf"required></td>
		</tr>';
		$content .= '
		<tr>
			<td colspan="2"><input type="submit" class="btn btn-default more" name="konfirmasi" value="'._KONFIRMASI.'"></td>
		</tr>';
		$content .= '
		<tr>
			<td colspan="2">'.$msgasterisk.'</td>
		</tr>';


		$content .= '</table>';
		$content .= '</form>';
	}
}

if ($action == 'do_confirm') {

	$pid = $_POST['pid'];
	$attachment = array();
	$isValid = true;

	if (!isset($pid)) {
		$isValid = false;
	}

	$sql = "SELECT user_id FROM $tabelwebmember WHERE user_id='$pid'";
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result) == 0) {
		$isValid = false;
	}

	if ($isValid) {

		if ($_FILES['filename']['error'] == UPLOAD_ERR_OK) {
			$pathinfo = pathinfo($_FILES['filename']['name']);
			if ($_FILES['filename']['size'] > $maxupload) {
				$is_valid = false;
				$errors[] = "Oops! Your files size is to large";
			}

			if (!in_array($pathinfo['extension'], $allowedtypes)) {
				$errors[] = "The uploaded file is not supported file type \r\n Only the following file types are supported: ".implode(', ',$allowedtypes);
				$is_valid = false;
			}

			$tmpFilePath = $_FILES['filename']['tmp_name'];

			if ($tmpFilePath != ""){
				$newFilePath = $cfg_thumb_path .'/'. $_FILES['filename']['name'];

				if(move_uploaded_file($tmpFilePath, $newFilePath)) {
					$attachment = "$newFilePath";
				}
			}


			// $now = date('Y-m-d H:i:s');
			// $sql = "UPDATE $tabelwebmember SET tgl_transfer='$now' WHERE user_id = '$pid' ";
			// $url = $urlfunc->makePretty("?p=webmember&action=history");

			// if ($result = $mysql->query($sql)) {

				$sql = "SELECT user_email, fullname FROM webmember WHERE user_id=$pid";
				$result = $mysql->query($sql);
				list($user_email, $fullname) = $mysql->fetch_row($result);

				$message = $email_anggota_setelah_konfirma;

				$message_admin = "
					<h1>Ada pembayaran pendaftaran</h1>
					<p>Nama Anggota: $fullname</p>
					<p>Silakan cek mutasi</p>
				";
				fiestophpmailer($user_email,_SUBJECTANGGOTAAFTERKONFIRMASI,$message,$smtpuser,$sendername,$smtpuser, $message);
				fiestophpmailer($umbalemail,'Ada pembayaran pendaftaran',$message_admin,$smtpuser,$sendername,$smtpuser, $message_admin, $attachment);
				$content .= "
				<p>Terima Kasih, konfirmasi transfer anda sudah diterima oleh system kami. Admin akan melakukan pengecekan terlebih dahulu maksimal dalam 2x24 jam untuk melakukan verifikasi transfer anda.</p>
				<p>Informasi selanjutnya akan dikirimkan melalui email anda yang telah terdaftar di website <a href=\"https://ahki.or.id\">www.ahki.or.id</a></p>
				<p><a href=\"$cfg_app_url\" class=\"btn btn-default more tombol-back\">Kembali</a></p>
				";
			// }
		}
	}

}

if ($action == 'renew') {
	
	$user_id = base64_decode($pid);
	
	$content .= '<h2>Perpanjang masa aktif member Anda?</h2>';
	$content .= '<form id="form-renew" method="POST" action="'.$urlfunc->makePretty("?p=webmember&action=dorenew").'">';
	$content .= '<input type="hidden" name="user_id" value="'.$user_id.'">';
	$content .= '<button type="submit" style="margin-right:10px">Confirm</button><a href="'.$urlfunc->makePretty("?p=webmember").'">'._BACK.'</a>';
	$content .= '</form>';
}

if ($action == 'dorenew') {
	
	$user_id = fiestolaundry($_POST['user_id'], 11);
	
	$sql = "SELECT user_email, fullname, kategori_hipnoterapi, gender, kode_digit, expired FROM webmember WHERE user_id='$user_id'";
	$result = $mysql->query($sql);
	
	if ($mysql->num_rows($result) > 0) {
		list($user_email, $fullname, $kategori_hipnoterapi, $gender, $kode_digit, $expired) = $mysql->fetch_row($result);
		
		$nominal = ($kategori_hipnoterapi == 1) ? substr($amountcats[$kategori_hipnoterapi], 0, 3) . $kode_digit : substr($amountcats[$kategori_hipnoterapi], 0, 4) . $kode_digit;
		$terbilang = bilangan($nominal);
		$kode = base64_encode($user_id);
		$html = array(
			'/#namaanggota/' => $fullname,
			'/#gender/' => $gender_prefix,
			'/#expired/' => get_expired(),
			'/#kategori/' => "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>",
			'/#nominalkategori/' => "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>",
			'/#httpserver/' => '<a href="'.$http_server.$kode.'">'.$http_server.$kode.'</a>'
		);
		$htmlmsg_user = preg_replace(array_keys($html), array_values($html), $email_anggota_renew);
		
		$postnama = $fullname;
		$postuser = $nickname;
		$postemail = $user_email;
		$kategori = "<strong>$hipnotetapiscats[$kategori_hipnoterapi]</strong>";
		$nominalkategori = "<strong>Rp " . number_format($nominal, 0, ',', '.') . ",- ($terbilang rupiah)</strong>";
			
		include("$cfg_app_path/modul/$namamodul/lang/$lang/mailactv.php");
		
		
		$sql = "UPDATE webmember SET isrenew=1 WHERE user_id='$user_id'";
		if ($mysql->query($sql)) {

			// admin
			fiestophpmailer($umbalemail, _FORMRENEWANGGOTA, $htmlmsg_admin, $smtpuser, $sendername, $smtpuser);
			
			// user
			fiestophpmailer($user_email, _FORMRENEWANGGOTA, $htmlmsg_user, $smtpuser, $sendername, $smtpuse, $htmlmsg_user);
			
			header("Location: " . $urlfunc->makePretty("?p=webmember"));
			exit;
		}
		
		/* $now = date('Y-m-d H:i:s');
		$sql = "INSERT INTO webmember_detail (user_id, user_name, status, create_at) VALUES ('$user_id', '$fullname', 'RENEW', '$now')";
		$mysql->query($sql); */
	}
	
}