<?php
$tabelwebmember = "webmember";
$prohibitedpasses = array('password','123456','abcdef');
$expired_periode = 3; // Jumlah dalam hari

$sql = "SELECT name, value FROM config WHERE modul='webmember'";
$result = $mysql->query($sql);
while(list($name, $value) = $mysql->fetch_row($result)) {
	$$name = $value;
}

$sql = "SELECT value FROM contact WHERE name='umbalemail'";
$result = $mysql->query($sql);
list($umbalemail) = $mysql->fetch_row($result);

$cfg_fullsizepics_path = "$cfg_img_path/webmember/large";
$cfg_fullsizepics_url = "$cfg_img_url/webmember/large";
$cfg_thumb_path = "$cfg_img_path/webmember/small";
$cfg_thumb_url = "$cfg_img_url/webmember/small";

$maxfilesize = 3000000;		//Max file size allowed to be uploaded
$prefix_tahun = 1940;


$bulan_sekarang = date('m');
	
$tahun_sekarang  = mktime(0, 0, 0, 12  , 31, date("Y"));
$tahun_depan  = mktime(0, 0, 0, 12,   31,   date("Y")+1);

// Kelompok pengaturan masa berlaku membership
$kelompok_awal = array(1, 2, 3, 4, 5, 6);
$kelompok_akhir = array(7, 8, 9, 10, 11, 12);

// Interval kirim email pemberitahuan.
$alert1 = 15; 	//hari
$alert2 = 30; 	//hari
$alert3 = 45; 	//hari

////////////WEB MEMBER
//REGISTER
$is_public_register = true; // Apakah Pengunjung boleh register
//ACTIVATION
// 0 aktivasi hanya oleh admin,
// 1 aktivasi lwt email,
// 2 auto aktivasi ketika daftar,
// 3 auto aktivasi dan otomatis login
$is_public_activation = 3; 

$educations = array(_SMA, _D3, _S1, _S2, _S3, _OTHERS);
$amountcats = array(1 => 500000, 1000000);
$hipnotetapiscats = array(1 => _HIPNOTERAPIS, _HIPNOTERAPISKLINIS);

/**
 * Google reCaptcha 
 * Aly
 */
require_once __DIR__ . '/../../aplikasi/vendor/autoload.php';

// Register API keys at https://www.google.com/recaptcha/admin
$siteKey = '6LewvS8UAAAAAEnQ4FtcVuFzGPH4yoAFyy7W567M';
$secret = '6LewvS8UAAAAAATlAQiilIPcQP2zYIlpccVhWNb7';

if ($admin == '') {
	ob_start();
	echo <<<END
	<!--================================Awal Style Webmember=============================================-->
	<!--<link type="text/css" href="$cfg_app_url/modul/webmember/css/basic.css" rel="stylesheet">-->
	<link href="$cfg_app_url/modul/webmember/css/smart_wizard.css" rel="stylesheet" type="text/css" />
	<link href="$cfg_app_url/modul/webmember/css/smart_wizard_theme_dots.css" rel="stylesheet" type="text/css" />
	<!--================================Akhir Style Webmember=============================================-->
END;

	$style_css['webmember']=ob_get_clean();

	ob_start();
	echo <<<JS
	<script src="$cfg_app_url/js/jquery.validate.min.js"></script>
	<!-- Include jQuery Validator plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js"></script>


    <!-- Include SmartWizard JavaScript source -->
    <script type="text/javascript" src="$cfg_app_url/modul/webmember/js/jquery.smartWizard.min.js"></script>
	<script>
	
		$(document).ready(function()
		{
			
			$('.foto-item-del').on('click', function() {
				let img = $(this).attr('data-img');
				let id = $(this).attr('data-id');
				let state = $(this).attr('data-state');
				
				let div = $(this).closest('div');
				
				let url = "$cfg_app_url/modul/webmember/ajax.php";
				let data = {action: 'image_del', pid: id, img: img, state :state};
				
				$.ajax({
					url: url,
					data: data,
					success: function(msg) {
						if (msg == 1) {
							div.remove();
							alert('File berhasil dihapus!');
						}
					}
					
				})
			})
			
			$(".menu-edit-password > a").click(function()
			{
				$(this).addClass("active");
				$(this).siblings().removeClass("active");
			});
			
			$('#txtEducation').on('change', function() {
				let id = $(this, "option:seleceted").val();
				let edu_other = $("#education_other");
				console.log(id);
				if (id == 5) {
					edu_other.show();
					edu_other.focus();
				} else {
					edu_other.hide();
				}
			})
			
			$('#signupform').validate({
				rules: {
					txtEmail: {
						required: true,
						email: true
					},
					txtNama: {
						required: true,
						minlength: 3
					},
					txtPonsel1: {
						required: true,
						number: true
					}
				}
			})
			
			
			$('#form-renew').submit(function() {
				var pilihan = confirm('Yakin akan perpanjang member Anda?');
				
				if (pilihan) {
					return true;
				} else {
					return false;
				}
			});
			
		});
		
		function readURL(input) {
			
			// var id = input.getAttribute('name');
			var id = $(input).attr('name');
			var div = $(input).closest('div');
			
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#imgresult-'+id).attr({'src': e.target.result, 'width': '100px'});
					div.addClass('thumb');
					// localStorage.setItem("imgresult_"+id, e.target.result);
					// $('#result-'+id).val(e.target.result);
				};
				reader.readAsDataURL(input.files[0]);
			}
		}
	</script>
JS;

	$script_js['webmember'] = ob_get_clean();
}