<?php

$cfg_fullsizepics_path = "$cfg_img_path/book/large";
$cfg_fullsizepics_url = "$cfg_img_url/book/large";
$cfg_thumb_path = "$cfg_img_path/book/small";
$cfg_thumb_url = "$cfg_img_url/book/small";

$maxfilesize = 500000;		//Max file size allowed to be uploaded
// $cfg_thumb_width = 133;		//pixels
$max_per_page = 12;

//Cantumkan nama table yang langsung terkait dengan modul ini, penamaan wajib namamodul'_drop'.
$car_drop = array('book');

if ($admin) {
ob_start();
echo <<<STYLE
<style>
.autocomplete-suggestions { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; border: 1px solid #999; background: #FFF; cursor: default; overflow: auto; -webkit-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); -moz-box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); box-shadow: 1px 4px 3px rgba(50, 50, 50, 0.64); }
.autocomplete-suggestion { padding: 2px 5px; white-space: nowrap; overflow: hidden; }
.autocomplete-no-suggestion { padding: 2px 5px;}
.autocomplete-selected { background: #F0F0F0; }
.autocomplete-suggestions strong { font-weight: bold; color: #000; }
.autocomplete-group { padding: 2px 5px; }
.autocomplete-group strong { font-weight: bold; font-size: 16px; color: #000; display: block; border-bottom: 1px solid #000; }
</style>
STYLE;
$style_css['book'] = ob_get_clean();
}

if($admin)
{
//css untuk admin
// echo <<<END
// <!--================================awal Style Catalog=============================================-->
// <link type="text/css" href="$cfg_app_url/modul/book/css/admin.css" rel="stylesheet">
// <!--================================akhir Style Catalog=============================================-->
// END;

}
else
{

ob_start();
//css untuk  front end
echo <<<END
	<link type="text/css" href="$cfg_app_url/js/mp/magnific-popup.css" rel="stylesheet">
END;
$style_css['book'] = ob_get_clean();
}

ob_start();
echo <<<SCRIPT
<script src="$cfg_app_url/js/jquery.autocomplete.min.js"></script>
<script type="text/javascript" src="$cfg_app_url/js/mp/jquery.magnific-popup.min.js"></script>
<script>
	$(function() {
		$('.image-popup').magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			mainClass: 'mfp-img-mobile',
			image: {
				verticalFit: true
			}
			
		});
		
	})
</script>
SCRIPT;
$script_js['book'] = ob_get_clean();

?>