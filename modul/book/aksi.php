<?php 

$action = fiestolaundry($_GET['action'], 10);
$pid = fiestolaundry($_GET['pid'], 11);
$screen = fiestolaundry($_GET['screen'],11);

if ($screen == '') $screen = 0;

$sql = "SELECT judulfrontend FROM module WHERE nama='book'";
$result = $mysql->query($sql);
list($title) = $mysql->fetch_row($result);

if ($action == '' || $action == 'list') {
	
	$sql = "SELECT id, penulis, penerbit, jumlah_halaman, filename, judul_buku FROM literatur WHERE status=1 ";
	$result = $mysql->query($sql);
	$total_pages = $mysql->num_rows($result);
	$pages = ceil($total_pages/$max_per_page);
	
	if ($total_pages > 0) {
		
		$start = $screen * $max_per_page;
		$sql .= " ORDER BY judul_buku ASC ";
		$sql .= " LIMIT $start, $max_per_page";
		
		$result = $mysql->query($sql);
		
		if ($pages>1) $catpage = aksipagination($namamodul, $screen, "action=list");	
		$content .= ( $catpage != '') ? "<div class=\"catpage\">$catpage</div>\r\n" : "";
		
		$content .= '<div class="row" id="book-list-thumbnail">';
		while(list($id, $penulis, $penerbit, $jumlah_halaman, $filename, $judul_buku) = $mysql->fetch_row($result)) {
			$thumbnail = ($filename != '' && file_exists("$cfg_thumb_path/$filename")) ? "$cfg_thumb_url/$filename" : "";
			
			$content .= "					
				<div class=\"col-sm-6\">
					<div class=\"book-list\">";
						$content .= "
								<div class=\"img-book\">
									<a class=\"image-popup\" href=\"$cfg_fullsizepics_url/$filename\" title=\"$penulis\">
										<img class=\"img-responsive\" alt=\"$penulis\" src=\"$thumbnail\">
									</a>
								</div>	<!-- /.img-news -->\r\n";
						$content .= "<div class=\"book-content\">";
						if ($judul_buku != '') $content .= "<div class=\"booktitle\">$judul_buku</div>";
						$content .= "<div class=\"bookpenulis\">$penulis</div>";
						if ($penerbit != '') $content .= "<div class=\"bookpenerbit\">$penerbit</div>";
						if ($jumlah_halaman != '') $content .= "<div class=\"bookpage\">$jumlah_halaman Halaman</div>";
						
						$content .= "
						</div>
					</div>	<!-- /.panel-body -->\r\n
				</div>	<!-- /.col-sm-6 col-md-6 -->";	
		}
		
		$content .= '</div>';
	}
	
}

// if ($action == 'view') {
	
	// $sql = "SELECT t.filename, t.title, t.keterangan, b.nama, c.type, c.year, t.tuner, s.bengkel, t.product_id, t.store_id
			// FROM tuning_guide t 
			// LEFT JOIN cellostore s ON s.id=t.store_id 
			// LEFT JOIN car c ON c.id=t.car_id
			// LEFT JOIN car_brand b ON b.id=c.brand_id
			// WHERE t.id='$pid'";
	
	// if ($result = $mysqli->query($sql)) {
		// $num_rows = $result->num_rows;
		
		// if ($num_rows > 0) {
			
			// $row = $result->fetch_assoc();
			// $mobil = $row['nama'].' '.$row['type'].' '.$row['year'];
			// $store_id = $row['store_id'];
			
			// $thumbnail = ($row['filename'] != "" && file_exists("$cfg_fullsizepics_path/{$row['filename']}")) ? "<img src=\"$cfg_fullsizepics_url/{$row['filename']}\" class=\"img-responsive\">" : "";
			
			// $titleurl = array();
			// $titleurl['pid'] = $row['bengkel'];
			
			// $content .= '<h2 class="second-header">' .$row['title'].'</h2>';
			// $content .= '<div id="guide-detail">';
			// $content .= '<div id="image-guide">';
			// if($row['filename']!='') {
				// $f = explode(":",$row['filename']);
				// $class_shop = (count($f) > 1) ? "shop" : "shopx";
				// $content .= '<div id="image-overview" class="'.$class_shop.' shop-single">';
								// // <!--<div class="container">-->
				// $content .= '<div class="row">';
				// $content .= '<div class="col-md-12">';
				// $content .= '<div class="shop-content-item">';
				// $content .= '<div class="icon-product">'.$cisnew.$cisobral.$cispromo.$cissold.'</div>	<!-- /.icon-product -->';
										// // <!-- Slider -->
				// $content .= '<div class="shop-slider-container">';
				// $content .= '<ul class="gallery" id="shop-slider">';
											
				// $x=0;
				// foreach($f as $i => $im) {	
					// $image_temp .= $i == 0 ? "$cfg_fullsizepics_url/$im" : "";
					// $x++;
					// $content .= '<li class="zoom"><img alt="" src="'.$cfg_fullsizepics_url.'/'.$im.'" /></li>';
				// }
				// $content .= '</ul>';
				// $content .= '<div class="bottom-border"></div>';
				// $content .= '<div class="shop-slider-pager" id="shop-slider-pager">';
				// if($x > 1) {	
					// $index = 0;
					// foreach($f as $i => $im) {
						// $content .= '<a data-slide-index="'.$index.'" href="#"><img alt="" src="'.$cfg_fullsizepics_url.'/'.$im.'" /></a>';
						// $index++;
					// }
				// }
				// $content .= '</div>	<!-- /#shop-slider-pager -->';
				// $content .= '</div> <!-- /.shop-slider-container -->';
				// $content .= '</div> <!-- /.shop-content-item -->';
				// $content .= '</div> <!-- /.col-md-12 -->';
				// $content .= '</div>	<!-- /.row -->';
				// $content .= '</div> <!-- /#shop-single -->';
			// }
			// $content .= '</div>';
			// $content .= '<div class="content-guide">';
			// $content .= '<table>';
			// $content .= '<tr><td>'._MOBIL.'</td><td>:</td><td> <span>'.$mobil.'</span></td></tr>';
			// $content .= '<tr valign="top"><td>'._PRODUCT.'</td><td>:</td><td>';
			
			// if ($row['product_id'] != '') {
				
				// $products = explode(',', $row['product_id']);
				// $titles = array();
				
				// if (count($products) > 0) {
					
					// foreach($products as $product_id) {
						// $titles[] = get_product_title($product_id);
					// }
					// asort($titles);
					// $judul = join(', ', $titles);
					
					// $content .= '<span>'.$judul.'</span>';
				// }
			// }
			
			// $content .= '</td></tr>';
			
			// $content .= '<tr><td>'._TUNER.'</td><td>:</td><td> <span>'.$row['tuner'].'</span></td></tr>';
			// $content .= '<tr><td>'._WORKSHOP.'</td><td>:</td><td> <a href="'.$urlfunc->makePretty("?p=cellostore&action=view&pid=$store_id", $titleurl).'"><span>'.$row['bengkel'].'</span></a></td></tr>';
			// $content .= '</table>';
			// $content .= '</div>';
			// $content .= '<div class="desc-guide">';
			// $content .= '<h2>'._DESCRIPTIONTUNING.'</h2>';
			// $content .= $row['keterangan'];
			// $content .= '</div>';
			// $content .= '</div>';
		// }
	// }
// }

// if ($action == 'list') {
	
	// if (!empty($cat_id)) {
		// $sql = "SELECT id, type FROM car WHERE brand_id='$cat_id' ORDER BY type";
		// $result = $mysqli->query($sql);
		// $types = array();
		// while($row = $result->fetch_assoc()) {
			// $types[] = array(
				// 'id' => $row['id'],
				// 'name' => $row['type']
			// );
		// }
	// }
	
	// if (count($types) > 0) {
		
		// foreach($types as $type) {
			// $car_id = $type['id'];
			// $car_name = $type['name'];
			
			// // $sql = "SELECT id, nama, filename FROM showoff WHERE car_id='$car_id'";
			// $sql = "SELECT t.id, t.title, t.filename, t.keterangan, b.nama, c.type, c.year, t.tuner, s.bengkel, t.product_id, t.store_id
					// FROM tuning_guide t 
					// LEFT JOIN cellostore s ON s.id=t.store_id 
					// LEFT JOIN car c ON c.id=t.car_id
					// LEFT JOIN car_brand b ON b.id=c.brand_id
					// WHERE car_id='$car_id'";
					// // echo "$sql;<br>";
			
			// if ($result = $mysqli->query($sql)) {
				// $num_rows = $result->num_rows;
				
				// if ($num_rows > 0) {
					
					// $content .= '
					// <div id="showoff-list" class="grey-block">
						// <div id="snowoff-oem-point" class="point-anchor"></div>
						// <h1 id="title-block-oem">
							// '.$car_name.'
						// </h1>
						// <div class="list-product">
						// <div class="owl-carousel owl-theme detail-product-slide">
					// ';
					
					// while($row = $result->fetch_assoc()) {
						// $id = $row['id'];
						// $car_name = $row['nama'];
						// $car_type = $row['type'];
						// $car_year = $row['year'];
						// $filename = $row['filename'];
						// $store_id = $row['store_id'];
						// $installer = _PROVIDEDBY . ' ' . $row['installer'];
						
						// // $name = "$car_name $car_type $car_year";
						// $name = "$row[title]";
						// $titleurl = array();
						// $titleurl['pid'] = $name;
						
						// if ($filename != '') {
							// $filenames = explode(':', $filename);
							// $file = $filenames[0];
							// $thumbnail = ($file != '' && file_exists("$cfg_thumb_path/$file")) ? "$cfg_thumb_url/$file" : "$cfg_app_url/images/default-store.jpg";
						// } else {
							// $thumbnail = "$cfg_app_url/images/default-store.jpg";
						// }
						
						// $content .= '<div class="item">';
						// $content .= '<div class="panel-body news-colx">';
						// $content .= '
									// <div class="img-news">
										// <a href="'.$urlfunc->makePretty("?p=tuning_guide&action=view&pid=$id", $titleurl).'">
											// <img class="img-responsive" alt="'.$photo_title.'" src="'.$thumbnail.'">
										// </a>
									// </div>	
									// <div class="news-content caption-news-thumb">
										// <h1 class="newstitle"><a href="'.$urlfunc->makePretty("?p=tuning_guide&action=view&pid=$id", $titleurl).'">
											// '.$name.'
										// </a></h1><br>By
										// <div classs="bengkel"><a href="'.$urlfunc->makePretty("?p=cellostore&action=view&pid=$store_id", $titleurl).'">'.$row['bengkel'].'</a></div>
										// <!--<p>'.$installer.'</p>-->
									// </div>	
						// ';
						// $content .= '</div>	<!-- /.product-col -->';
						// $content .= '</div>	<!-- /.item -->';
					// }
					
					// $content .= '</div>	<!-- /.list-product -->';
					// $content .= '</div>	<!-- /#list-product -->';
					// $content .= '</div>	<!-- /#showoff-list -->';
				// } else {
					
				// }
			// }
		// }
	// } else {
		
	// }
// }