<?php

if (!$isloadfromindex) {
    include ("../../kelola/urasi.php");
    include ("../../kelola/fungsi.php");
    include ("../../kelola/lang/$lang/definisi.php");
    pesan(_ERROR, _NORIGHT);
}

$keyword = fiestolaundry($_GET['keyword'], 100);
$screen = fiestolaundry($_GET['screen'], 11);
$pid = fiestolaundry($_REQUEST['pid'], 11);

$action = fiestolaundry($_REQUEST['action'], 10);
$nama = fiestolaundry($_POST['nama'], 200);

$notification = fiestolaundry(($_GET['notification']));

$modulename = $_GET['p'];
if (isset($_POST['back'])) {
    header("Location:?p=$modulename");
}

if ($action == '') {
    $sql = "SELECT id FROM catalogmerek";
    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($notification == "addsuccess") {
        $admincontent .= createnotification(_ADDSUCCESS, _SUCCESS, "success");
    } else if ($notification == "editsuccess") {
        $admincontent .= createnotification(_EDITSUCCESS, _SUCCESS, "success");
    } else if ($notification == "delsuccess") {
        $admincontent .= createnotification(_DELETESUCCESS, _SUCCESS, "success");
    } else if ($notification == "dberror") {
        $admincontent .= createnotification(_DBERROR, _ERROR, "error");
    }

    if ($mysql->num_rows($result) > 0) {
        $start = $screen * $max_page_list;

        $sql = "SELECT id, nama FROM catalogmerek ORDER BY nama LIMIT $start, $max_page_list";
        $result = $mysql->query($sql);

        if ($pages > 1) {
            $adminpagination = pagination($namamodul, $screen);
        }
        $admincontent .= "<table class=\"stat-table\">\n";
        $admincontent .= "<tr><th>" . _BRAND . "</th>";
        $admincontent .= "<th>" . _EDIT . "</th><th>" . _DEL . "</th></tr>\n";
        while (list($id, $merek) = $mysql->fetch_row($result)) {
            $admincontent .= "<tr class=\"merek\">\r\n";
            $admincontent .= "<td>$merek</td>\r\n";
            $admincontent .= "<td align=\"center\"><a href=\"?p=$modulename&action=modify&pid=$id\">";
            $admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
            $admincontent .= "<td align=\"center\"><a href=\"?p=$modulename&action=remove&pid=$id\">";
            $admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            $admincontent .= "</tr>\n";
        }
        $admincontent .= "</table>";
    } else {
        $admincontent .= createstatus(_NOBRAND, "error");
    }
    $specialadmin .= "<a href=\"?p=$modulename&action=add\" class=\"btn btn-danger\">" . _ADDBRAND . "</a>";
}
if ($action == 'search') {
    $admintitle = _SEARCHRESULTS;
    $sql = "SELECT id, nama FROM catalogmerek WHERE (nama LIKE '%$keyword%') ORDER BY nama";
    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($mysql->num_rows($result) > 0) {
        $start = $screen * $max_page_list;
        $sql = "SELECT id, nama FROM catalogmerek WHERE (nama LIKE '%$keyword%') ORDER BY nama LIMIT $start, $max_page_list ";
        $result = $mysql->query($sql);
        if ($pages > 1) {
            $adminpagination = pagination($namamodul, $screen);
        }
        $admincontent .= "<table class=\"stat-table\">\n";
        $admincontent .= "<tr><th>" . _BRAND . "</th>";
        $admincontent .= "<th>" . _EDIT . "</th><th>" . _DEL . "</th></tr>\n";
        while (list($id, $merek) = $mysql->fetch_row($result)) {
            $admincontent .= "<tr class=\"merek\">\n";
            $admincontent .= "<td>$merek</td>";
            $admincontent .= "<td align=\"center\"><a href=\"?p=$modulename&action=modify&pid=$id\">";
            $admincontent .= "<img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a></td>\n";
            $admincontent .= "<td align=\"center\"><a href=\"?p=$modulename&action=remove&pid=$id\">";
            $admincontent .= "<img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            $admincontent .= "</tr>\n";
        }
        $admincontent .= "</table>";
    } else {//menampilkan record di halaman yang sesuai
        $admincontent .= createstatus(_NOSEARCHRESULTS, "error");
    }
    $specialadmin = "<a href=\"?p=$modulename\" class=\"btn\">" . _BACK . "</a>";
}

if ($action == "add") {
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($nama, _BRAND, false);
        if ($notificationbuilder != "") {
            $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
        } else {
            $sql = "INSERT INTO catalogmerek (nama) VALUES ('$nama')";
            $result = $mysql->query($sql);
            if ($result) {
                header("Location:?p=$modulename&notification=addsuccess");
            } else {
                $builtnotification = createnotification(_DBERROR, _ERROR, "error");
            }
        }
    } else {
        /* default value untuk form */
        $nama = "";
    }

    $admintitle = _ADDBRAND;
    $admincontent .= '
	<form class=\"form-horizontal\" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
            <input type="hidden" name="action" value="add">
            ' . $builtnotification . '
            <div class="control-group">
                    <label class="control-label">' . _BRAND . '</label>
                    <div class="controls">
                            <input type="text" name="nama" placeholder="' . _BRAND . '" class="span12">
                    </div>
            </div>
            <div class="control-group">
                    <div class="controls">
                            <input type="submit" name="submit" class="btn btn-danger" value="' . _ADD . '">
                            <input type="submit" name="back" class="btn" value="' . _BACK . '">
                    </div>
            </div>
	</form>
    ';
}

if ($action == "modify") {
    $sql = "SELECT id, nama FROM catalogmerek WHERE id='$pid'";
    $result = $mysql->query($sql);
    $result_list = $result;
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($nama, _BRAND, false);
        if ($notificationbuilder != "") {
            $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
        } else {
            $sql = "UPDATE catalogmerek SET nama='$nama' WHERE id='$pid'";
            $result = $mysql->query($sql);
            if ($result) {
                header("Location:?p=$modulename&notification=editsuccess");
            } else {
                $builtnotification = createnotification(_DBERROR, _ERROR, "error");
            }
        }
    } else {
        /* default value untuk form */
        list($id, $merek) = $mysql->fetch_row($result);
    }

    $admintitle = _EDITBRAND;
    if ($mysql->num_rows($result) > 0) {
        $admincontent .= '
		  <form class=\"form-horizontal\" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
                        <input type="hidden" name="action" value="modify">
                        <input type="hidden" name="pid" value="' . $pid . '">
                        ' . $builtnotification . '
                        <div class="control-group">
                                <label class="control-label">' . _BRAND . '</label>
                                <div class="controls">
                                    <input type="text" name="nama" value="' . $merek . '" placeholder="' . _BRAND . '" class="span12">
                                </div>
                        </div>
                        <div class="control-group">
                                <div class="controls">
                                        <input type="submit" name="submit" class="btn btn-danger" value="' . _EDIT . '">
                                        <input type="submit" name="back" class="btn" value="' . _BACK . '">
                                </div>
                        </div>
		</form>';
    } else {
        $admincontent .= createstatus(_NOBRAND, "error");
    }
}

// DELETE BRAND
if ($action == "remove" || $action == "delete") {
    $admintitle = _DELBRAND;
    $sql = "SELECT id, nama FROM catalogmerek WHERE id='$pid'";
    $result = $mysql->query($sql);
    if ($mysql->num_rows($result) == "0") {
        $$admincontent .= createstatus(_NOBRAND, "error");
    } else {
        list($id, $merek) = $mysql->fetch_row($result);
        if ($action == "remove") {
            $admincontent .= "<h5>" . _PROMPTDEL . "</h5>";
            $admincontent .= '<div class="control-group">
                                <div class="controls">
                                        <a class="btn btn-danger" href="?p=' . $modulename . '&action=delete&pid=' . $id . '">' . _YES . '</a>
                                        <a class="btn" href="javascript:history.go(-1)">' . _NO . '</a></p>
                                </div>
                        </div>';
        } else {
            $sql = "DELETE FROM catalogmerek WHERE id='$pid'";
            $result = $mysql->query($sql);
            if ($result) {
                header("Location:?p=$modulename&notification=delsuccess");
            } else {
                header("Location:?p=$modulename&notification=dberror");
            }
        }
    }
}
?>
