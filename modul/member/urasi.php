<?php
$namalevel = array("Super Administrator","Pengelola","Therapist");

ob_start();
echo <<<SCRIPT
<style>
</style>
<link href="$cfg_app_url/js/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="$cfg_app_url/js/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="$cfg_app_url/js/select2/js/select2.min.js"></script>
<script>
	$(function() {
		$("#therapist_id").select2({
			theme: "bootstrap"
		});
		
		$("#newlevel").change(function() {
			var id = $(this, "option:selected").val();
			var therapist = $("#block-therapist");
			if (id == 2) {
				therapist.show();
			} else {
				therapist.hide();
			}
			
			
		})
	})
</script>
SCRIPT;
$script_js['member'] = ob_get_clean();
?>
