<?php 

$action = fiestolaundry($_GET['action'], 10);
$pid = fiestolaundry($_GET['pid'], 11);
$screen = fiestolaundry($_GET['screen'],11);

if ($screen == '') $screen = 0;

$sql = "SELECT judulfrontend FROM module WHERE nama='jurnal'";
$result = $mysql->query($sql);
list($title) = $mysql->fetch_row($result);

if ($action == '' || $action == 'list') {
	
	$sql = "SELECT id, judul, filename, keterangan, link FROM jurnal ";
	$result = $mysql->query($sql);
	$total_pages = $mysql->num_rows($result);
	$pages = ceil($total_pages/$max_per_page);
	
	if ($total_pages > 0) {
		
		$start = $screen * $max_per_page;
		$sql .= " ORDER BY judul ASC ";
		$sql .= " LIMIT $start, $max_per_page";
		
		$result = $mysql->query($sql);
		
		if ($pages>1) $catpage = aksipagination($namamodul, $screen, "action=list");	
		$content .= ( $catpage != '') ? "<div class=\"catpage\">$catpage</div>\r\n" : "";
		
		$content .= '<div class="row" id="jurnal-list">';
		while(list($id, $judul, $filename, $keterangan, $link) = $mysql->fetch_row($result)) {
			$thumbnail = ($filename != '' && file_exists("$cfg_thumb_path/$filename")) ? "$cfg_thumb_url/$filename" : "";
			
			$content .= "					
				<div class=\"col-sm-3 col-md-2\">
					<div class=\"jurnal-list\">";
						$content .= "
								<div class=\"img-jurnal\">
									<a href=\"".$urlfunc->makePretty("?p=jurnal&action=view&pid=$id", $titleurl)."\" title=\"$penulis\">
										<img class=\"img-responsive\" alt=\"$penulis\" src=\"$thumbnail\">
									</a>
								</div>	<!-- /.img-news -->\r\n";
						$content .= "<div class=\"book-content\">";
						if ($judul != '') $content .= "<div class=\"jurnaltitle\"><a href=\"".$urlfunc->makePretty("?p=jurnal&action=view&pid=$id", $titleurl)."\">".$judul."</a></div>";
						
						
						$content .= "
						</div>
					</div>	<!-- /.panel-body -->\r\n
				</div>	<!-- /.col-sm-6 col-md-6 -->";	
		}
		
		$content .= '</div>';
	}
	
}

if ($action == 'view') {
		$sql = "SELECT id, judul, filename, keterangan, link FROM jurnal WHERE id='$pid'";
		$result = $mysql->query($sql);
		if ($mysql->num_rows($result) > 0) {
			
			list($id, $judul, $filename, $keterangan, $link) = $mysql->fetch_row($result);
			$title = $judul;
			$thumbnail = ($filename != '' && file_exists("$cfg_fullsizepics_path/$filename")) ? "$cfg_fullsizepics_url/$filename" : "";
			$content .= "<div class=\"row\">";
			$content .= "<div class=\"col-md-12 video-detail-left\">";
			$content .= "<img class=\"img-responsive image-detail-jurnal\" alt=\"$penulis\" src=\"$thumbnail\">";
			if ($keterangan != '') $content .= "<div class=\"descjurnal\">$keterangan</div>";
			if ($link != '') $content .= "<div class=\"buttonjurnal\"><a class=\"btn btn-default more\" href=".$link.">"._LINKJURNAL."</a></div>";
			$content .= "</div>";
			$content .= "</div>";
		}
	}