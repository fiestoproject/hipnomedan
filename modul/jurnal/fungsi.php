<?php 

function get_product_title($product_id) {
	global $mysqli, $urlfunc;
	
	$sql = "SELECT id, cat_id, title FROM catalogdata WHERE id='$product_id'";
	$result = $mysqli->query($sql);
	list($id, $cat_id, $title) = $result->fetch_row();
	
	$titleurl = array();
	$titleurl['pid'] = $title;
	$titleurl['cat_id'] = get_cat_name($cat_id);
	
	$url = $urlfunc->makePretty("?p=catalog&action=detail&pid=$id&cat_id=$cat_id", $titleurl);
	$html = "<a href=\"$url\">$title</a>";
	return $html;
}