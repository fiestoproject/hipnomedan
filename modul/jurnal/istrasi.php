<?php

if (!$isloadfromindex) {
    include ("../../kelola/urasi.php");
    include ("../../kelola/fungsi.php");
    include ("../../kelola/lang/$lang/definisi.php");
    pesan(_ERROR, _NORIGHT);
}

$keyword = fiestolaundry($_GET['keyword'], 100);
$screen = fiestolaundry($_GET['screen'], 11);
$pid = fiestolaundry($_REQUEST['pid'], 11);

$action = fiestolaundry($_REQUEST['action'], 10);
$judul = fiestolaundry($_POST['judul'], 200);
$link = fiestolaundry($_POST['link'], 200);
$keterangan = fiestolaundry($_POST['keterangan'], 0, TRUE);
$status = fiestolaundry($_POST['status'], 11);

//$notification = fiestolaundry(($_GET['notification']));

$modulename = $_GET['p'];
if (isset($_POST['back'])) {
    back($modulename);
}

if ($action == "save") {
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($judul, _JUDUL, false);
        // $notificationbuilder .= validation($product_id, _TUNINGGUIDEPRODUCT, false);
		
		if ($_FILES['filename']['error'] != UPLOAD_ERR_NO_FILE) {
			//jika kolom file diisi
			//upload dulu sebelum insert record
			$hasilupload = fiestoupload('filename', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if ($hasilupload != _SUCCESS) {
				$notificationbuilder = $hasilupload;
			} else {
				//ambil informasi basename dan extension
				$temp = explode(".", $_FILES['filename']['name']);
				$extension = $temp[count($temp) - 1];
				$basename = '';
				for ($i = 0; $i < count($temp) - 1; $i++) {
					$basename .= $temp[$i];
				}
				
			}
		}
			
        if ($notificationbuilder != "") {
            $action = createmessage($notificationbuilder, _ERROR, "error", "add");
        } else {
			
			$sql = "INSERT INTO jurnal (judul, filename, keterangan, link) VALUES ('$judul', '$filename', '$keterangan', '$link')";
					
			if ($mysql->query($sql)) {
				$newid = $mysql->insert_id();
				$modifiedfilename = "$basename-$newid.$extension";
				
				list($filewidth, $fileheight, $filetype, $fileattr) = getimagesize("$cfg_fullsizepics_path/" . $_FILES['filename']['name']);
				//create thumbnail
				$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
				if ($hasilresize != _SUCCESS)
				$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
							
				if ($filewidth > $cfg_max_width) { //rename sambil resize gambar asli sesuai yang diizinkan
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename", 'w', $cfg_max_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
					//del gambar asli
					unlink("$cfg_fullsizepics_path/" . $_FILES['filename']['name']);
				} else { //create thumbnail
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
					rename("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename");
				}
				
				$sql = "UPDATE jurnal SET filename='$modifiedfilename' WHERE id='$newid'";
				$mysql->query($sql);
									
				$action = createmessage(_DBSUCCESS, _SUCCESS, "success", "");
			} else {
				$action = createmessage(_DBERROR, _ERROR, "error", "add");
			}
        }
    } else {
        /* default value untuk form */
        $action = "";
    }
}
if ($action == "add") {
    $admintitle = _ADDJURNAL;

    $admincontent .= '
	<form class="form-horizontal" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
            <input type="hidden" name="action" value="save">
			<div class="control-group">
			<label class="control-label">' . _COVER . ':</label>
			<div class="controls">
				<div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden"  name="filename" size="35" />
					<div class="input-append">
						<div class="uneditable-input span2">
							<i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span>
						</div>
						<span class="btn btn-file"><span class="fileupload-new">Cari File</span><span class="fileupload-exists">'._CHANGE.'</span>
						<input type="file" name="filename" size="35">
						</span><a data-dismiss="fileupload" class=\btn fileupload-exists" href="#">'._REMOVE.'</a>
					</div>
				</div>
			</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _JUDUL . '</label>
				<div class="controls">
					<input type="text" id="judul" name="judul" placeholder="' . _JUDUL . '" class="span12">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _LINK . '</label>
				<div class="controls">
					<input type="text" id="link" name="link" placeholder="' . _LINK . '" class="span12">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label">' . _KETERANGAN . '</label>
				<div class="controls">
					<textarea type="text" id="keterangan" name="keterangan" placeholder="' . _KETERANGAN . '" class="span12 usetiny"></textarea>
				</div>
			</div>		
            <div class="control-group">
				<div class="controls">
					<input type="submit" name="submit" class="buton" value="' . _ADD . '">
					<input type="submit" name="back" class="buton" value="' . _BACK . '">
				</div>
            </div>
	</form>
    ';
}

if ($action == "update") {
    
    if (isset($_POST['submit'])) {
        $notificationbuilder = "";
        $notificationbuilder .= validation($judul, _JUDUL, false);
		
		if ($_FILES['filename']['error'] != UPLOAD_ERR_NO_FILE) {
			//jika kolom file diisi
			//upload dulu sebelum insert record
			$hasilupload = fiestoupload('filename', $cfg_fullsizepics_path, '', $maxfilesize, $allowedtypes = "gif,jpg,jpeg,png");
			if ($hasilupload != _SUCCESS) {
				$notificationbuilder = $hasilupload;
			} else {
				//ambil informasi basename dan extension
				$temp = explode(".", $_FILES['filename']['name']);
				$extension = $temp[count($temp) - 1];
				$basename = '';
				for ($i = 0; $i < count($temp) - 1; $i++) {
					$basename .= $temp[$i];
				}
				
				$modifiedfilename = "$basename-$pid.$extension";
				
				//create thumbnail
				$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
				if ($hasilresize != _SUCCESS)
				$notificationbuilder = createmessage($hasilresize, _ERROR, "add");

				if ($filewidth > $cfg_max_width) { //rename sambil resize gambar asli sesuai yang diizinkan
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename", 'w', $cfg_max_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
					//del gambar asli
					unlink("$cfg_fullsizepics_path/" . $_FILES['filename']['name']);
				} else { //create thumbnail
					$hasilresize = fiestoresize("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_thumb_path/$modifiedfilename", 'l', $cfg_thumb_width);
					if ($hasilresize != _SUCCESS)
					$notificationbuilder = createmessage($hasilresize, _ERROR, "add");
					rename("$cfg_fullsizepics_path/" . $_FILES['filename']['name'], "$cfg_fullsizepics_path/$modifiedfilename");
				}
				
				//del gambar yang dioverwrite (hanya jika filename beda)
				$sql = "SELECT filename FROM jurnal WHERE id='$pid'";
				$result = $mysql->query($sql);
				list($oldfilename) = $mysql->fetch_row($result);
				if ($modifiedfilename != $oldfilename) {
					if ($oldfilename != '' && file_exists("$cfg_fullsizepics_path/$oldfilename")) unlink("$cfg_fullsizepics_path/$oldfilename");
					if ($oldfilename != '' && file_exists("$cfg_thumb_path/$oldfilename")) unlink("$cfg_thumb_path/$oldfilename");
				}
			}
			
			$sql = "UPDATE jurnal SET filename='$modifiedfilename' WHERE id='$pid'";
			$result = $mysql->query($sql);
		}
		
        if ($notificationbuilder != "") {
            $builtnotification = createnotification($notificationbuilder, _ERROR, "error");
        } else {
			
			$sql = "UPDATE jurnal SET judul='$judul', link='$link', keterangan='$keterangan' WHERE id='$pid'";
			$result = $mysql->query($sql);
			
			if ($result) {
				$action = createmessage(_DBSUCCESS, _SUCCESS, "success", "");	
			} else {
				$action = createmessage(_DBERROR, _ERROR, "error", "");	
			}
        }
    } else {
        /* default value untuk form */
        $action = "modify";
    }
}

if ($action == "modify") {
	$sql  = "SELECT id, judul, filename, keterangan, link
			FROM jurnal
			WHERE id='$pid'";
    $result = $mysql->query($sql);
    $admintitle = _EDIT;
    if ($mysql->num_rows($result) > 0) {
        list($id, $judul, $filename, $keterangan, $link) = $mysql->fetch_row($result);
		if ($filename != '' && file_exists("$cfg_thumb_path/$filename")) {
			$admincontent .= "<div class=\"img-filename\">
			<img src=\"$cfg_thumb_url/$filename\">
			</div>";
		}
		
		$admincontent .= '
		<form class="form-horizontal" method="POST" action="' . $thisfile . '" enctype="multipart/form-data">
				<input type="hidden" name="action" value="update">
				<div class="control-group">
				<label class="control-label">' . _COVER . ':</label>
				<div class="controls">
					<div data-provides="fileupload" class="fileupload fileupload-new"><input type="hidden"  name="filename" size="35" />
						<div class="input-append">
							<div class="uneditable-input span2">
								<i class="icon-file fileupload-exists"></i><span class="fileupload-preview"></span>
							</div>
							<span class="btn btn-file"><span class="fileupload-new">Cari File</span><span class="fileupload-exists">'._CHANGE.'</span>
							<input type="file" name="filename" size="35">
							</span><a data-dismiss="fileupload" class=\btn fileupload-exists" href="#">'._REMOVE.'</a>
						</div>
					</div>
				</div>
				</div>
				<div class="control-group">
					<label class="control-label">' . _JUDUL . '</label>
					<div class="controls">
						<input type="text" id="judul" name="judul" placeholder="' . _JUDUL . '" class="span12" value="'.$judul.'">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">' . _LINK . '</label>
					<div class="controls">
						<input type="text" id="link" name="link" placeholder="' . _LINK . '" class="span12" value="'.$link.'">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label">' . _KETERANGAN . '</label>
					<div class="controls">
						<textarea type="text" id="keterangan" name="keterangan" class="span12 usetiny">'.$keterangan.'</textarea>
					</div>
				</div>		
				<div class="control-group">
					<div class="controls">
						<input type="submit" name="submit" class="buton" value="' . _ADD . '">
						<input type="submit" name="back" class="buton" value="' . _BACK . '">
					</div>
				</div>
		</form>
		';
    } else {
        $admincontent .= createstatus(_NOBRAND, "error");
    }
}

// DELETE BRAND
if ($action == "remove" || $action == "delete") {
    $admintitle = _DELBRAND;
    $sql = "SELECT id, filename FROM jurnal WHERE id='$pid'";
    $result = $mysql->query($sql);
    if ($mysql->num_rows($result) == "0") {
        $action = createmessage(_NOBRAND, _INFO, "info", "");
    } else {
        list($id, $filename) = $mysql->fetch_row($result);
        if ($action == "remove") {
            $admincontent .= "<h5>" . _PROMPTDEL . "</h5>";
            $admincontent .= '<div class="control-group">
                                <div class="controls">
                                        <a class="buton" href="?p=' . $modulename . '&action=delete&pid=' . $id . '">' . _YES . '</a>
                                        <a class="buton" href="javascript:history.go(-1)">' . _NO . '</a></p>
                                </div>
                        </div>';
        } else {
			
			if ($filename != '' && file_exists("$cfg_fullsizepics_path/$filename")) unlink("$cfg_fullsizepics_path/$filename");
            if ($filename != '' && file_exists("$cfg_thumb_path/$filename")) unlink("$cfg_thumb_path/$filename");
					
            $sql = "DELETE FROM jurnal WHERE id='$pid'";
            $result = $mysql->query($sql);
            if ($result) {
                $action = createmessage(_DELETESUCCESS, _SUCCESS, "success", "");
            } else {
                $action = createmessage(_DBERROR, _ERROR, "error", "modify");
            }
        }
    }
}
if ($action == "") {
    $admintitle = "";
	$sql  = "SELECT  id, judul, filename, keterangan, link
			FROM jurnal";
    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($total_records > 0) {
        $start = $screen * $max_page_list;

		$sql  = "SELECT id, judul, filename, keterangan, link
				FROM jurnal ORDER BY judul
				LIMIT $start, $max_page_list";
		
        $result = $mysql->query($sql);

        if ($pages > 1) {
            $adminpagination = pagination($namamodul, $screen);
        }
        $admincontent .= "<table class=\"stat-table table table-stats table-striped table-sortable table-bordered\">\n";
        $admincontent .= "<tr><th>" . _JUDUL . "</th><th>" . _LINK . "</th>";
        $admincontent .= "<th align=\"center\">" . _ACTION . "</th></tr>\n";
        while (list($id, $judul, $filename, $keterangan, $link) = $mysql->fetch_row($result)) {
			$titleurl = array();
			$titleurl['cat_id'] = $car_brand;
			$url = $urlfunc->makePretty("?p=jurnal&action=list&cat_id=$car_id", $titleurl);
            $admincontent .= "<tr class=\"merek\">\r\n";
            $admincontent .= "<td>$judul</td>\r\n";
            $admincontent .= "<td>$link</td>\r\n";
            $admincontent .= "<td align=\"center\" class=\"action-ud\">";
            $admincontent .= "<a href=\"?p=$modulename&action=modify&pid=$id\"><img alt=\"" . _EDIT . "\" border=\"0\" src=\"../images/modify.gif\"></a>\r\n";
            $admincontent .= "<a href=\"?p=$modulename&action=remove&pid=$id\"><img alt=\"" . _DEL . "\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            $admincontent .= "</tr>\n";
        }
        $admincontent .= "</table>";
    } else {
        createnotification(_NOLJURNAL, _INFO, "info");
    }
    $admincontent .= "<a href=\"?p=$modulename&action=add\" class=\"buton\">" . _ADDJURNAL . "</a>";
}

if ($action == 'search') {
    $admintitle = _SEARCHRESULTS;

	$sql  = "SELECT id, judul, filename, keterangan, link
			 FROM jurnal";
    $result = $mysql->query($sql);
    $total_records = $mysql->num_rows($result);
    $pages = ceil($total_records / $max_page_list);

    if ($total_records > 0) {
        $start = $screen * $max_page_list;
        // $sql = "SELECT id, c.nama FROM jurnal c INNER JOIN car_brand b ON b.id=c.brand_id WHERE (nama LIKE '%$keyword%') ORDER BY nama LIMIT $start, $max_page_list ";
		$sql  = "SELECT id, judul, filename, keterangan, link
					FROM jurnal
					WHERE penulis LIKE '%$keyword%' OR judul_buku LIKE '%$keyword%' OR penerbit LIKE '%$keyword%'
					ORDER BY id
					LIMIT $start, $max_page_list";
        $result = $mysql->query($sql);
        if ($pages > 1) {
            $adminpagination = pagination($namamodul, $screen);
        }
        $admincontent .= "<table class=\"stat-table table table-stats table-striped table-sortable table-bordered\">\n";
        $admincontent .= "<tr><th>" . _PENULIS . "</th><th>" . _JUDULBUKU . "</th><th>" . _PENERBIT . "</th><th>" . JUMLAHHALAMAN . "</th>";
        $admincontent .= "<th align=\"center\">" . _ACTION . "</th></tr>\n";
        while (list($id, $penulis, $jumlah_halaman, $penerbit, $judul_buku) = $mysql->fetch_row($result)) {
			$titleurl = array();
			$titleurl['cat_id'] = $car_brand;
			$url = $urlfunc->makePretty("?p=jurnal&action=list&cat_id=$car_id", $titleurl);
            $admincontent .= "<tr class=\"merek\">\r\n";
            $admincontent .= "<td>$penulis</td>";
            $admincontent .= "<td>$judul_buku</td>\r\n";
            $admincontent .= "<td>$penerbit</td>\r\n";
            $admincontent .= "<td>$jumlah_halaman</td>\r\n";
            $admincontent .= "<td align=\"center\" class=\"action-ud\">";
            $admincontent .= "<a href=\"?p=$modulename&action=modify&pid=$id\"><img alt=\"" . _EDIT . "\" border=\"0\" src=\"../images/modify.gif\"></a>\r\n";
            $admincontent .= "<a href=\"?p=$modulename&action=remove&pid=$id\"><img alt=\"" . _DEL . "\" border=\"0\" src=\"../images/delete.gif\"></a></td>\n";
            $admincontent .= "</tr>\n";
        }
        $admincontent .= "</table>";
    } else {//menampilkan record di halaman yang sesuai
        createnotification(_NOSEARCHRESULTS, _INFO, "info");
    }
    $admincontent .= "<a href=\"?p=$modulename\" class=\"buton\">" . _BACK . "</a>";
}
?>
