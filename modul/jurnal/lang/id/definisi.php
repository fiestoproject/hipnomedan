<?php
define(_NOBRAND,"Tidak ada data literatur");
define(_BRAND,"Mobil");
define(_EDITBRAND,"Edit literatur");
define(_DELBRAND,"Hapus literatur");
define(_ADDBRAND,"Tambah literatur");
define(_ACTION,"Aksi");
define('_CHANGE', 'Change');
define('_REMOVE', 'Remove');
define('_NOLJURNAL', 'Tidak ada Jurnal');
define('_ADDJURNAL', 'Tambah Jurnal');
define('_PENULIS', 'Penulis');
define('_PENERBIT', 'Penerbit');
define('_JUMLAHHALAMAN', 'Jumlah halaman');
define('_STATUS', 'Tampilkan');
define('_JUDULBUKU', 'Judul buku');
define('_JUDUL', 'Judul');
define('_LINK', 'Link asal');
define('_COVER', 'Cover');
define('_KETERANGAN', 'Keterangan');
?>