<?php 

function show_ebooks($result) {
	global $mysql, $cfg_fullsizepics_path, $cfg_thumb_url, $urlfunc, $cfg_file_path;
	
	if ($mysql->num_rows($result) > 0) {
		
		$content = "<table class=\"download_conten table table-striped\">\r\n";
		$content .= "<tr>
			<th style=\"text-align:center !important\">No</th>
			<th width=\"40%\">Judul</th>
			<th>Penulis</th>
			<th width=\"120px\">Ukuran File</th>
			<th>Unduh</th>
			</tr>\r\n";
			$x=1;
		while ($row = $mysql->fetch_assoc($result)) {
			extract($row);
			$filetitle = $title;
			
			$thumbnail = ($thumbnail != '' && file_exists("$cfg_fullsizepics_path/$thumbnail")) ? '<img class="thumbnail" src="'.$cfg_thumb_url.'/'.$thumbnail.'" alt="'.$filetitle.'">' : '';
			
			// if ($filetitle=='') $filetitle=$filename;
			
			$titleurl = array();
			$titleurl["pid"] = $filetitle;
				
			// // $content .= "<li class=\"link_download\">$filetitle (".convertbyte($filesize).") <a href=\"".$urlfunc->makePretty("?p=download&action=go&pid=$pid", $titleurl)."\"><i class=\"fa fa-cloud-download fa-2\"></i>"._DOWNLOAD."</a></li>\r\n";
			// $content .= "<li class=\"xdata\">
				// $thumbnail
				// <div class=\"judul-ebook\"><a href=\"".$urlfunc->makePretty("?p=ebook&action=view&pid=$id", $titleurl)."\">$filetitle</a></div>
				// <div class=\"file-author\">$penulis</div> 
				// ";
			// if ($filename != '' && file_exists("$cfg_file_path/$filename")) {
				// $content .= "<a class=\"btn btn-default more\" href=\"".$urlfunc->makePretty("?p=ebook&action=go&pid=$id", $titleurl)."\"><i class=\"fa fa-download\"></i> Download File</a>";
			// }
			// $content .= "
			// </li>\r\n";
			$content .= "<tr>
			<td align=\"center\">
				";
			$content .=$x++;
			$content .="</td>";
			// $content .="<td><a href=\"".$urlfunc->makePretty("?p=ebook&action=view&pid=$id", $titleurl)."\">$filetitle</a></td>";
			$content .="<td>$filetitle</td>";
			$content .="<td>$penulis</td>
			<td>".number_format($filesize,0,",",".")." kb</td>
			$cat_name
			<td>";
			if ($filename != '' && file_exists("$cfg_file_path/$filename")) {
				$content .= "<a class=\"btn btn-default more\" href=\"".$urlfunc->makePretty("?p=ebook&action=go&pid=$pid", $titleurl)."\"><i class=\"fa fa-download\"></i> Download File</a>";
			}
			$content .= "</td></tr>\r\n";
			
		}
		$content .= "</table>\r\n";
		
		return $content;
	}
	
	return false;
}

function sort_by($urlstatus = '', $sb = '') {
	global $sort_by;
	global $mysql;
	if (isset($_GET['screen']) && $_GET['screen'] != '') {
		$status = $_GET['screen'];
	}
	$data = '<label>'._SORTBY.'</label>';
	$data .= "<select name=\"sortbyorder\" onchange=\"window.location='$urlstatus'+this.value+'/$status'\">";
	foreach($sort_by as $i => $val)  {
		$selected = ($sb == $i) ? 'selected' : '';
		$data .= '<option value="'.$i.'" '.$selected.'>'.$val.'</option>';
	}
	$data .= '</select>';	
	return $data;
}