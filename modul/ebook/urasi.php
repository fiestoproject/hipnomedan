<?php
$cfg_file_url = "$cfg_img_url/ebook";
$cfg_file_path = "$cfg_img_path/ebook";
$cfg_fullsizepics_path = "$cfg_img_path/ebook/large";
$cfg_fullsizepics_url = "$cfg_img_url/ebook/large";
$cfg_thumb_path = "$cfg_img_path/ebook/small";
$cfg_thumb_url = "$cfg_img_url/ebook/small";

// 1e+8 100MB
$maxfilesize = 1e+8;		//Max file size allowed to be uploaded
$allowedfiletypes = "pdf,epub,mobi,zip,png,jpg";
$cfg_thumb_width = 200;

//Cantumkan nama table yang langsung terkait dengan modul ini, penamaan wajib namamodul'_drop'.
$file_drop = array('filecat', 'filedata');
$thisfile="http://".param_url();

$sort_by = array('title ASC' => _TITLEASC, 'title DESC' => _TITLEDESC/* , 'hargadiskon ASC' => _PRICEASC, 'hargadiskon DESC' => _PRICEDESC/* , 'diskon ASC' => _DISCONTASC, 'diskon DESC' => _DISCONTDESC */);

ob_start();
if(!$admin) {
	//css untuk admin
	echo <<<END
	<link type="text/css" href="$cfg_app_url/modul/ebook/css/basic.css" rel="stylesheet">
END;
}
$style_css['ebook']=ob_get_clean();

?>