<?php
$sort = "title";
$action = fiestolaundry($_GET['action'],20);
$pid = fiestolaundry($_GET['pid'],11);

$screen = fiestolaundry($_GET['screen'], 20);
$keyword = fiestolaundry($_GET['keyword'], 20);
// $sb = fiestolaundry($_GET['sb'],20);
$sb = $_SEG[2];

$cfg_per_page = 30;

$tempid = explode('_',$_SEG[2]);
$pid = $tempid[0];
$sql = "SELECT judulfrontend FROM module WHERE nama='ebook'";
$result = $mysql->query($sql);
list($title) = $mysql->fetch_row($result);


if (!$_SESSION['member_uid']) {
	// $content .= '<div class="alert alert-danger text-center">'._NORIGHT.'</div>';
	$content .= '<div class="alert alert-danger text-center">Anda harus <a href="'.$urlfunc->makePretty("?p=webmember&action=login").'">login</a> terlebih dahulu untuk bisa mengakses halaman ini. </div>';
} else {

	if ($action=='go') {
		$sql = "SELECT filename FROM filedata WHERE id='$pid'";
		$result = $mysql->query($sql);
		list($filename) = $mysql->fetch_row($result);
		$mysql->query("UPDATE filedata SET clickctr=clickctr+1 WHERE id='$pid'");
		header("Location: $cfg_file_url/$filename");
		exit();
	} else {
		
		// $content .= '
		// <form method="GET" action="'.$urlfunc->makePretty("?p=ebook&action=search").'">
			// <div class="ebook-search">
				// <input type="text" name="keyword">
				// <button value="search">Cari</button>
			// </div>
		// </form>';
		// $sql = "SELECT id,nama FROM filecat ORDER BY urutan";
		// $result = $mysql->query($sql);
		// while (list($cat_id,$cat_name) = $mysql->fetch_row($result)) {
			// $content .= "<h2>$cat_name</h2>\r\n";
			// $sql1 = "SELECT id, filename, filesize, title, thumbnail, penulis FROM filedata WHERE cat_id='$cat_id' ORDER BY $sort";
			// $result1 = $mysql->query($sql1);
			// $content .= "<ul class=\"download_conten\">\r\n";
			// while (list($pid, $filename, $filesize, $filetitle, $thumb, $penulis) = $mysql->fetch_row($result1)) {
				// $thumbnail = ($thumb != '' && file_exists("$cfg_fullsizepics_path/$thumb")) ? '<img class="thumbnail" src="'.$cfg_thumb_url.'/'.$thumb.'" alt="'.$filetitle.'">' : '';
				
				// if ($filetitle=='') $filetitle=$filename;
				
				// $titleurl = array();
				// $titleurl["pid"] = $filetitle;
					
				// // $content .= "<li class=\"link_download\">$filetitle (".convertbyte($filesize).") <a href=\"".$urlfunc->makePretty("?p=download&action=go&pid=$pid", $titleurl)."\"><i class=\"fa fa-cloud-download fa-2\"></i>"._DOWNLOAD."</a></li>\r\n";
				// $content .= "<li class=\"xdata\">$thumbnail<a href=\"".$urlfunc->makePretty("?p=download&action=go&pid=$pid", $titleurl)."\"><i class=\"fa fa-download\"></i> $filetitle</a><div class=\"file-author\">$penulis</div> <span class=\"filesize\">(".convertbyte($filesize).")</span></li>\r\n";
			// }
			// $content .= "</ul>\r\n";
		// }
	}

		

	if ($action == '' || $action == 'list') {
		
		// print_r($_GET);

		$titleurl = array();
		$titleurl['cat_id'] = $cat_name;
		$urlstatus = $urlfunc->makePretty("?p=ebook&action=list&pic=&sb=", $titleurl);
		$sortbyorder = sort_by($urlstatus, $sb);
		$content .= '
		<div id="filter-ebook">';
		$content .= ( $sortbyorder != '') ? "<div class=\"sortbyorder\">$sortbyorder</div>\r\n" : "";
		$content .= '	<form method="GET" action="'.$urlfunc->makePretty("?p=ebook&action=search").'">
				<div class="ebook-search">
					<input type="text" name="keyword" placeholder="Cari ebook">
					<button value="search" class="btn btn-default more">Cari</button>
				</div>
			</form>
		';
		$content .= "</div>";
		$content .= ( $catpage != '') ? "<div class=\"catpage\">$catpage</div>\r\n" : "";
		
		
		$sql = "SELECT id,nama FROM filecat ORDER BY urutan";
		$result = $mysql->query($sql);
		$filecats = array();
		$content .= "<ul class=\"nav nav-tabs ebook-tabs\" id=\"myTab\" role=\"tablist\">";
		$i = 0;
		// ambil nama kategori
		while (list($cat_id,$cat_name) = $mysql->fetch_row($result)) {
			
			// simpan attribut di array
			$filecats[] = array('id' => $cat_id, 'name' => $cat_name);
			
			$selected = ($i == 0) ? 'active' : '';
			$content .= "
			  <li class=\"nav-item $selected\">
				<a class=\"nav-link\" id=\"contact-tab\" data-toggle=\"tab\" href=\"#ebook$cat_id\" role=\"tab\" >$cat_name</a>
			  </li>";
			  
			$i++;
		}
		$content .= "</ul>";
		
		// looping atribut disini
		if (count($filecats) > 0) {
			
			// isi kategori ambil disini
			$content .= "<div class=\"tab-content\" id=\"myTabContent\">";
			foreach($filecats as $idx => $val) {
				
				$cat_id = $val['id'];
				
				// ORDER BY $sort
				$sql = "SELECT id, filename, filesize, title, thumbnail, penulis FROM filedata WHERE cat_id='$cat_id' ";
				$temp_sort_by = array_keys($sort_by);
				$sql .= ' ORDER BY ';
				$sb = str_replace('%20',' ', $sb);
				if (isset($sb) && $sb != '') {
					$sql .= " $sb ";
				} else {
					$sql .= " {$temp_sort_by[0]} ";
				}
				$result = $mysql->query($sql);
				
				$selected = ($idx == 0) ? 'active' : '';
				$content .= "<div class=\"tab-pane $selected\" id=\"ebook$cat_id\" role=\"tabpanel\" aria-labelledby=\"contact-tab\">";
				$content .= "<div class=\"table-responsive\">";
				$content .= "<table class=\"download_conten table table-striped\">\r\n";
				$content .= "
				<tr>
					<th style=\"text-align:center !important\">No</th>
					<th width=\"40%\">Judul</th>
					<th>Penulis</th>
					<th width=\"120px\">Ukuran File</th>
					<th>Unduh</th>
				</tr>";
				
				$x=1;
				while (list($pid, $filename, $filesize, $filetitle, $thumb, $penulis) = $mysql->fetch_row($result)) {
					$thumbnail = ($thumb != '' && file_exists("$cfg_fullsizepics_path/$thumb")) ? '<img class="thumbnail" src="'.$cfg_thumb_url.'/'.$thumb.'" alt="'.$filetitle.'">' : '';
					
					if ($filetitle=='') $filetitle=$filename;
					
					$titleurl = array();
					$titleurl["pid"] = $filetitle;
						
					// $content .= "<li class=\"link_download\">$filetitle (".convertbyte($filesize).") <a href=\"".$urlfunc->makePretty("?p=download&action=go&pid=$pid", $titleurl)."\"><i class=\"fa fa-cloud-download fa-2\"></i>"._DOWNLOAD."</a></li>\r\n";
					$content .= "<tr><td align=\"center\">";
					$content .= $x++;
					$content .= "</td>";
					// $content .= "<td><a href=\"".$urlfunc->makePretty("?p=ebook&action=view&pid=$pid", $titleurl)."\">$filetitle</a></td>";
					$content .= "<td>$filetitle</td>";
					$content .= "<td>$penulis</td>
					<td>".convertbyte($filesize)."</td>
					<td>";
					if ($filename != '' && file_exists("$cfg_file_path/$filename")) {
						$content .= "<a class=\"btn btn-default more\" href=\"".$urlfunc->makePretty("?p=ebook&action=go&pid=$pid", $titleurl)."\"><i class=\"fa fa-download\"></i> Download File</a>";
					}
					$content .= "</td></tr>\r\n";
				}
				
				$content .= "</table>	<!-- .download_conten -->";
				$content .= "</div>	<!-- .table-responsive -->";
				$content .= "</div>	<!-- .tab-pane -->";
			}
			$content .= "</div>	<!-- #myTabContent -->";
		}
		
	}

	if ($action == 'search') {
		
		if ($keyword != '') {
			$keyword = strip_tags($keyword);
			$r_keyword = preg_split("/[\s,]+/",$keyword);
			
			// print_r($r_keyword);
			
			$title = _SEARCHRESULTS." : ".$keyword;
			
			$sql  = "SELECT id FROM filedata WHERE (1=1";
			foreach ($r_keyword as $splitkeyword) {
				$sql .= " AND title LIKE '%$splitkeyword%'";
			}
			$sql .= ")";
			
			$sql .= " OR (1=1";
			foreach ($r_keyword as $splitkeyword) {
				$sql .= " AND penulis LIKE '%$splitkeyword%'";
			}
			$sql .= ")";
			
			$result = $mysql->query($sql);
			$total_records = $mysql->num_rows($result);
			$pages = ceil($total_records / $cfg_per_page);

			if ($screen == '') {
				$screen = 0;
			}

			$start = $screen * $cfg_per_page;
			
			$sql = "SELECT id, filename, filesize, title, thumbnail, penulis FROM filedata WHERE (1=1";
			foreach ($r_keyword as $splitkeyword) {
				$sql .= " AND title LIKE '%$splitkeyword%'";
			}
			$sql .= ")";
			
			$sql .= " OR (1=1";
			foreach ($r_keyword as $splitkeyword) {
				$sql .= " AND penulis LIKE '%$splitkeyword%'";
			}
			$sql .= ")";
			
			$sql .= " ORDER BY $sort LIMIT $start, $cfg_per_page";
			$result = $mysql->query($sql);
			$total_images = $mysql->num_rows($result);
			if ($total_images > 0) {
				$keyword = urlencode($keyword);
				if ($pages > 1)
					$catpage = aksipagination($namamodul, $screen, "action=search", "?keyword=$keyword");
				$content .= show_ebooks($result);
			} else {
				$content .= _NOSEARCHRESULT;
			}
		}

	}

	if ($action == 'view') {
		
		$sql = "SELECT id, filename, filesize, title, thumbnail, penulis, keterangan FROM filedata WHERE id='$pid'";
		$result = $mysql->query($sql);
		if ($mysql->num_rows($result) > 0) {
			$row = $mysql->fetch_assoc($result);
			extract($row);
			
			$thumbnail = ($thumbnail != '' && file_exists("$cfg_fullsizepics_path/$thumbnail")) ? "$cfg_fullsizepics_url/$thumbnail" : "";
			$content .= "<div id=\"ebook-detail\">";
			$content .= "<a class=\"btn btn-default more\" href=\"".$urlfunc->makePretty("?p=ebook&action=go&pid=$pid", $titleurl)."\"><i class=\"fa fa-download\"></i> "._DOWNLOADFILE."</a>";
			$content .= "<div class=\"images-detail-ebook\"><img src=\"".$thumbnail."\"></div>";
			// $content .= convertbyte($filesize);
			$content .= "<style>#filter-ebook{display:none}</style>";
			$content .= "<div class=\"penulis-ebook\">".$penulis."</div>";
			$content .= "<div class=\"keterangan-ebook\">".$keterangan."</div>";
			$content .= "</div>";
			
			
		}
	}
}

// $content .= ( $sortbyorder != '') ? "<div class=\"sortbyorder\">$sortbyorder</div>\r\n" : "";
$content .= ( $catpage != '') ? "<div class=\"catpage\">$catpage</div>\r\n" : "";