<?php

if($jenismenu=="cattree")
{	
	$sql_catalog = "SELECT id, nama, parent FROM packagecat ORDER BY urutan ";
	$result_catalog = $mysql->query($sql_catalog);
	while($row_catalog = $mysql->fetch_assoc($result_catalog)) 
	{	$titleurl = array();
		$titleurl["cat_id"] = $row_catalog['nama'];
		$url = "";
		
		$sqlchild = "SELECT id, nama, parent FROM packagecat 
			WHERE parent='".$row_catalog['id']."' ";
		$resultchild = $mysql->query($sqlchild);
		if($mysql->num_rows($resultchild)==0)
		{	$url = "?p=package&action=images&cat_id=".$row_catalog['id'];
		}
	
		if(strlen($url)>0)
		{	$url = $urlfunc->makePretty($url, $titleurl);
		}
		
		if($row_catalog['parent']==0)
		{	$row_catalog['parent'] = $row['id'];
		}
		else
		{	$row_catalog['parent'] = $row['id']."-".$row_catalog['parent'];
		}
		$row_catalog['id'] = $row['id']."-".$row_catalog['id'];
		
		$mycats[] = array('id'=>$row_catalog['id'],
			'parent'=>$row_catalog['parent'], 'type'=>$row['type'], 
			'judul'=>$row_catalog['nama'], 'url'=>$url, 'level'=>0);
	}
	
}

if($jenismenu=="producttree")
{	
	$sql_catalog = "SELECT id, nama, parent FROM packagecat ORDER BY urutan ";
	$result_catalog = $mysql->query($sql_catalog);
	while($row_catalog = $mysql->fetch_assoc($result_catalog)) 
	{	$titleurl = array();
		$titleurl["cat_id"] = $row_catalog['nama'];
		$url = "";
		
		$sqlchild = "SELECT id, nama, parent FROM packagecat 
			WHERE parent='".$row_catalog['id']."' ";
		$resultchild = $mysql->query($sqlchild);
		
		$sqlproduct = "SELECT id, title, cat_id FROM packagedata 
			WHERE cat_id='".$row_catalog['id']."' AND publish='1' 
			ORDER BY title ";

		$resultproduct = $mysql->query($sqlproduct);
		if($mysql->num_rows($resultchild)==0 and $mysql->num_rows($resultproduct)==0)
		{	$url = "?p=package&action=images&cat_id=".$row_catalog['id'];
		}
	
		if(strlen($url)>0)
		{	$url = $urlfunc->makePretty($url, $titleurl);
		}
		
		if($row_catalog['parent']==0)
		{	$row_catalog['parent'] = $row['id'];
		}
		else
		{	$row_catalog['parent'] = $row['id']."-".$row_catalog['parent'];
		}
		$row_catalog['id'] = $row['id']."-".$row_catalog['id'];
		
		$mycats[] = array('id'=>$row_catalog['id'],
			'parent'=>$row_catalog['parent'], 'type'=>$row['type'], 
			'judul'=>$row_catalog['nama'], 'url'=>$url, 'level'=>0);
		
		if($mysql->num_rows($resultproduct)>0)
		{	while($row_product = $mysql->fetch_assoc($resultproduct)) 
			{	$titleurl = array();
				$titleurl["cat_id"] = $row_catalog['nama'];
				$titleurl["pid"] = $row_product['title'];
				$url = "?p=package&action=detail&pid=".$row_product['id'];
				$url .= "&cat_id=".$row_product['cat_id'];
				if(strlen($url)>0)
				{	$url = $urlfunc->makePretty($url, $titleurl);
				}
		
				$row_product['id'] = $row_catalog['id']."-".$row_product['id'];
				$mycats[] = array('id'=>$row_product['id'],
					'parent'=>$row_catalog['id'], 'type'=>$row['type'], 
					'judul'=>$row_product['title'], 'url'=>$url, 'level'=>0);
			}
		}
		
	}
	
}

if($jenismenu=="single")
{	
	list($menucatid)=explode(';',$row['isi']);
	// echo "ha$menucatid-";
	
	$sql = "SELECT id, judulberita FROM newsdata WHERE cat_id = '$menucatid'";
	
	$result_package = $mysql->query($sql);
	while($row_package = $mysql->fetch_assoc($result_package)) 
	{	
		$titleurl = array();
		$titleurl["pid"] = $row_package['judulberita'];
		$url = "";
		
		$url = "?p=news&action=shownews&pid=".$row_package['id'];
		
		if(strlen($url)>0)
		{	$url = $urlfunc->makePretty($url, $titleurl);
		}		
		
		$mycats[] = array('id'=>$row_package['id'],
			'parent'=> $row['id'], 'type'=>$row['type'], 
			'judul'=>$row_package['judulberita'], 'url'=>$url, 'level'=>0);		
	}	
}

if($jenismenu=="singletree")
{	
	list($menucatid)=explode(';',$row['isi']);
	// echo "ha$menucatid-";
	
	$sql = "SELECT id, nama FROM newscat WHERE id = '$menucatid'";
	
	$result_package = $mysql->query($sql);
	while($row_package = $mysql->fetch_assoc($result_package)) 
	{	$titleurl = array();
		$titleurl["cat_id"] = $row_package['nama'];
		$url = "";
		
		$sqlchild = "SELECT id, nama FROM newscat 
			WHERE id='".$row_package['id']."' ";
		$resultchild = $mysql->query($sqlchild);
		
		$sqlproduct = "SELECT id, judulberita, cat_id FROM newsdata 
			WHERE cat_id='".$row_package['id']."' AND publish='1' 
			ORDER BY judulberita ";
		
		$resultproduct = $mysql->query($sqlproduct);
		if($mysql->num_rows($resultchild)==0 and $mysql->num_rows($resultproduct)==0)
		{	
			// $url = "?p=news&action=list&cat_id=".$row_package['id'];
			$url = "?p=news&action=shownews&pid=".$row_package['id'];
		}
		
		// $url = "?p=package&action=list&cat_id=".$row_package['id'];
		if(strlen($url)>0)
		{	$url = $urlfunc->makePretty($url, $titleurl);
		}		
		
		if($row_package['parent']==0)
		{	$row_package['parent'] = $row['id'];
		}
		else
		{	$row_package['parent'] = $row['id']."-".$row_package['parent'];
		}
		$row_package['id'] = $row['id']."-".$row_package['id'];		
		
		$mycats[] = array('id'=>$row_package['id'],
			'parent'=> $row['id'], 'type'=>$row['type'], 
			'judul'=>$row_package['nama'], 'url'=>$url, 'level'=>0);
			
		if($mysql->num_rows($resultproduct)>0)
		{	while($row_product = $mysql->fetch_assoc($resultproduct)) 
			{	$titleurl = array();
				$titleurl["cat_id"] = $row_package['nama'];
				$titleurl["pid"] = $row_product['judulberita'];
				// $url = "?p=package&action=detail&pid=".$row_product['id'];
				$url = "?p=news&action=shownews&pid=".$row_package['id'];
				$url .= "&cat_id=".$row_product['cat_id'];
				if(strlen($url)>0)
				{	$url = $urlfunc->makePretty($url, $titleurl);
				}
		
				$row_product['id'] = $row_package['id']."-".$row_product['id'];
				$mycats[] = array('id'=>$row_product['id'],
					'parent'=>$row_package['id'], 'type'=>$row['type'], 
					'judul'=>$row_product['judulberita'], 'url'=>$url, 'level'=>0);
			}
		}				
	}	
}
?>
