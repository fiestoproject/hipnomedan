<?php

function archive($tahun,$bulan,$cat_id) {
	global $namabulan, $isdateshown, $issourceshown, $issummary, $lang, $urlfunc, $idmenu;
	global $cfg_thumb_path, $cfg_thumb_url;
	$bulandepan=$bulan+1;

	if ($cat_id=='') {
		$sql = "SELECT id,tglmuat,judulberita,summary,tglberita, thumb FROM newsdata WHERE tglberita >= '$tahun-$bulan-01 00:00:00' AND tglberita < '$tahun-$bulandepan-01 00:00:00' ORDER BY tglberita DESC";
	} else {
		$sql = "SELECT id,tglmuat,judulberita,summary,tglberita, thumb FROM newsdata WHERE tglberita >= '$tahun-$bulan-01 00:00:00' AND tglberita < '$tahun-$bulandepan-01 00:00:00' AND cat_id='$cat_id' ORDER BY tglberita DESC";
	}
	$result = $mysql->query($sql);
	if ($mysql->num_rows($result)>0) {
		 $newscontent = "<div class=\"row masonry-container\">";
		while (list($id, $tglmuat, $judulberita, $summary, $tglberita, $thumb) = $mysql->fetch_row($result)) {
			$titleurl = array();
			$titleurl["pid"] = $judulberita;
			$thumbs = explode(':',$thumb);
			
			$newscontent .= "	
					<div class=\"col-sm-6 col-md-6 item\" itemscope itemtype=\"http://schema.org/NewsArticle\">
						<div class=\"panel panel-default news-col\">
							<div class=\"panel-body\">
								<div class=\"row\">";
								if ($thumbs[0] != '' && file_exists("$cfg_thumb_path/$thumbs[0]")) {
									$newscontent .= "
									<div class=\"col-sm-12 img-news\">
										<img class=\"img-responsive\" alt=\"$judulberita\" src=\"$cfg_thumb_url/$thumbs[0]\">
									</div>	<!-- /.img-news -->\r\n";
								}
								$newscontent .= "
									<div class=\"col-sm-12 news-content\">
										<div class=\"title-news-thumbnail\" itemprop=\"name\"><h4>$judulberita</h4></div>";
								if ($isdateshown) $newscontent .= "<div class=\"date_news\"><meta itemprop=\"datePublished\" content=\"$tglberita\"/>".tglformat($tglberita)."</div>";
								if ($issummary && $summary!='') $newscontent .= "<div class=\"content-news-thumbnail\" temprop=\"description\">$summary</div>";
								$newscontent .= "<a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\" class=\"btn btn-default more\">"._LEARNMORE."</a>
									</div>	<!-- /.news-content -->\r\n
								";
								$newscontent .= "
								</div>	<!-- /.rows -->\r\n
							</div>	<!-- /.panel-body -->\r\n
						</div>	<!-- /.news-col -->\r\n 
					</div>	<!-- /.col-sm-6 col-md-6 -->";				
			// $newscontent .= "<li><span class=\"newstitle\"><a href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\">$judulberita</a></span>\r\n";
			// if ($isdateshown) $newscontent .= "<br /><span class=\"newsdate\">".tglformat($tglberita)."</span>\r\n";
			// if ($issummary && $summary!='') $newscontent .= "<br /><div class=\"newsshortdesc\">$summary</span>\r\n";
			// $newscontent .= "</li>";
		}
		$newscontent .= "</div>";
	} else {
		$newscontent = _NONEWS;
	}
	return $newscontent;
}

function catstructure($sql)
{
	global $urlfunc;
	$cats = new categories();
	$mycats = array();
	$result = $mysql->query($sql);
	while ($row = $mysql->fetch_assoc($result)) {
	$mycats[] = array('id' => $row['id'], 'nama' => $row['nama'],'parent' => 0,'level' => 0);
	}
	$cats->get_cats($mycats);
	ob_start();
	echo "<ol class=\"sortable ui-sortable\">";
	 for ($i = 0; $i < count($cats->cats); $i++) 
	 {
		if($cats->cats[$i]['parent']==0)
		{
			$cat_id=$cats->cats[$i]['id'];
			echo "<li id='list_$cat_id' data-catid='$cat_id'>";
			echo "<div class=\"kat_item ui-state-default ui-draggable\"><div class=\"kat_nama\">".$cats->cats[$i]['nama']."</div>";
			   echo "<div class=\"kat_action\"><a href=\"?p=news&action=catedit&cat_id=".$cats->cats[$i]['id']."\"><img alt=\"Edit\" border=\"0\" src=\"../images/modify.gif\"></a> <a href=\"?p=news&action=catdel&cat_id=" . $cats->cats[$i]['id'] . "\"><img alt=\"Hapus\" border=\"0\" src=\"../images/delete.gif\"></a>
				</div></div>";
			echo "</li>";
		}
	 }
	echo "</ol>";

	return ob_get_clean();
}

/* By Aly */

/*
return JSON Schema Single Product
*/
function generate_schema_single_news($sql) {
	global $mysqli, $cfg_fullsizepics_path, $cfg_fullsizepics_url, $urlfunc, $schema_person_organization, $cfg_app_url;
	
	define('SCHEMA_HEADER', '<script type="application/ld+json">');
	define('SCHEMA_FOOTER', '</script>');
	define('SCHEMA_CONTEXT', 'http://schema.org');
	
	$markup = array();
	if ($result = $mysqli->query($sql)) {
		$total_record = $result->num_rows;
		
		$row = $result->fetch_assoc();
		
		$pid = $row['id'];
		
		$markup = array(
			'@context' 	=> SCHEMA_CONTEXT,
			'@type' 	=> 'NewsArticle'
		);
		
		$titleurl = array();
		$titleurl['pid'] = $row['judulberita'];
		// $titleurl['cat_id'] = get_cat_name($row['cat_id']);
		$url = $urlfunc->makePretty("?p=news&action=shownews&pid=$pid", $titleurl);
		
		$markup_main_entity_of_page = array(
			'@type' => 'WebPage',
			'@id' => $url
		);
		$markup['mainEntityOfPage'] = $markup_main_entity_of_page;
		$markup['headline'] = $row['judulberita'];
		
		if ($row['thumb'] != '' && file_exists("$cfg_fullsizepics_path/{$row['thumb']}")) {
			$image_url = "$cfg_fullsizepics_url/{$row['thumb']}";
			$markup_images = array(
				'@type' 	=> 'ImageObject',
				'url' 		=> $image_url,
				'width'		=> 600,
				'height' 	=> 600
			);
			
			$markup['image'] = $markup_images;
		}
		
		$markup['datePublished'] = $row['tglberita'];
		$markup['dateModified'] = $row['tglmuat'];
		
		$markup_authors = array(
			'@type'	=> 'Organization',
			'name'	=> $schema_person_organization
		);
		$markup['author'] = $markup_authors;
		
		$sql_logo = "SELECT basename, extension FROM decoration WHERE basename='headerlogo'";
		if ($result_logo = $mysqli->query($sql_logo)) {
			list($basename, $extension) = $result_logo->fetch_row();
			$image_logo = "$cfg_app_url/file/dekorasi/$basename.$extension";
			$result_logo->close();
		}
		
		$markup_publisher_type = array(
			'@type'	=> 'ImageObject',
			'url'	=> $image_logo,
			'width'	=> 600,
			'height'	=> 60
		);
		
		$markup_publisher = array(
			'@type'	=> 'Organization',
			'name'	=> $schema_person_organization,
			'logo'	=> $markup_publisher_type
		);
		$markup['publisher'] = $markup_publisher;
		$markup['description'] = $row['isiberita'];
		
	}
	
	$response = json_encode($markup);
	return SCHEMA_HEADER.$response.SCHEMA_FOOTER;
}
?>