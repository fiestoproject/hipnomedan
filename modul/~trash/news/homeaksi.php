<?php
if ($jenis=='category') {
	list($homecatid,$homemaxnews)=explode(';',$isi);
	if ($homemaxnews=='') $homemaxnews=5;
	$sql = "SELECT id, cat_id, tglmuat, judulberita, summary, isiberita, tglberita, ishot, thumb FROM newsdata WHERE publish='1'";
	if ($homecatid>0) $sql .= " AND cat_id='$homecatid'";
	$sql .= " ORDER BY tglberita DESC LIMIT $homemaxnews";
	$result = $mysql->query($sql);
	if ($result and $mysql->num_rows($result)>0) {
		$x = 1;
		$home .= "<div class=\"col-lg-12 block-list-news\"><div class=\"row\">";
		// $home .= "<ul class=\"newshome\"  itemscope itemtype=\"http://schema.org/NewsArticle\">\r\n";'
		while (list($id, $cat_id, $tglmuat, $judulberita, $summary, $isiberita, $tglberita, $ishot, $thumb) = $mysql->fetch_row($result)) {
			$titleurl = array();
			$titleurl["pid"] = $judulberita;
			$home .= "					
					<div class=\"col-sm-6 col-md-6\" itemscope itemtype=\"http://schema.org/NewsArticle\">
						<div class=\"panel panel-default news-col\">
							<div class=\"panel-body\">
								<div class=\"row\">";
								if ($thumb != '' && file_exists("$cfg_thumb_path/$thumb")) {
									$home .= "
									<div class=\"col-sm-12 img-news\">
										<img class=\"img-responsive\" alt=\"$judulberita\" src=\"$cfg_thumb_url/$thumb\">
									</div>	<!-- /.img-news -->\r\n";
									$colsm = "12";
								} else {
									$colsm = "12";
								}
								$home .= "
									<div class=\"col-sm-$colsm news-content\">
										<div class=\"title-news-thumbnail\" itemprop=\"name\"><h4>$judulberita</h4></div>";
								if ($isdateshown) $home .= "<div class=\"date_news\"><meta itemprop=\"datePublished\" content=\"$tglberita\"/>".tglformat($tglberita)."</div>";
								if ($issummary && $summary!='') $home .= "<div class=\"content-news-thumbnail\" temprop=\"description\">$summary</div>";
								$home .= "<a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\" class=\"btn btn-default more\">"._LEARNMORE."</a>
									</div>	<!-- /.news-content -->\r\n
								";
								$home .= "
								</div>	<!-- /.rows -->\r\n
							</div>	<!-- /.panel-body -->\r\n
						</div>	<!-- /.news-col -->\r\n 
					</div>	<!-- /.col-sm-6 col-md-6 -->";
			// $home .= "<li><span class=\"newstitle\" itemprop=\"name\"><a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\">$judulberita</a></span>\r\n";
			// if ($isdateshown) $home .= "<br /><span class=\"newsdate\"><meta itemprop=\"datePublished\" content=\"$tglberita\">".tglformat($tglberita)."</span>\r\n";
			// if ($issummary && $summary!='') $home .= "<br /><div class=\"newsshortdesc\" temprop=\"description\">$summary</div>\r\n";
			// $home .= "</li>\r\n";
			
		}
		// $home .= "</ul>\r\n";
		$home .= "</div></div>";
	} else {
		$home .= "<p>"._NONEWS."</p>";
	}
}

if ($jenis=='newest') {
	list($homemaxnews)=explode(';',$isi);
	if ($homemaxnews=='') $homemaxnews=5;
	$sql = "SELECT id, cat_id, tglmuat, judulberita, summary, isiberita, tglberita, ishot, thumb FROM newsdata WHERE publish='1'";
	// if ($homecatid>0) $sql .= " AND cat_id='$homecatid'";
	$sql .= " ORDER BY tglberita DESC LIMIT $homemaxnews";
	$result = $mysql->query($sql);
	if ($result and $mysql->num_rows($result)>0) {
		$x = 1;
		$home .= "<div class=\"col-lg-12 block-list-news\"><div class=\"row masonry-container\">";
		// $home .= "<ul class=\"newshome\"  itemscope itemtype=\"http://schema.org/NewsArticle\">\r\n";'
		while (list($id, $cat_id, $tglmuat, $judulberita, $summary, $isiberita, $tglberita, $ishot, $thumb) = $mysql->fetch_row($result)) {
			$titleurl = array();
			$titleurl["pid"] = $judulberita;
			
			$home .= "					
					<div class=\"col-sm-6 col-md-6 item\" itemscope itemtype=\"http://schema.org/NewsArticle\">
						<div class=\"panel panel-default news-col\">
							<div class=\"panel-body\">
								<div class=\"row\">";
								$thumbs = explode(':',$thumb);
								$home .= "
									<div class=\"col-sm-12 img-news\">";
								if (count($thumbs) > 1) {
									// $home .= "<div id=\"myCarousel\" class=\"carousel page slide\" data-ride=\"carousel\">";
									// $home .= "<!-- Indicators -->
													// <ol class=\"carousel-indicators\">";
														// for($i = 0; $i < count($thumbs); $i++) {
															// $active = ($i == 0) ? "active" : "";
															// $content .= "<li data-target=\"#myCarousel\" data-slide-to=\"$i\" class=\"$active\"></li>";
														// }
									// $home .= "</ol>";
									// $i = 0;
									// $home .= "
												// <div class=\"carousel-inner\">";
													// foreach($thumbs as $file) {
														// $active = ($i == 0) ? "active" : "";
														// $home .= "<div class=\"item $active\">";
														// $home .= "<img class=\"img-responsive\" alt=\"$judulberita\" src=\"$cfg_thumb_url/$file\">";
														// $home .= "</div>";
														// $i++;
													// }
									// $home .= "</div>";
									// $home .= "
												// <a class=\"left carousel-control\" href=\"#myCarousel\" data-slide=\"prev\"><span class=\"glyphicon glyphicon-chevron-left\"></span></a>
												// <a class=\"right carousel-control\" href=\"#myCarousel\" data-slide=\"next\"><span class=\"glyphicon glyphicon-chevron-right\"></span></a>";
									// $home .= "</div><hr>";
									if ($thumbs[0] != '' && file_exists("$cfg_thumb_path/$thumbs[0]")) {
										$home .= "<img class=\"img-responsive\" alt=\"$judulberita\" src=\"$cfg_thumb_url/$thumbs[0]\">";
									}									
								} else {
									if ($thumb != '' && file_exists("$cfg_thumb_path/$thumb")) {
										$home .= "<img class=\"img-responsive\" alt=\"$judulberita\" src=\"$cfg_thumb_url/$thumb\">";
									}
									
								}
								$home .= "</div>";
								
								// if ($thumb != '' && file_exists("$cfg_thumb_path/$thumb")) {
									// $home .= "
									// <div class=\"col-sm-12 img-news\">
										// <!--<img class=\"img-responsive\" alt=\"$judulberita\" src=\"$cfg_thumb_url/$thumb\">-->
										// $file_thumb
									// </div>	<!-- /.img-news -->\r\n";
									// $colsm = "12";
								// } else {
									// $colsm = "12";
								// }
								$colsm = "12";
								$home .= "
									<div class=\"col-sm-$colsm news-content\">
										<div class=\"title-news-thumbnail\" itemprop=\"name\"><a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\"><h4>$judulberita</h4></a></div>";
								if ($isdateshown) $home .= "<div class=\"date_news\"><meta itemprop=\"datePublished\" content=\"$tglberita\"/>".tglformat($tglberita)."</div>";
								if ($issummary && $summary!='') $home .= "<div class=\"content-news-thumbnail\" temprop=\"description\">$summary</div>";
								$home .= "<a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\" class=\"btn btn-default more\">"._LEARNMORE."</a>
									</div>	<!-- /.news-content -->\r\n
								";
								$home .= "
								</div>	<!-- /.rows -->\r\n
							</div>	<!-- /.panel-body -->\r\n
						</div>	<!-- /.news-col -->\r\n 
					</div>	<!-- /.col-sm-6 col-md-6 -->";
			// $home .= "<li><span class=\"newstitle\" itemprop=\"name\"><a itemprop=\"url\" href=\"".$urlfunc->makePretty("?p=news&action=shownews&pid=$id", $titleurl)."\">$judulberita</a></span>\r\n";
			// if ($isdateshown) $home .= "<br /><span class=\"newsdate\"><meta itemprop=\"datePublished\" content=\"$tglberita\">".tglformat($tglberita)."</span>\r\n";
			// if ($issummary && $summary!='') $home .= "<br /><div class=\"newsshortdesc\" temprop=\"description\">$summary</div>\r\n";
			// $home .= "</li>\r\n";
			
		}
		// $home .= "</ul>\r\n";
		$home .= "</div></div>";
	} else {
		$home .= "<p>"._NONEWS."</p>";
	}
}
?>
