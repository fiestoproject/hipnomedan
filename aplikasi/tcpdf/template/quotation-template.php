<?php 

require_once 'kelola/fungsi.php';

// Include the main TCPDF library (search for installation path).
require_once('aplikasi/tcpdf/config/tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('TCPDF Example 001');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$terbilang = bilangan(10822000);
$html = <<<EOD
<html>
    <head>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <title></title>
    </head>
    <body>
        <form action="http://localhost/iwd/berca/cart/order?type=q" method="post" id="checkout-form" class="checkout-form"
        novalidate="novalidate">
        <h3>e-QUOTATION</h3>
        <div class="table_block table-responsive table-small-gap">
			<table>
				<tr valign="top">
					<td>To</td>
					<td>:</td>
					<td>
						Company #1<br>
						Jl. Belitung Kecamatan Bojong Gede Sebelah sungai ciliwung
						no 10 didepan rumah makan<br>
						Phone: +62 813321802312
					</td>
				</tr>
				<tr>
					<td>Date</td>
					<td>:</td>
					<td>October, 29 2017</td>
				</tr>
				<tr>
					<td>Number</td>
					<td>:</td>
					<td>DE17120006</td>
				</tr>
				<tr>
					<td>Prepared by</td>
					<td>:</td>
					<td>feby</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>:</td>
					<td>bagonggede@yahoo.com</td>
				</tr>
				<tr valign="top">
					<td>Attn</td>
					<td>:</td>
					<td>
						Attn no<br>
						feby@fa.itb.ac.id
					</td>
				</tr>
			</table>
            <table border="1" class="table table-bordered" width="100%">
                <tbody>
                    <tr style="background: #eee;font-weight: bold;">
                        <th style="text-align:center">Item</th>
                        <th style="text-align:center">Part Number/Description</th>
                        <th style="text-align:center">Unit Price (IDR)</th>
                        <th style="text-align:center">Discount</th>
                        <th style="text-align:center">Qty</th>
                        <th style="text-align:right">Total (IDR)</th>
                    </tr>
                    <tr>
                        <td>
                            <img src="http://localhost/iwd/berca/file/catalog/small/gambar-300-300-gbr_produk-CSDGCColumn-1.jpg"
                            style="width:50px;" />
                        </td>
                        <td>
                            <p class="product-name">112 2032</p>
                        </td>
                        <td style="text-align:right">9.911.000</td>
                        <td style="text-align:center">9.911.000</td>
                        <td style="text-align:center">1</td>
                        <td style="text-align:right">9.911.000</td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right" style="font-weight: bold;">Total List Price (IDR)</td>
                        <td align="right">9.911.000</td>
                    </tr>
					<tr>
                        <td colspan="5" align="right" style="font-weight: bold;">VAT 10% (IDR)</td>
                        <td align="right">911.000</td>
                    </tr>
					<tr>
                        <td colspan="5" align="right" style="font-weight: bold;">Total (IDR)</td>
                        <td align="right">10.822.000</td>
                    </tr>
					<tr>
						<td colspan="6">Terbilang: $terbilang</td>
					</tr>
                </tbody>
            </table>
			
			<table>
				<p><u>Term and Condition</u></p>
				<tr>
					<td>Term of Payment</td><td>:</td><td>100%</td>
				</tr>
				<tr>
					<td>Delivery Time</td><td>:</td><td>6-8 weeks againts receive official PO</td>
				</tr><tr>
					<td>Delivery Franco</td><td>:</td><td>Surabaya</td>
				</tr>
				<tr>
					<td>Warranty</td><td>:</td><td>-</td>
				</tr><tr>
					<td>Quotation Validity</td><td>:</td><td> 2 weeks againts quotation date</td>
				</tr>
			</table>
			
			<p>&nbsp;</p>
			<p>Sincerely yours,</p>
			<p>&nbsp;</p>
			<p>Logo</p>
        </div>
         
        <br /></form>
    </body>
</html>
EOD;

$html;
// Print text using writeHTMLCell()
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
// $pdf->Output('example_001.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+